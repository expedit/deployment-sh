
#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/snippets/utility


usage()
{
	cat <<-EOF

	Usage: dymsh git feature <branch-name>

	Create local feature branches recursively.

	<branch-name> is the name of the new branch to be created in all
	repositories of the repository tree. Any name except 'dev', 'master',
	'release-*' or 'hotfix-*' is accepted.

	Adapted from https://nvie.com/posts/a-successful-git-branching-model/

	Feature branches
	----------------------------------------------------------------------
	May branch off from:       dev
	Must merge back into:      dev
	Branch naming convention:  anything except master, develop, release-*,
	                           or hotfix-* 
	----------------------------------------------------------------------

	EOF
	exit 255
}


# Usage: <branch-name>
feature_begin()
{
	# validate args
	echo "$1"
	if is-string-in "$1" 'master' 'dev' 'release-*' 'hotfix-*' || ! grep -E '^[a-zA-Z0-9_-]+$' <<< "$1" > /dev/null;
	then
		echo "Invalid feature branch name: $1" | colorpipe --red
		echo "Feature branches must match the regex [a-zA-Z0-9_-]+ and cannot be named as master, dev, release-* or hotfix-*." | colorpipe --red
		echo "Nothing has been modified." | colorpipe --red
		return 1
	fi

	# pre-conditions
	if ! dymsh foreach "${OPTS[@]}" -- dymsh util is-current-branch-in dev;
	then
		echo "You must be in the 'dev' branch to create a feature branch." | colorpipe --red
		echo "Nothing has been modified." | colorpipe --red
		return 1
	fi


	# skip if --dry-run option enabled
	if is-dry-run; then
		if ! is-quiet; then
			echo dymsh foreach "${OPTS[@]}" -- git checkout -b "$1" dev | colorpipe --cyan
		fi
		return 0
	fi

	is-verbose || set -x   # enable xtrace (command execution information)

	# actual commands (TODO should we create a commit "Starting feature branch 'myfeature'"?)
	dymsh foreach "${OPTS[@]}" -- git checkout -b "$1" dev

	{ RET=$?; set +x; } 2>/dev/null     # disable xtrace
	return $RET
}


parse-cmd-line()
{
	OPTS=()
	while [ $# -gt 1 ];
	do
		case $1 in
			--dry-run)
				DYMSH_DRY_RUN=1;
				OPTS+=(--dry-run)
				shift
				;;
			-q|--quiet)
				DYMSH_QUIET=1;
				OPTS+=(--quiet)
				shift
				;;
			-v|--verbose)
				DYMSH_VERBOSE=1;
				OPTS+=(--verbose)
				shift
				;;
			help|-h|--help)
				usage
				exit
				;;
			*)
				fail 128 "Unexpected parameter: $1"
				;;
		esac
	done

	test $# = 1 || {
		echo "Missing parameter for '$DYMSH_COMMAND'" | colorpipe --red >&2;
		usage;
	}
	test "$1" = '-h' -o "$1" = '--help' && usage

	feature_begin "$1"
}

parse-cmd-line "$@"
