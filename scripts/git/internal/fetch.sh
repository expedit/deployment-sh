#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/submodule.sh


############################
# CLI parsing and usage

usage()
{
	cat <<-EOF

	Usage: dymsh git internal fetch [OPTIONS] -- PARAM...

	OPTIONS
	   -h, --help
	       Show usage help.

	PARAM...
	   Parameters which will be forwarded to custom scripts, such as .dymsh.
	EOF
	exit 255
}

ALL_ARGS=("$@")

FETCH=1

while test $# -gt 0 -a "$1" != '--'
do
	# echo "current parameter = $1"
	case $1 in
		-h|--help)
			usage
			;;
		--no-fetch)
			FETCH=0
			shift
			;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done
if [ $# -gt 0 ]; then
	shift
fi

# echo CUSTOM_ARGS $# "$@"
# echo ALL_ARGS ${#ALL_ARGS[@]} ${ALL_ARGS[@]}

######################


# Load .dymsh script of this repository
source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"

checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1

DYMSH_NETWORK_NAME=${DYMSH_NETWORK_NAME:-appnet}

DYMSH_IMAGE_NAME=$($DYMSH_ROOT/util/imgname.sh "$@")
[ "$?" -eq 0 ] || fail 1


test -z "$BASE_PATH" && export BASE_PATH=$(dirname $(pwd))/

log "Initializing submodules for .../${PWD#$BASE_PATH}..."
git submodule init | logpipe || exit 1


export GIT_SSH=$DYMSH_ROOT/util/ssh-git.sh

for MODPATH in $(git submodule status | awk '{print $2}');
do
	MODNAME=$(git config --file=.gitmodules --list | grep $MODPATH | tr '.' '\t' | cut -f2)
	MODURL=$(git config --get submodule.$MODNAME.url)
	export SVM_HOSTNAME=$(grep -oP '(?<=git@)[a-zA-Z0-9._-]+(?=:.*)' <<< $MODURL)
	# echo "SVM_HOSTNAME    $SVM_HOSTNAME"

	# Executes pre-fetch custom script
	if [ -x $MODPATH.pre.sh ]; then  # TODO pipe stderr to logerr
		$MODPATH.pre.sh "$@" | logpipe   # PIPESTATUS is bash-only
		test ${PIPESTATUS[0]} -eq 0 || fail $? "Hook has failed: $MODPATH.pre.sh"
	fi

	submodule_clone $MODPATH

	# Executes fetch error custom script on failure
	if [ $? -eq 1 ]; then
		test -x $MODPATH.err.sh && $MODPATH.err.sh "$@" | logpipe
		test ${PIPESTATUS[0]} -eq 0 || fail 1 "Submodule clone/fetch has failed"
	fi

	# Executes post-fetch custom script
	if [ -x $MODPATH.pos.sh ]; then  # TODO pipe stderr to logerr
		$MODPATH.pos.sh "$@" | logpipe
		test ${PIPESTATUS[0]} -eq 0 || fail $? "Hook has failed: $MODPATH.pos.sh"
	fi

	# Recurse on nested submodules
	if [ -d $MODPATH -a -e $MODPATH/.git ]; then
		LWD=$(pwd)
		log "Entering .../${PWD#$BASE_PATH}/$MODPATH..."
		cd $MODPATH || fail 1 "Could not change to diretory: $MODPATH"

		if [ -x "$0" ];
		then
			DYMSH_INDENT="$DYMSH_INDENT>" "$0" "${ALL_ARGS[@]}" || fail 21
		fi
		log "Backing to $LWD"
		cd $LWD
	fi
done
