#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/snippets/array
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/timed-loop.sh


dymsh-git-push-usage()
{
	cat <<-EOF
	Usage: dymsh git push [OPTION...]  [<remote> [<branch>...]]

	Push branches (dev and master by default) and tags to a remote.

	OPTIONS
	   -h, --help)
	      Show this usage help.
	   -v, --verbose
	      Increase verbosity.
	EOF
	exit 255
}


dymsh-git-push()
{
	local verbose=
	local remote=
	local branches=()

	while [ $# -gt 0 ]
	do
		case $1 in
			-h|--help)
				dymsh-git-push-usage
				;;
			-v|--verbose)
				verbose=--verbose
				# echo $verbose
				shift
				;;
			*)
				echo "$1" | grep -E '[a-zA-Z0-9_-]+' >/dev/null ||
				  fail 128 "dymsh git push: unexpected parameter: $1"
				test -z "$remote" && remote="$1" && shift && continue
				branches+=("$1") && shift && continue
				;;
		esac
	done

	test ${#branches[@]} = 0 && branches=(dev master)
	test -z "$remote" && remote=origin
	test -n "$verbose" && set -x

	cmd=(git push $verbose $remote ${branches[*]} "&&" git push $verbose $remote --tags)
	dymsh foreach -v -s -- "${cmd[*]}"
}


dymsh-git-push "$@"
