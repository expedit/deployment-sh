
Usage: dymsh git checkout [OPTION...] <branch>

Switch branches or restore working tree files.

OPTIONS
   -h, --help            show usage help
   -q, --quiet           suppress progress reporting
   -b <branch>           create and checkout a new branch
   -B <branch>           create/reset and checkout a branch
   --detach              detach the HEAD at named commit
   -t, --track           set upstream info for new branch
   -f, --force           force checkout (throw away local modifications)

