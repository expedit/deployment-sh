#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/snippets/array
source $DYMSH_ROOT/util/snippets/color
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/timed-loop.sh


############################
# CLI parsing and usage

usage()
{
	cat <<-EOF
	Usage: dymsh git init <project-name> <service-name> [<network-name>]
	
	Initialize a dymsh project inside an already created directory.

	It is preferable the current directory is empty.

	Example:   mkdir -p foo/bar &&
	           cd foo/bar &&
	           dymsh git init foo bar

	EOF
	exit 255
}


# init <project-name> <service-name>
init_repo()
{
	test -d .dymsh && fail 1 "Dymsh configuration directory already exists."
	test -f .dymsh && fail 2 "Deprecated dymsh configuration file found."

	mkdir -p .dymsh || fail 3 "Could not create .dymsh directory."

	local template_dir="$DYMSH_ROOT/git/init-template"

	rsync -av --ignore-existing "$template_dir/.dymsh" ./ ||
	   fail 4 "Could not add contents to .dymsh directory."

	cat "$template_dir/..dymsh/.pre-init.sh" \
	    - \
	    "$template_dir/..dymsh/.pos-init.sh" \
	    >> .dymsh/init.sh <<-EOF ||
	DYMSH_VERSION=~$(dymsh --version)
	DYMSH_PROJECT_NAME="$1"
	DYMSH_SERVICE_NAME="$2"
	DYMSH_NETWORK_NAME="${3:-net}"
	EOF
	   fail 5 "Could not create .dymsh/init.sh configuration file."

	# rm ..dymsh/.pre-init.sh ..dymsh/.pos-init.sh

	if [ ! -f ./Dockerfile ]; then
		cp "$template_dir/Dockerfile" ./Dockerfile ||
		   fail 6 "Could not create Dockerfile."
	else
		echo "Dockerfile already found. It has been left intact."
	fi

	# Append or copy template of .gitignore to current .gitignore
	cat "$template_dir/.gitignore" >> ./.gitignore ||
	     fail 7 "Could not create/update .gitignore"

	git init &&
	git add -f .dymsh Dockerfile .gitignore &&
	dymsh version -m "Initial commit" 0.0.1 &&
	git checkout -b dev || fail 8 "Failed to initialize and commit the first version"

	# { set +x; } 2>/dev/null   # disable xtrace

	setcolor --blue
	cat <<-EOF

	Dymsh initialization completed successfully. To start the service run:

	dymsh build -l -p && dymsh run -t dev

	EOF
	resetcolor
	
	return 0
}


test $# -ge 2 || usage
init_repo "$@"
