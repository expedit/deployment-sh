#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/snippets/array
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/timed-loop.sh


############################
# CLI parsing and usage

usage()
{
	cat $DYMSH_ROOT/git/${1}${1:+-}usage.txt
	exit 255
}




ALL_ARGS=("$@")

test $# = 0 && usage

while [ $# -gt 0 ]
do
	case $1 in
		-h|--help)
			usage
			;;
		help)
			shift
			test -n "$1" && dymsh git "$1" --help
			exit
			;;
		br|branch)
			shift
			array_contains '--help' "$@" && usage branch
			array_contains '-h' "$@" && usage branch

			args=("$@")
			while [ $# -gt 0 ]; do
				case "$1" in
				   -v|--verbose|-q|--quiet|-t|--track|-r|--remotes|-a|--all|-d|--delete|-D|-m|--move|-M|--list|-f|--force) shift ;;
				   --contains|--merged|--no-merged) shift; test "${1:0:1}" != '-' && shift ;;
				   [a-z0-9]*) shift ;;
				   -*) fail 1 "Invalid parameter: $1" ;;
				esac
			done
			
			dymsh foreach -v -s -- git branch "${args[@]}"
			exit
			;;
		co|checkout)
			shift
			array_contains '--help' "$@" && usage checkout
			array_contains '-h' "$@" && usage checkout

			args=("$@")
			while [ $# -gt 0 ]; do
				case "$1" in
				   -q|--quiet|--detach|-t|--track|-f|--force) shift ;;
				   -b|-B) shift 2 ;;
				   [a-z0-9]*) shift ;;
				   *) fail 1 "Invalid parameter: $1" ;;
				esac
			done

			dymsh foreach -v -s -- git checkout "${args[@]}"
			exit
			;;
		update)
			shift
			test $# -eq 0 || usage update

			dymsh git internal fetch ||
			  fail 1 "Could not update submodules"
			exit
			;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done
