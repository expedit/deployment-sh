


_string_in()
{
	VAL="$1"
	shift
	while [ $# -gt 0 ];
	do
		# echo "Testing if '$VAL' matches '$1'..."
		if [[ "$VAL" == $1 ]]; then return 0; fi
		shift
	done
	return 1
}


_detect_cur_branch()
{
	git branch | grep '*' | awk '{print $2}'
}


_test_branch_in()
{
	CURBR=$(_detect_cur_branch)
	_string_in "$CURBR" "$@"
}



