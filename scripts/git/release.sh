#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/util/log.sh

echo "Using deprecated begin release." | colorpipe --yellow >&2
dymsh exp hit release "$@"
