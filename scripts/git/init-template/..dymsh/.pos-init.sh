

fail()
{
	test -n "$2" && echo -e "\e[0;31m$2\e[0m" >&2
	exit $1
}



case "$DYMSH_TARGET" in
	dev|prod)
		# Load special variables according to the target environment
		source .dymsh/conf/$DYMSH_TARGET-vars.sh || fail 1 'Could not load configuration'
		;;
	*)
		X=("dymsh run" "dymsh stack build" "dymsh deploy start")
		if dymsh util is-one-of "$DYMSH_COMMAND" "${X[@]}"; then
			fail 100 "${DYMSH_PROJECT_NAME}-${DYMSH_SERVICE_NAME}: invalid target environment: $DYMSH_TARGET"
		fi
		;;
esac
