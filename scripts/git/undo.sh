


usage()
{
	cat <<-EOF
	Usage: dymsh git undo

	Undo a started feature, release or hotfix branch before it is joined.
	All branch commits will be lost and the previous branch (dev or master) will
	be checked out.

	EOF
}
