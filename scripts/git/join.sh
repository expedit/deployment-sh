
#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/snippets/utility


usage()
{
	cat <<-EOF
	Usage: dymsh git join

	Recursively merge the current branch (a feature, release or hotfix branch)
	into dev only (for feature branches) or dev and master (for release
	and hotfix branches).

	EOF
}


show-merge-conflicts-error()
{
	echo "There are merge conflicts or changes on working tree or in the index." | colorpipe --red >&2
	echo "Please, fix, stash or commit them, then try again." | colorpipe --red >&2
	echo "Nothing has been modified." | colorpipe --red >&2
}


# this function must be called from feature-join-all after the
# pre-conditions checking has passed
feature-join-single()
{
	TMP_RMT_FILE=$(source ./.dymsh/init.sh && echo "$DYMSH_TMP_DIR/$DYMSH_SERVICE_NAME")

	cur_branch=$(get-current-branch)

	local submodules_changed=

	if is-foreach-first-mmm;
	then
		pwd -P > $TMP_RMT_FILE || return

		has-submodule-only-changes
		case $? in
			0)  # only submodules have changes
				echo "Submodules have changed." | colorpipe --cyan
				submodules_changed=1
				;;
			1)  # there are changes on submodules and other objects
				show-merge-conflicts-error
				return 1
				;;
			2)  # there are no changes
				submodules_changed=0
				;;
			*)
				echo "Internal error" | colorpipe --red >&2
				return 255
		esac

		set -x

		if [ $submodules_changed = 1 ]; then
			git add -u &&
			git commit -m "Updating submodules before joining feature branch"
		fi &&

		git checkout dev &&
		git merge --no-ff "$cur_branch" -m "Merge branch '$cur_branch' into dev"
		test $? -eq 0 || _handle_merge_conflict || return 1
		
		git branch -d "$cur_branch"

	else  # is rest
		{ git remote rm dyfs 2>/dev/null || true; } &&
		git remote add dyfs $(cat $TMP_RMT_FILE) &&
		git fetch dyfs &&
		git fetch dyfs --tags &&

		# cur_branch=$(_detect_cur_branch) &&
		git checkout dev &&
		git reset --hard dyfs/dev

		git branch -d "$cur_branch"
	fi

	{ RET=$?; set +x; } 2>/dev/null     # disable xtrace
	return $RET
}


# usage: error-message-unchanged <required-branch> <found-branch> <found-branch-type>
error-message-unchanged()
{
	echo "You must be in a '$1' branch." | colorpipe --red
	echo "Current branch: $2." | colorpipe --red
	echo "Current branch type: $3." | colorpipe --red
	echo "Nothing has been modified." | colorpipe --red
	return 1
}


feature-join-all()
{
	# echo feature_merge params are: "$@"
	# while [ $# -gt 0 ]; do
	# 	case $1 in
	# 		--conflict)
	# 			export CONFLICT_HANDLING_SCRIPT=$2
	# 			shift 2
	# 			;;
	# 		*)
	# 			echo "Invalid option given to feature_merge: $1" | colorpipe --red
	# 			return 1
	# 			;;
	# 	esac
	# done
	# echo CONFLICT_HANDLING_SCRIPT="$CONFLICT_HANDLING_SCRIPT"

	test $# -eq 0 || return 1

	# pre-conditions
	echo "Checking pre-conditions..."
	command=(
		dymsh util is-current-branch-a-feature
		'&&' echo "Branch '$(dymsh util get-current-branch)' is a valid feature branch." \>\&2
		'|| {'
			dymsh util get-current-branch \;
			dymsh util get-current-branch-type \;
			false \;
		'}'
	)

	tempfile=$(mktemp)
	if ! dymsh foreach -v -s "${command[*]}" >$tempfile;
	then
		error-message-unchanged feature $(cat $tempfile)
		return 1
	fi

	echo "Pre-conditions were satisfied."

	dymsh foreach -v -- dymsh git join --only
}





current_branch_name=$(get-current-branch)
# test "$current_branch_name" = "dev" -o "$current_branch_name" = "master" && {
#     echo "Only feature, release* and hotfix* branches may be merged." | colorpipe --red
#     echo "The current branch is '$current_branch_name'." | colorpipe --red
#     echo "Nothing has been modified." | colorpipe --red
#     exit 1;
# }


test -n "$DYMSH_TMP_DIR" || export DYMSH_TMP_DIR=$(mktemp -d -t dymsh-remote.XXXXXXXX)


while [ $# -gt 0 ];
do
	case "$1" in
		--only)
			only=1
			shift
			;;
		*)
			echo "Invalid parameter" | colorpipe --red >&2
			exit 1
			;;
	esac
done


is-only() { test "$only" = 1; }


current_branch_type=$(get-branch-type $current_branch_name)
case "$current_branch_type" in
	feature)          # public entrypoint for feature-join
		is-only && feature-join-single || feature-join-all
		;;
	release)
		echo "Using deprecated merge due to join is not implemented for release yet." | colorpipe --yellow >&2
		dymsh exp hit merge
		# fail 1 "Not implemented yet"
		;;
	hotfix)
		echo "Using deprecated merge due to join is not implemented for hotfix yet." | colorpipe --yellow >&2
		dymsh exp hit merge
		# fail 1 "Not implemented yet"
		;;
	*)
		# error-message-unchanged <required-branch> <found-branch> <found-branch-type>
		fail 1 "Invalid branch type: $current_branch_type"
		;;
esac

exit

if [ $current_branch_type = feature ]; then
	CMD="merge"
	FNC=${current_branch_type}_merge
	RECURSIVE=${RECURSIVE:-1}
	INTERACTIVE=1
	exec_recursive "$@"
else
	HITCMD="dymsh foreach -v -i -- dymsh exp hit"

	if _is_root_repo; then
		current_branch_name=$(get-current-branch) &&
		$HITCMD ${OPTS[@]} --function release_or_hotfix_merge_preconditions $current_branch_type &&
		$HITCMD ${OPTS[@]} --function release_or_hotfix_merge "$@" $current_branch_type master || exit

		# TODO/2018-08-16: once current_branch_type is already merged into master,
		#   maybe the following merges could be:
		#     - release: master into dev
		#     - hotfix:  master into release-* and dev
		#   but SIDE EFFECTS must be evaluated to prevent conflict between
		#   the master tag, the version in the branch name and the VERSION file.
		case $current_branch_type in
			release)
				$HITCMD ${OPTS[@]} --function release_or_hotfix_merge "$@" $current_branch_type dev || exit
				;;
			hotfix)
				RELEASE_BRANCHES="$(git branch | sed 's/[* ]//g' | grep release-)"
				for b in $RELEASE_BRANCHES dev; do
					$HITCMD ${OPTS[@]} --function release_or_hotfix_merge "$@" $current_branch_type $b || exit
				done
				;;
		esac

		dymsh foreach -v -- bash -c 'git branch -d '"$current_branch_type"'-$(cat VERSION)'
	else
		echo "ERROR !!!!! " && exit 255
	fi
fi
