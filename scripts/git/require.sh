#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/git/library.sh


usage()
{
	cat <<-EOF
	Usage: dymsh git require [OPTION...] <repository-url> <name> [<directory>=.]
	
	Add a submodule from the given URL to the <directory>/<name> directory.

	OPTIONS
	   -i, --idfile, --identity-file <identity-file>
	      The name of the file which contains the private SSH key of the
	      repository.
	   -v, --verbose
	      Increase verbosity.
	   --dry-run
	      Just show the commands which would be sent but do not execute them.

	EOF
	exit 255
}



# require <repo-url> <name> [<directory>=.]
require()
{
	# echo ========
	if _test_branch_in 'master' 'hotfix-*' 'release-*'; then
		echo "You are in a hotfix-*, release-* or in the master branch." | colorpipe --red
		echo "Change to dev or a feature branch and try again." | colorpipe --red
		echo "Nothing has been modified." | colorpipe --red
		return 1;
	else
		echo bransh is ok
	fi

	local url="$1"
	local name="$2"
	local path="${3:-.}/$2"

	test -n "$url" -a -n "$name" || return 1

	if [ $DYMSH_DRY_RUN = 1 ]; then
		echo " git submodule add -b $(_detect_cur_branch) --name '$name' '$url' '$path' &&" | colorpipe --cyan
		echo " git config diff.submodule log &&" | colorpipe --cyan
		echo " git config status.submodulesummary 1 &&" | colorpipe --cyan
		echo " git config -f .gitmodules 'submodule.$name.branch' . &&" | colorpipe --cyan
		echo " git add .gitmodules &&" | colorpipe --cyan
		echo " git commit -m \"Adding $name submodule from '$url' to '$path' directory\"" | colorpipe --cyan
		return 0
	fi

	test $DYMSH_VERBOSE -eq 1 && set -x

	git submodule add -b $(_detect_cur_branch) --name "$name" "$url" "$path" &&
	git config diff.submodule log &&
	git config status.submodulesummary 1 &&
	git config -f .gitmodules "submodule.$name.branch" . &&
	git add .gitmodules &&
	git commit -m "Adding $name submodule from '$url' to '$path' directory"

	{ RET=$?; set +x; } 2>/dev/null
	return $RET
}



test $# -ge 2 || usage


ALL_ARGS=("$@")

DYMSH_VERBOSE=0
DYMSH_DRY_RUN=0
repo_url=
mod_name=
mod_path=

while test $# -gt 0
do
	case $1 in
		--dry-run)
			DYMSH_DRY_RUN=1
			shift
			;;
		-i|--idfile|--identify-file)
			idfile="$2"
			shift 2
			;;
		-v|--verbose)
			DYMSH_VERBOSE=1
			shift
			;;
		-h|--help)
			usage
			;;
		*)
			test -z "$repo_url" && repo_url="$1" && shift && continue
			test -z "$mod_name" && mod_name="$1" && shift && continue
			test -z "$mod_path" && mod_path="$1" && shift && continue
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done

test -n "$idfile" &&
  export GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no -i '$idfile'"

require "$repo_url" "$mod_name" "$mod_path"
