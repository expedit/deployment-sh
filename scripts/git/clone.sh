#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/snippets/array
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/timed-loop.sh


############################
# CLI parsing and usage

usage()
{
	cat <<-EOF
	Usage: dymsh git clone <repo-url> [<path>]
	             [-b <branch>|-t <tagname>]
	             [-i <identity-file>]
	
	Clone a dymsh repository tree take into account needed SSH keys.

	repo-url is the URL of the project. E.g. git@github.com:foo/bar.git
	path is the directory (absolule ou relative) where to clone the project.

	OPTION
	   -b, --branch <branch>
	      The name of the branch which must be checked out. It must exist in
	      all submodules.
	   -t, --tag <tagname>
	      The name of the tag which must be checked out in the root repository.
	      Submodules will be checked out according to the commit its parent
	      points to.
	   -i, --idfile, --identity-file <identity-file>
	      The name of the file which contains the private SSH key of the root
	      repository. All submodules will be cloned according to the SSH keys
	      specified by dymsh project strucutre requirements.
	
	NOTES: if neither branch or tagname are specified it defaults to master.

	EXAMPLE:  dymsh git clone git@github.com:foo/bar.git ./foo \\
	                          -b dev -i ~/.ssh/foo-bar.rsa
	EOF

	exit 255
}


ALL_ARGS=("$@")

repo_url=
path=

while test $# -gt 0
do
	case $1 in
		-b|--branch)
			branch="$2"
			shift 2
			;;
		-t|--tag)
			tagname="$2"
			shift 2
			;;
		-i|--idfile|--identify-file)
			idfile="$2"
			shift 2
			;;
		-h|--help)
			usage
			;;
		*)
			test -z "$repo_url" && repo_url="$1" && shift && continue
			test -z "$path" && path="$1" && shift && continue
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done

echo $repo_url
echo $path
echo $branch
echo $tagname
echo $idfile

test \( -n "$branch" -a -z "$tagname" \) \
  -o \( -z "$branch" -a -n "$tagname" \) \
  -o \( -z "$branch" -a -z "$tagname" \) \
  ||
  fail 1 "branch and tag name are mutually exclusive."

test -z "$repo_url" && fail 2 "Repository URL is required"

get-git-ref()
{
	test -n "$branch" && echo "$branch" && return 0
	test -n "$tagname" && echo "$tagname" && return 0
	echo "master"
}


test -n "$idfile" &&
  GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no -i '$idfile'" ||
  GIT_SSH_COMMAND="ssh"

path=${path:-$(echo "$repo_url" | grep -oE '([a-zA-Z0-9_-]+).git$' | sed -r 's~^([a-zA-Z0-9_-]+).git$~\1~')}

GIT_SSH_COMMAND="$GIT_SSH_COMMAND" \
  git clone -b "$(get-git-ref)" "$repo_url" "$path" ||
  fail 1 "Failed to clone repository."

cd "$path"

dymsh git internal fetch || fail 1 "Failed to fetch submodules"

if [ -n "$branch" ];
then
	dymsh git checkout "$branch" || fail 1 "Could not check out to branch '$branch'"
	exit 0
elif [ -n "$tagname" ];
then
	exit 0
else
	exit 2
fi
