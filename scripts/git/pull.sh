#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/snippets/array
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/timed-loop.sh
source $DYMSH_ROOT/git/library.sh


dymsh-git-pull-usage()
{
	cat <<-EOF
	Usage: dymsh git pull [OPTION...]  [<remote> [<branch>...]]

	Pull (fetch & merge) branches (dev and master by default) and tags from
	a remote.

	OPTIONS
	   -h, --help)
	      Show this usage help.
	   -v, --verbose
	      Increase verbosity.
	   --ff
	      When the merge resolves as a fast-forward, only update the branch
	      pointer, without creating a merge commit. This is the default behavior.
	   --no-ff
	      Create a merge commit even when the merge resolves as a fast-forward.
	      This is the default behaviour when merging an annotated (and possibly
	      signed) tag.
	   --ff-only
	      Refuse to merge and exit with a non-zero status unless the current HEAD
	      is already up-to-date or the merge can be resolved as a fast-forward.
	   -m <message>
	      Set the commit message to be used for the merge commit (in case one is
	      created).
	EOF
	exit 255
}


dymsh-git-pull()
{
	local merge_args=()
	local verbose=
	local remote=
	local branches=()

	while [ $# -gt 0 ]
	do
		case $1 in
			-h|--help)
				dymsh-git-pull-usage
				;;
			-v|--verbose)
				verbose=--verbose
				shift
				;;
			--ff|--no-ff|--ff-only)
				merge_args+=($1)
				shift
				;;
			-m)
				merge_args+=($1 "\"$2\"")
				shift 2
				;;
			*)
				echo "$1" | grep -E '[a-zA-Z0-9_-]+' >/dev/null ||
				  fail 128 "dymsh git pull: unexpected parameter: $1"
				test -z "$remote" && remote="$1" && shift && continue
				branches+=("$1") && shift && continue
				;;
		esac
	done

	test ${#branches[@]} = 0 && branches=(dev master)
	test -z "$remote" && remote=origin
	test -n "$verbose" && set -x

	dymsh foreach -v -s -- "git fetch $verbose $remote ${branches[*]}" &&

	local previous_branch=$(_detect_cur_branch)

	checkout-and-merge() {
		for branch in ${branches[@]}; do
			cmd=(git checkout $branch "&&" git merge $verbose "${merge_args[@]}" $remote/$branch) &&
			dymsh foreach -v -s -- "${cmd[*]}" || return
		done
	}

	checkout-and-merge
	local status=$?

	dymsh foreach -v -s -- git checkout "${previous_branch:-dev}"
	dymsh foreach -v -s -- git checkout "${previous_branch:-dev}"
	local status2=$?

	return $(($status+$status2))
}


dymsh-git-pull "$@"
