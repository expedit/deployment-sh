#!/bin/bash

#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh

############################
# CLI parsing and usage

usage()
{
	cat <<-EOF

	Usage: dymsh link [OPTIONS] <dest-dir>

	<dest-dir>
	   Directory where the symbolic links will be created.
	   Required. Must be absolute. Must exist.

	OPTIONS
	   --all
	       Link all repositories, even if their base names already
	       exist in <dest-dir>. This is made by appending a counter
	       to the link name.
	   -h, --help
	       Show usage help.

	PARAM...
	   Parameters which will be forwarded to custom scripts, such as .dymsh.
	EOF
	exit 255
}

ALL_ARGS=("$@")
DEST_DIR=
ALL_REPOS=0

while test $# -gt 0 #-a "$1" != '--'
do
	# echo "current parameter = $1"
	case "$1" in
		--all)
			ALL_REPOS=1
			shift
			;;
		-h|--help)
			usage
			;;
		*)
			test -z "$DEST_DIR" || fail 128 "Unexpected parameter: $1"
			test -d "$1" && DEST_DIR="$1" || fail 128 "Unexpected parameter: $1"
			shift
			;;
	esac
done
if [ $# -gt 0 ]; then
	shift
fi

# echo CUSTOM_ARGS $# "$@"
# echo ALL_ARGS ${#ALL_ARGS[@]} ${ALL_ARGS[@]}

######################

test -n "$DEST_DIR" || fail 1 "<dest-dir> is required."
[ "$DEST_DIR" != "${DEST_DIR#/}" ] || fail 1 "<dest-dir> must be absolute."

# Load .dymsh script of this repository
# source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"

# checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1

if [ ! -e "$DEST_DIR/$(basename $(pwd))" ]; then
	echo "Linking $(basename $(pwd))."
	ln -s "$(pwd)" "$DEST_DIR"
elif [ "$ALL_REPOS" -eq 1 ]; then
	counter=1
	while [ -e "$DEST_DIR/$(basename $(pwd))-${counter}" ];
	   do counter=$((counter+1)); done
	echo "Linking $(basename $(pwd)) as $(basename $(pwd))-${counter}."
	ln -s "$(pwd)" "$DEST_DIR/$(basename $(pwd))-${counter}"
else
	echo "Skipping $(pwd)."
fi

for f in $(git submodule foreach  | awk '{print $2}' | tr -d "'");
do
	cd $f
	$DYMSH_ROOT/link.sh "${ALL_ARGS[@]}" || fail 1
	cd - > /dev/null
done
