#!/bin/bash

fail()
{
	test -n "$2" && echo -e "\e[0;31m$2\e[0m" >&2
	exit $1
}


usage()
{
	cat <<-EOF

	Usage: dymsh version OPTIONS <newversion>|major|minor|patch

	<newversion>
	   Set the new version to the given string
	   Regex: [0-9]+\.[0-9]+\.[0-9]+(-[a-zA-Z_0-9-]+)?

	major, minor, patch
	   Increment the specified part of the current version
	   Shortcuts: maj, 1; min, 2; pat, 3.

	OPTIONS
	   -l LABEL, --label LABEL
	       Use the given label at the end of version number (eg. 1.3.5-LABEL).
	       Optional. Default: empty. Regex: [a-zA-Z_0-9-]+
	   -m MSG, --message MSG
	       Use the given text as tag and commit messages
	   --dry-run
	       Do not make any changes, but show the commands which would be executed.
	   -h, --help
	       Show this help.
	EOF
	exit 255
}


# Usage:     increment_version <position> <version>
# position   major, minor, patch, maj, min, pat, 1, 2, 3
# version:   ^[0-9]+\.[0-9]+\.[0-9]+(-[a-zA-Z_0-9]+)?$
increment_version()
{
	local pos=$1
	local ver=$2
	IFS='.-' read maj min patch label < <(echo "$ver")

	case $pos in
		major|maj|1)
			((maj++))
			min=0
			patch=0
			;;
		minor|min|2)
			((min++))
			patch=0
			;;
		patch|pat|3)
			((patch++))
			;;
		*)
			fail 255 "Invalid version position: $pos"
			;;
	esac

	echo $maj.$min.$patch
}


DRY_RUN=0

test $# -eq 0 && usage

while test $# -gt 0
do
	if echo $1 | grep -E '^[0-9]+\.[0-9]+\.[0-9]+(-[a-zA-Z_0-9-]+)?$'
	then
		VER="$1"
		shift
	else
		case $1 in
			-m|--message)
				COMMIT_MSG="$2"
				shift; shift
				;;
			-l|--label)
				echo "$2" | grep -E "[a-zA-Z_0-9-]+" || fail 128 "Invalid version label"
				VER_LABEL="$2"
				shift; shift
				;;
			--dry-run)
				DRY_RUN=1
				shift
				;;
			major|minor|patch|maj|min|pat|1|2|3)
				POS="$1"
				shift
				;;
			-h|--help)
				usage
				;;
			*)
				fail 128 "Unexpected parameter: $1"
				;;
		esac
	fi
done



test -n "$POS" -a -n "$VER" \
  && fail 128 "Only one of position or version is allowed"

test -z "$POS" -a -z "$VER" \
  && fail 128 "One of position or version is required"

test -n "$VER" -o -f VERSION || fail 129 "VERSION file not found. Please specify the version directly."

GIT_TAG=$(git describe --tags --abbrev=0 2>/dev/null | sort -V | tail -1)
FILE_VER=$(test -f VERSION && cat VERSION || echo "")

if [ -n "$POS" ];
then
	test "$GIT_TAG" = "$FILE_VER" \
	  || fail 130 "Versions mismatch (git tag=$GIT_TAG, file=$FILE_VER). Please specify directly."

	OLD_VER=$GIT_TAG
	NEW_VER=$(increment_version $POS $OLD_VER)${VER_LABEL:+-}$VER_LABEL
else
	OLD_VER=$(test "$GIT_TAG" = "$FILE_VER" && echo "$GIT_TAG" || echo unknown)
	NEW_VER=$VER${VER_LABEL:+-}$VER_LABEL
fi



COMMIT_MSG=${COMMIT_MSG:-Bumped version number from $OLD_VER to $NEW_VER}

if [ $DRY_RUN -eq 1 ]; then
	cat <<-EOF
	echo "$NEW_VER" > VERSION
	git add VERSION
	git commit -m "$COMMIT_MSG"
	git tag -a "$NEW_VER" -m "$COMMIT_MSG"
	EOF
else
	echo "$NEW_VER" > VERSION &&
	git add VERSION &&
	git commit -m "$COMMIT_MSG" &&
	git tag -a "$NEW_VER" -m "$COMMIT_MSG"
fi


# cat <<EOF
# To push use:
# 	git push origin $(git branch | grep '*' | cut -d ' ' -f2)"
# 	git push origin --tags $(git branch | grep '*' | cut -d ' ' -f2)"
# EOF

