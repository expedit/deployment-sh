#!/usr/bin/env python

import sys
import os

if len(sys.argv) < 3:
    sys.stderr.write('Usage: %s string element ...\n\n' % os.environ['DYMSH_COMMAND'])
    sys.stderr.flush()
    sys.exit(25)

a = sys.argv[1]

for e in sys.argv[2:]:
    if e == a:
        sys.exit(0)

sys.exit(1)
