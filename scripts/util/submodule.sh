#!/bin/bash

source $DYMSH_ROOT/util/log.sh

submodule_clone()
{
	f=$1

	log "$DYMSH_INDENT> $f"
	
	log "submodule update -- $f"
	if [ -f $f.pem ]; then
		SUBMODULE=$f git submodule update -- $f | logpipe || return 1
	else
		git submodule update -- $f | logpipe || return 1
	fi

	# LWD=$(pwd)
	# cd $f || fail 1 "Could not change to diretory: $f"

	# if [ -f $f.pem ]; then
	# 	SUBMODULE=$f git fetch --tags | logpipe || return 1
	# else
	# 	git fetch --tags | logpipe || return 1
	# fi
	# cd $LWD

	log "submodule status -- $f"
	if [ -f $f.pem ]; then
		SUBMODULE=$f git submodule status -- $f | logpipe || return 1
	else
		git submodule status -- $f | logpipe || return 1
	fi
}

