#!/usr/bin/python3

import yaml
import json
import sys
import re
import os

# print(sys.argv, file=sys.stderr)


network = os.getenv('DYMSH_NETWORK_NAME')

config = {
    'version' : '3.2',
    'services' : {},
    'networks' : {
        network: None
    },
    'volumes' : {},
    'secrets' : {}
}


for i in range(1, len(sys.argv)):
    f = sys.argv[i]
    # print("f=" + f, file=sys.stderr)
    doc = yaml.load(open(f))
    # print("doc is:\n" + yaml.dump(doc, default_flow_style=False), file=sys.stderr)
    for k in doc:
        # print("k is " + k, file=sys.stderr)
        if k in ['volumes', 'secrets', 'services']:
            config[k] = { **config[k], **doc[k] }
        # if k == 'volumes':
        #     config['volumes'] = { **config['volumes'], **doc[k] }
        # if k == 'secrets':
        #     print("...secrets1", file=sys.stderr)
            
        #     print("...secrets2", file=sys.stderr)
        # if k == 'services':
        # else:
        #     print("...other", file=sys.stderr)
        #     config['services'][k] = doc[k]
        #     print("...other", file=sys.stderr)
        else:
            print("Unexpected key " + k + " in input", file=sys.stderr)
            exit(1)

# print("config is:\n" + yaml.dump(config, default_flow_style=False), file=sys.stderr)
print(yaml.dump(config, default_flow_style=False))

