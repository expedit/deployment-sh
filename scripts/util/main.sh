

#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/snippets/utility



test $# = 0 && usage

commands=(
	is-current-branch-in
	get-current-branch
	get-current-branch-type
	is-current-branch-a-feature
	check-modules-changed-only
	has-submodule-only-changes
)


while [ $# -gt 0 ]
do
	case $1 in
		-h|--help)
			printf 'dymsh util %s\n' "${commands[@]}"
			exit 255
			;;
		help)
			shift
			echo usage
			exit
			;;
		*)
			if is-string-in "$1" "${commands[@]}";
			then
				fn="$1"
				shift
				$fn "$@"
				exit
			else
				fail 128 "Unexpected parameter: $1"
			fi
			;;
	esac
done
