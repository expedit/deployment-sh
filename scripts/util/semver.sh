#!/bin/bash

checkversion()
{
	RULE=${1:-x}
	$DYMSH_ROOT/3rd/semver.sh -r "$RULE" "$2" | grep "$2" > /dev/null
	test $? -ne 0 && cat <<-EOF  && return 1
	Unsupported version on "$DYMSH_PROJECT_NAME/$DYMSH_SERVICE_NAME"
	at "$(pwd)":
	  Supported by this project: $RULE
	  Current version of dymsh:  $2
	Please updade the project settings (.dymsh/ directory) or reinstall the right version of dymsh.
	EOF
	return 0
}

check-dymsh-version()
(
	# Load .dymsh script of this repository
	source ./.dymsh/init.sh "$@" || return 1
	checkversion "$DYMSH_VERSION" $(dymsh --version) || return 2
)

# Usage:     increment_version <position> <version>
# position   major, minor, patch, maj, min, pat, 1, 2, 3
# version:   ^[0-9]+\.[0-9]+\.[0-9]+(-[a-zA-Z_0-9]+)?$
increment_version()
{
	local pos=$1
	local ver=$2
	IFS='.-' read maj min patch label < <(echo "$ver")

	case $pos in
		major|maj|1)
			((maj++))
			min=0
			patch=0
			;;
		minor|min|2)
			((min++))
			patch=0
			;;
		patch|pat|3)
			((patch++))
			;;
		*)
			fail 255 "Invalid version position: $pos"
			;;
	esac

	echo $maj.$min.$patch
}
