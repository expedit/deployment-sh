#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail


MOVE_DOCKER=0
MOVE_TEST=0
TARGET_VERSION=$(dymsh --version)


############################
# CLI parsing and usage

usage()
{
	cat <<-EOF

	Usage: dymsh util migrate [OPTIONS]

	OPTIONS
	   -d, --docker-dir
	       Move the whole docker/ directory do the new .dymsh directory. The default
	       behavior is to move only specific files.
	   -t, --test-dir
	       Move the whole test/ directory do the new .dymsh directory. The default
	       behavior is to move only specific files.
	   -a, --all
	       Same as --docker-dir and --test-dir.
	   -s, --replace-source
	       Also try to find occurrences of "source .dymsh" and replace by
	       "source .dymsh/init.sh" in the docker/ and test/ scripts.
	   -h, --help
	       Show usage help.
	
	EOF
	exit 255
}


while test $# -gt 0
do
	case $1 in
		-h|--help)
			usage
			;;
		-d|--docker-dir)
			MOVE_DOCKER=1
			shift
			;;
		-t|--test-dir)
			MOVE_TEST=1
			shift
			;;
		-a|--all)
			MOVE_DOCKER=1
			MOVE_TEST=1
			shift
			;;
		-s|--replace-source)
			REPLACE_SOURCE=1
			shift
			;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done


test -f .dymsh || fail 1 ".dymsh file not found"


# Usage: move-f2d DEST_DIR  FILE...
move-f2d()
{
	DEST_DIR="$1"
	mkdir -p "$DEST_DIR"
	shift
	while [ $# -gt 0 ]; do
		test -f "$1" && mv -v "$1" "$DEST_DIR"
		shift
	done
}

migrate-v0.9()
{
	migrate-general "${TARGET_VERSION}"
}

migrate-v0.8()
{
	migrate-general "${TARGET_VERSION}"
}


migrate-general()
{
	######################################################################
	# Required migration
	# varibles, .dymsh and some specific shell files and YAML files
	######################################################################

	# replace variables
	sed -i -r 's/DYMSH_VERSION=.*/DYMSH_VERSION=~'"${1}"'/' .dymsh
	sed -i -r 's/^PROJECT_NAME=(.*)$/PROJECT_NAME=\1\nDYMSH_PROJECT_NAME=${PROJECT_NAME}  # added by dymsh util migrate/' .dymsh
	sed -i -r 's/^SERVICE_NAME=(.*)$/SERVICE_NAME=\1\nDYMSH_SERVICE_NAME=${SERVICE_NAME}  # added by dymsh util migrate/' .dymsh
	sed -i -r   's/^APP_NETWORK=(.*)$/APP_NETWORK=\1\nDYMSH_NETWORK_NAME=${APP_NETWORK}   # added by dymsh util migrate/' .dymsh

	FILES=($(ls -1 docker/service*.yml 2> /dev/null))
	test ${#FILES[@]} -gt 0 &&
	  sed -i -r 's/(\$\{?)IMG_NAME([^_a-zA-Z0-9].*|$)/\1DYMSH_IMAGE_NAME\2 /' "${FILES[@]}"

	# move .dymsh file
	mv .dymsh  .dymsh-init.sh.tmp
	mkdir -v ./.dymsh || fail 1 "Could not create .dymsh directory"
	echo "Moving previous .dymsh to .dymsh/init.sh"
	mv .dymsh-init.sh.tmp ./.dymsh/init.sh || fail 1 "Could not move previous .dymsh file"

	# move .yml files
	if ls docker/service*.yml > /dev/null; then
		echo "Moving YAML configuration files"
		mv -v docker/service*.yml  ./.dymsh/
	else
		echo "No YAML configuration files found in docker/"
	fi


	if [ $REPLACE_SOURCE = 1 ]; then
		FILES=($(ls -1 docker/create-service.sh docker/build-image.sh test/connect.sh test/start.sh 2> /dev/null))
		test ${#FILES[@]} -gt 0 &&
		sed -i -r 's@source ([a-zA-Z0-9_/\.-]+/)?.dymsh[ $]@source \1.dymsh/init.sh\1@' "${FILES[@]}"
	fi


	######################################################################
	# Conditional migration
	# docker and/or test directories or specific scripts only
	######################################################################

	echo "Moving other scripts or directories"


	# move docker directory or specific scripts
	if [ $MOVE_DOCKER = 1 ]; then
		test -d docker  &&  mv -v docker ./.dymsh/
	else
		move-f2d  ./.dymsh/docker  docker/build-image.sh docker/create-service.sh
	fi


	# move test directory or specific scripts
	if [ $MOVE_TEST = 1 ]; then
		test -d test    &&  mv -v test   ./.dymsh/
	else
		move-f2d  ./.dymsh/test  ./test/connect.sh ./test/start.sh
	fi

	git add .dymsh/
	git add docker/ 2>/dev/null
	git add test/   2>/dev/null
	true
}



migrate-v$(sed -r 's~\.[0-9]+$~~' <<< "${TARGET_VERSION}")
true
