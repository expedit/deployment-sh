#!/usr/bin/python2

import yaml
import json
import sys
import re
import os

regex1 = re.compile('^([^:]+):.*$')

docs = yaml.load(sys.stdin)

for k in docs['services']:
   s = docs['services'][k]
   for v in s['volumes']:
      if type(v) == str:
         m = re.search(regex1, v)
         if m:
            print(m.group(1))

'''
docker-compose config | 
python ~/devel/deployment-sh/scripts/util/yml-list-volumes-sources.py | 
xargs -l mkdir -p
'''
