#!/bin/bash
# echo "log   $0  vs.  ${BASH_SOURCE[@]}"
export DYMSH_ROOT=$(dirname "$BASH_SOURCE")/../

# Load internal scripts
source $DYMSH_ROOT/util/snippets/color

export START_TIME=${START_TIME:-$(date +%s.%N)}
export LC_ALL=en_US.UTF-8

 

timestamp_us()
{
	bc <<< "$(date +%s.%N) - $START_TIME"
}


printargs()
{
	echo ARGS
	while [ $# -gt 0 ]; do
		echo "-->$1"
		shift
	done
}


printfmt()
{
	# echo "ARGS" "$@"
	# printf "%8.3f   \e[0m"   $(timestamp_us)
	# printargs "[%8.3f] %s \e[0m\n" $(timestamp_us) "$*"
	printf "[%8.3f] %s\e[0m" $(timestamp_us) "$*"
}

printfmtn()
{
	printfmt "$@" $'\n'
}


# Write formatted log to stdout using one color for
# the whole line (defaults to blue).
# Usage: log [<color-name>] MESSAGE...
log()
{
	setcolor $1 && shift || setcolor --blue
	printfmtn "$@"
}

# Write formatted log to stderr using one color for
# the whole line (defaults to red).
# Usage: logerr [<color-name>] MESSAGE...
logerr()
{
	setcolor $1 && shift || setcolor --red
	printfmtn "$@" 1>&2
}


# Write formatted log to stdout using one color and 
# prefixing all lines from stdin with the given arguments
# Usage: logpipe [<color-name>] PREFIX...
logpipe()
{
	while IFS= read LINE; do
		echo -n "$(log "$@" "${LINE}")"
	done	
}


colorout()
{
	parseprint "$@"
	echo -e "\e[0m"
}

# indented_logpipe()
# {
# 	logpipe "${DYMSH_INDENT} ""$@"
# }

parseprint()
{
	ARGS=("$@")
	i=0
	while [ $i -lt $# ]; do
		ARG=${ARGS[i]}
		if setcolor "$ARG"; then
			true
		else
			echo -n "${ARG} "
		fi
		i=$((i+1))
	done	
}

# Usage: colorpipe [<color-name>] TEXT... ...
colorpipe()
{
	P=$(parseprint "$@")
	while read LINE; do
		echo -e "${P}${LINE}\e[0m"
	done
	# test -n "$LINE" && parseprint "$@"
	# echo "$LINE" | hexdump -C
	# test -n "${LINE}" && echo -ne "${P}${LINE}\e[0m"
}


# indented_colorpipe()
# {
# 	colorpipe "${DYMSH_INDENT}" "$@"
# }

# indented_colorlogpipe()
# {
# 	colorlogpipe "${DYMSH_INDENT}" "$@"
# }


# Write formatted log to stdout using multiple colors and 
# prefixing all lines from stdin with the given arguments
# Usage: colorlogpipe [<color-name>] TEXT... ...
colorlogpipe()
{
	while IFS= read LINE; do
		printfmtn "$(parseprint "$@")""${LINE}"
	done
	test -n "${LINE}" && printfmtn "$(parseprint "$@")""${LINE}"  # TODO check side effects
}
