#!/bin/bash

DYMSH_ROOT=$(dirname "$0")

usage()
{
	cat <<-EOF
	
	Usage: dymsh util variables [OPTIONS] -- PARAM...

	OPTIONS
	   -c, --check
	       Check if the configuration variables values are valid. They are
	       defined inside \`./.dymsh/init.sh\` script and may be required or
		   optional (see below).
	   -l, --list
	       List variables defined inside \`./.dymsh/init.sh\` script
	   -a, --list-all
	       List variables defined inside \`./.dymsh/init.sh\` script plus all 
	       variables defined by dymsh (DYMSH_* variables).
	   -t, --target <target-name>
	       The target of the service specific input configuration file. Useful
	       for different environments (e.g. dev, testing, staging, production).
	    --quoted
	       Show quoted string as value of variables. Used in conjuntion with
	       --list and --list-all.
	   -h, --help
	       Show usage help.
	PARAM...
	    Parameters to be forwarded to custom scripts, such as .dymsh/init.sh.

	The configuration variables are: DYMSH_PUSH_IMAGE, DYMSH_REGISTRY_DOMAIN, 
	DYMSH_PROJECT_NAME, DYMSH_SERVICE_NAME and DYMSH_NETWORK_NAME.
	EOF
	exit 255
}

_DYMSH_UTIL_VARIABLE_COMMAND=list

while [ $# -gt 0 -a "$1" != '--' ];
do
	case $1 in
		-c|--check)
			_DYMSH_UTIL_VARIABLE_COMMAND=check
			_DYMSH_UTIL_VARIABLE_KIND=conf
			shift
			;;
		-l|--list)
			_DYMSH_UTIL_VARIABLE_COMMAND=list
			_DYMSH_UTIL_VARIABLE_KIND=conf
			shift
			;;
		-a|--list-all)
			_DYMSH_UTIL_VARIABLE_COMMAND=list
			_DYMSH_UTIL_VARIABLE_KIND=all
			shift
			;;
		-t|--target)
			DYMSH_TARGET="$2"
			shift 2
			;;
		--quoted)
			QUOTED=1
			shift
			;;
		-h|--help)
			usage
			;;
		*)
			echo "Unknown command line parameter: $1" >&2
			exit 1
			;;
	esac
done
test $# -gt 0 && shift  # skip "--" from command line

source $DYMSH_ROOT/snippets/variables



command-list-vars()
{
	BEFORE=$(mktemp -u).env
	AFTER=$(mktemp -u).env

	case ${_DYMSH_UTIL_VARIABLE_KIND:-conf} in
		all)   # variables created by .dymsh/init.sh and dymsh
			list-vars | grep -v '^DYMSH_' > $BEFORE
			source ./.dymsh/init.sh "$@" 1>&2
			set-dymsh-vars "$@"
			list-vars | grep -v '^BASH_' > $AFTER
			;;
		conf)  # variables created by .dymsh/init.sh only
			list-vars > $BEFORE
			source ./.dymsh/init.sh "$@" 1>&2
			list-vars | grep -v '^BASH_' > $AFTER
			;;
		*)
			echo "You must specify --all or --conf."
			exit 1
			;;
	esac

	diff-vars $BEFORE $AFTER

	rm $BEFORE $AFTER
}



case $_DYMSH_UTIL_VARIABLE_COMMAND in
	check)
		check-vars "$@"
		;;
	list)
		command-list-vars "$@" | { test -n "$QUOTED" && env-add-quotes || cat; }
		;;
	*)
		echo "Unknown command."
		exit 1
		;;
esac

