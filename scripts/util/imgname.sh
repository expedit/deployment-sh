#!/bin/bash

DYMSH_ROOT=$(dirname "$BASH_SOURCE")/../

source $DYMSH_ROOT/util/log.sh

TAG_NAME=$($DYMSH_ROOT/util/tagname.sh)
test $? -eq 0 || exit 1

source ./.dymsh/init.sh "$@" 1>&2

test -n "$DYMSH_PROJECT_NAME" && DYMSH_PROJECT_NAME=$DYMSH_PROJECT_NAME/

# printenv | grep -v LS_COLORS | colorpipe --brown 1>&2

if [ "${DYMSH_PUSH_IMAGE:-1}" -eq 1 ]; then
	if [ -z "$DYMSH_REGISTRY_DOMAIN" ]; then
		DYMSH_REGISTRY_DOMAIN=registry:5000/
	else
		DYMSH_REGISTRY_DOMAIN=${DYMSH_REGISTRY_DOMAIN}/
	fi
	echo "${DYMSH_REGISTRY_DOMAIN}${DYMSH_PROJECT_NAME}$DYMSH_SERVICE_NAME:$TAG_NAME"
else
	echo "${DYMSH_PROJECT_NAME}$DYMSH_SERVICE_NAME:$TAG_NAME"
fi

