!#/usr/bin/python2

import yaml
import json
import sys
import re
import os

regex1 = re.compile('\${([A-Za-z_][A-Za-z_0-9]+)[:}]')
regex2 = re.compile('^([A-Za-z0-9_]+)$')
regex3 = re.compile('^([A-Za-z0-9_]+)=(.*)$')

vars = {}
def setvar(k, v):
   if k in vars:
      vars[k] = (vars[k][0]+1, v)
   else:
      vars[k] = (1, v)

def parse_envstr(s):
   # print 'at parse_envstr: s=', s
   m = re.search(regex2, s)
   if m:
      return (m.group(1), None)
   
   m = re.search(regex3, s)
   if m:
      return (m.group(1), m.group(2))

   return None

def parse_str(s):
   m = re.search(regex1, s)
   return m.group(1) if m else None


def inspect(node):
   if type(node) == str:
      # print "# finding interpolation at ", node
      m = re.search(regex1, node)
      if m:
         # print 'm=', m
         setvar(m.group(1), None)
   elif type(node) == bool:
      pass
   elif type(node) == int:
      pass
   elif type(node) == list:
      for e in node:
         # print 'e=', e
         inspect(e)
   else:
      for k in node:
         # print 'K=' + k
         if k == 'environment':
            environ = node[k]
            if type(environ) == list:
               for e in environ:
                  t = parse_envstr(e)
                  if t is not None:
                     setvar(t[0], t[1])
            elif type(environ) == dict:
               for k in environ:
                  v = environ[k]
                  setvar(k, parse_str(environ[k]))
            else:
               print ("error")
         else:
            inspect(node[k])


docs = yaml.load(sys.stdin)
inspect(docs['service'])

# print '----------------------'
for k in vars:
   if vars[k][1] is not None:
      print "%s %s" % (k, vars[k][1])
   else:
      print k
