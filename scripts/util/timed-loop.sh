#!/bin/bash

# params: MAX_TIME TEST_FUNCION 
timed_loop()
{
	if [ "$#" -lt 2 -a  "$1" ]
	then
		echo "$0 MAX_TIME TEST_FUNCION ARGS..."
		exit 1
	fi

	NEXT_WAIT_TIME=0
	X=$1
	MAX_WAIT_TIME=${X:-10}

	shift

	until "$@" || [ $NEXT_WAIT_TIME -eq $MAX_WAIT_TIME ];
	do
	   sleep $(( NEXT_WAIT_TIME++ ))
	done
	test $NEXT_WAIT_TIME -lt $MAX_WAIT_TIME
}
