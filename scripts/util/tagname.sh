
# Fails if HEAD does not exist yet
git rev-parse HEAD 1>/dev/null || exit 1

git-status-hash-helper()
{
    F=$(mktemp -u)
    mkfifo $F

    local OUT=$(git diff --numstat && git diff --staged --numstat )

    while read A M REST; do
        OUT=$(printf "%s\n" "${OUT}" | grep -v "$M")
    done < <(git submodule status | sed -r 's@^ @@')
    
    test -n "${OUT}" && printf "%s\n" "${OUT}"
}

# git-status-hash-helper
# exit

git-status-hash()
{
    NO_DATA_MD5=d41d8cd98f00b204e9800998ecf8427e
    read X Y < <(git-status-hash-helper | md5sum) &&
    test "$X" != $NO_DATA_MD5 &&
    echo -s${X:0:10}
}

# git-status-hash
# exit


GIT_BRANCH=${CI_BRANCH:-$(git rev-parse --abbrev-ref HEAD)}
GIT_COMMIT=${CI_COMMIT:-$(git rev-parse HEAD)}
GIT_COMMIT=${GIT_COMMIT:0:7}
GIT_STATUS=$(git-status-hash)

#####################
# tag-commit => git describe
# branch-commit => echo ...
IMG_TAG_NAME=$(git describe 2>/dev/null || echo $GIT_COMMIT)$GIT_STATUS

# Adds build number if exists
# [ -n "$CI_BUILD_NUMBER" ] && IMG_TAG_NAME=$IMG_TAG_NAME-build-$CI_BUILD_NUMBER

echo "$IMG_TAG_NAME"
