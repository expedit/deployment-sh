#!/bin/bash

source $DYMSH_ROOT/exp/snippets/fail
source $(dirname $0)/log.sh ||
	{ echo "Failed when loading logging script" 1>&2 ; exit 1; }


if [ -z "$SUBMODULE" ]; then
	# echo key is not specified, run ssh using default keyfile
	ssh "$@"
else
	SSH_KEY=./$SUBMODULE.pem
	chmod 400 $SSH_KEY

	# TODO verificar porque esta variavel ficou vazia na instance EC2 da AWS
	test -z "$SVM_HOSTNAME" && fail 1 "Invalid Source Versioning Manager hostname"

	ssh -i "$SSH_KEY" \
	    -o StrictHostKeyChecking=no \
	    -o UserKnownHostsFile=<(ssh-keyscan -t rsa,dsa,ecdsa $SVM_HOSTNAME) \
		"$@"
fi
