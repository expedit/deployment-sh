#!/usr/bin/python3

# This program read env vars from stdin, input YAML file from command line and
# write output YAML to stdout and additional messages/errors to stderr

import yaml
import json
import sys
import re
import os

regex1 = re.compile('\${([A-Za-z_][A-Za-z_0-9]+)[:}]')
regex2 = re.compile('^([A-Za-z0-9_]+)$')
regex3 = re.compile('^([A-Za-z0-9_]+)=(.*)$')

def parse_envstr(s):
   # print 'at parse_envstr: s=', s
   m = re.search(regex2, s)
   if m:
      return (m.group(1), None)   # only var name
   
   m = re.search(regex3, s)
   if m:
      return (m.group(1), m.group(2)) # name=value

   return None

# def parse_str(s):
#    m = re.search(regex1, s)
#    return m.group(1) if m else None


def replace_vars(s, flags = {}):
   # z = 'replace_vars: input=%s' % (s)
   # it = re.finditer(regex1, s)
   path = '*' if 'path' not in flags else flags['path']
   for m in regex1.finditer(s):
      var = m.group(1)
      if var in env:
         s = s.replace('${' + var + '}', env[var])
      else:
         # unset variables were being replaced by empty string in build.sh when docker-compose config was run.
         # this replacement was put here to keep track of the variable reference for future interpolation
         # by docker stack inside the server  -- TODO better solutions should be developed
         s = s.replace('${' + var + '}', '$$([DYMSH-UNSET-VAR-TRACKING]{' + var + '})')
         print("Unset variable '%s' in '%s'. It will be replaced dynamically." % (var, path), file=sys.stderr)

   if 'cwd' in flags and flags['cwd']:
      # sys.stderr.write( 'cwd %s\n' % os.getcwd() )
      # print( 'cwd %s\n' % os.getcwd() , file=sys.stderr)
      # sys.stderr.write( 'before %s\n' % s )
      s = re.sub('^\\./', os.getcwd() + '/', s)
   # z = z + ' output=%s' % (s)
      # sys.stderr.write( 'after  %s\n' % s )
   # print z
   return s

def inspect(node, path):
   # sys.stderr.write(path + '\n')
   if type(node) == str:
      flags = {'path':path}
      if re.match('.*/volumes\[\d+\]/source', path):
         flags['cwd'] = True
      return replace_vars(node, flags)
   elif type(node) == bool:
      return node
   elif type(node) == int:
      return node
   elif type(node) == list:
      res = []
      i = 0
      for e in node:
         # print 'e=', e
         res.append(inspect(e, path + '[' + str(i) + ']'))
         i = i + 1
      return res
   elif type(node) == dict:
      res = {}
      for k in node:
         # print 'K=' + k
         if k == 'environment':
            environ = node[k]
            if type(environ) == list:
               val = []
               i = 0
               for e in environ:
                  t = parse_envstr(e)
                  if t is None:
                     v2 = None
                  elif t[1] is None:
                     v2 = t[0] + '=' + env[t[0]]
                  else:
                     v2 = t[0] + '=' + replace_vars(t[1], {'path': path + '/%s[%d]' % (k, i)})
                  val.append(v2)
                  i = i + 1
               
            elif type(environ) == dict:
               val = {}
               for k2 in environ:
                  v = environ[k2]
                  if v is None:
                     val[k2] = env[k2]
                  else:
                     val[k2] = replace_vars(v, {'path':path + '/%s/%s' % (k,k2)})
            else:
               val = None

            res[k] = val
         else:
            res[replace_vars(k, {'path':path + '/' + k})] = inspect(node[k], path + '/' + k)
      return res
   else:
      return node
# end of  inspect(node, path):


# sys.stderr.write(sys.argv[1] + '\n')

# os.write(open(sys.stderr), sys.argv[1])

# the virtual environment which will be taken from stdin
env = {}

try:
   # Read variables from stdin
   for line in sys.stdin:
      kv = parse_envstr(line)
      env[kv[0]] = kv[1]
   #    print kv
   # print '----------------'

   docs = yaml.load(open(sys.argv[1]))
   # print("docs is:\n" + yaml.dump(docs, default_flow_style=False), file=sys.stderr)

   # handles the main service
   out = {
      'services': {
         env['DYMSH_SERVICE_NAME']: inspect(docs['service'], '/service')
      }
   }

   # handles additional services
   if 'services' in docs:
      for key in docs['services']:
         # print(key, file=sys.stderr)
         out['services'][key] = inspect(docs['services'][key], '/services/' + key)

   if 'volumes' in docs:
      out['volumes'] = inspect(docs['volumes'], '/volumes')

   if 'secrets' in docs:
      out['secrets'] = {}
      for key in docs['secrets']:
         # print('found: secrets.' + key, file=sys.stderr)
         out['secrets'][key] = inspect(docs['secrets'][key], '/secrets/' + key)

   # print("out is\n" + yaml.dump(out, default_flow_style=False), file=sys.stderr)

   print(yaml.dump(out, default_flow_style=False))

except Exception as e:
   # sys.stderr.write('ERROR: ' + str(e) + '\n')
   # sys.stderr.flush()
   import traceback
   traceback.print_exc()
   sys.exit(1)
