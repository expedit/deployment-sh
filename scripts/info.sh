#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh


############################
# CLI parsing and usage

usage()
{
	cat <<-EOF

	Usage: dymsh info [OPTIONS] [-- PARAM...]

	OPTIONS
	   -v
	       Be verbose. Same as -i -m -s
	   -s
	       Show short status (one line).
	   -m
	       Show submodules (commit SHA1, path and tag)
	   -i
	       Show the name of the image which would be generated
	   --only
	       Do not show info about children services/submodules,
	       i.e., disable recursion.
	   -h, --help
	       Show usage help.

	PARAM...
	   Parameters which will be forwarded to custom scripts, such as .dymsh.
	EOF
	exit 255
}

ALL_ARGS=("$@")

SHOW_SUBMODULE=0
SHOW_IMAGE_NAME=0
SHOW_STATUS=0
RECURSIVE=1

while test $# -gt 0 -a "$1" != '--'
do
	# echo "current parameter = $1"
	case $1 in
		-h|--help)
			usage
			;;
		-v)
			# echo 'verbose on'
			SHOW_SUBMODULE=1
			SHOW_IMAGE_NAME=1
			SHOW_STATUS=1
			shift
			;;
		-s)
			SHOW_STATUS=1
			shift
			;;
		-m)
			SHOW_SUBMODULE=1
			shift
			;;
		-i)
			SHOW_IMAGE_NAME=1
			shift
			;;
		--only)
			RECURSIVE=0
			shift;
			;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done
if [ $# -gt 0 ]; then
	shift
fi

# echo CUSTOM_ARGS $# "$@"
# echo ALL_ARGS ${#ALL_ARGS[@]} ${ALL_ARGS[@]}

######################


# Load .dymsh script of this repository
# echo source ./.dymsh/init.sh "$@"
source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"

checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1

DYMSH_NETWORK_NAME=${DYMSH_NETWORK_NAME:-appnet}

#####################
INDENT_TOKEN="  "  # "."
# DYMSH_INDENT=${DYMSH_INDENT:-1.}

TAG_NAME=$($DYMSH_ROOT/util/tagname.sh "$@")


if [ $RECURSIVE = 1 ];
then
	dymsh foreach $DYMSH_ROOT/info.sh --only "${ALL_ARGS[@]}" || fail 21
	exit 0
fi


show_status()
{
	if [ $SHOW_STATUS -eq 1 ]; then
		dymsh exp status --summary | colorpipe
	else
		echo "$DYMSH_SERVICE_NAME:$TAG_NAME" | colorpipe
	fi
}

show_image()
{
	if [ $SHOW_IMAGE_NAME -eq 1 ]; then
		DYMSH_IMAGE_NAME=$($DYMSH_ROOT/util/imgname.sh "$@")
		[ "$?" -eq 0 ] || fail 1
		echo "IMAGE: $DYMSH_IMAGE_NAME" | colorpipe --blue
	fi
}

show_submodules()
{
	test $SHOW_SUBMODULE -eq 0 && return
	git submodule status | colorpipe --blue 'MODULE: '
}


show_status
show_image
show_submodules
exit 0


# DEPRECATED

# i=0
git submodule status | while read MODULE
do
	# i=$((i+1))
	test $SHOW_SUBMODULE -eq 1 && echo "${MODULE}" | indented_logpipe
	f=$(echo "$MODULE" | awk '{print $2}')
	if [ -d $f -a -e $f/.git ]; then
		cd $f || fail 1 "Could not change to diretory: $f"

		if [ -f $DYMSH_ROOT/info.sh ];
		then
			DYMSH_INDENT="${DYMSH_INDENT}${INDENT_TOKEN}" $DYMSH_ROOT/info.sh "${ALL_ARGS[@]}" || fail 21
		fi
		cd - > /dev/null
	fi
done
