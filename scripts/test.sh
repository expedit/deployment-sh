#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/timed-loop.sh


############################
# CLI parsing and usage

usage()
{
	cat <<-EOF

	Usage: dymsh test [OPTIONS] [-- PARAM...]

	OPTIONS
	   -h, --help
	       Show usage help.

	PARAM...
	   Parameters which will be forwarded to custom scripts, such as .dymsh.
	EOF
	exit 255
}

ALL_ARGS=("$@")

while test $# -gt 0 -a "$1" != '--'
do
	# echo "current parameter = $1"
	case $1 in
		-h|--help)
			usage
			;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done
if [ $# -gt 0 ]; then
	shift
fi

# echo CUSTOM_ARGS $# "$@"
# echo ALL_ARGS ${#ALL_ARGS[@]} ${ALL_ARGS[@]}

######################


# Load .dymsh script of this repository
source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"

checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1


echo
echo
echo "========================================================="
echo "Starting tests for \"$DYMSH_SERVICE_NAME\"..."
echo "========================================================="
echo
echo


# connect_test() {
# 	./test/connect.sh "$@"
# }


# First connection to the service.
# Try some times before failing
if [ -x ./test/connect.sh ];
then
	echo -n "Waiting \"$DYMSH_SERVICE_NAME\" service to start..."
	timed_loop ${TEST_MAX_WAIT_TIME:-10} ./test/connect.sh "$@" || exit 1
	sleep 2
else
	echo "\"./test/connect.sh\" not found or not executable."
fi


FAILED=0
if [ -x ./test/start.sh ];
then
	./test/start.sh "$@"
	[ "$?" -eq 0 ] || FAILED=1
else
	echo "\"./test/start.sh\" not found or not executable."
fi




echo
echo
echo "========================================================="
[ "$FAILED" -eq 1 ] && echo "Tests have finished with errors."
[ "$FAILED" -eq 0 ] && echo "Tests have finished successfully."
echo "========================================================="
echo
echo

[ "$FAILED" -eq 1 ] && exit 1