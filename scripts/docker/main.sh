#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/snippets/array
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/timed-loop.sh


############################
# CLI parsing and usage

usage()
{
	cat $DYMSH_ROOT/docker/usage.txt
	exit 255
}

ALL_ARGS=("$@")

test $# = 0 && usage

if [ $# -gt 0 ]
then
	case $1 in
		-h|--help)
			usage
			;;
		help)
			shift
			test -n "$1" && dymsh docker "$1" --help
			exit
			;;
		build)
			shift 1
			dymsh docker build0 "$@"
			;;
		config)
			shift
			fail 128 "This code should not be reached"
			;;
		start)
			shift 1
			dymsh docker run0 "$@"
			;;
		stop)
			dymsh stop
			;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
fi
