#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/snippets/array
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/timed-loop.sh


############################
# CLI parsing and usage

usage()
{
	cat $DYMSH_ROOT/docker/run0/usage.txt
	exit 255
}


ALL_ARGS=("$@")
DYMSH_UPDATE=0
RECURSE=1
recursion_depth=-1

while test $# -gt 0 -a "$1" != '--'
do
	# echo "current parameter = $1"
	case $1 in
		# -u|--update)
		# 	DYMSH_UPDATE=1
		# 	shift
		# 	;;
		# --only)
		# 	RECURSE=0
		# 	shift
		# 	;;
		# --skip)
		# 	SKIP+=(${2//,/ })
		# 	shift 2
		# 	;;
		-d|--depth)
			test $recursion_depth -eq -1 && recursion_depth="$2"
			shift 2
			;;
		-t|--target)
			DYMSH_TARGET="$2"
			shift 2
			;;
		-h|--help)
			usage
			;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done
if [ $# -gt 0 ]; then
	shift
fi


test -n "$DYMSH_TARGET" -a \( ${#SKIP} -gt 0 -o ${RECURSE} = 0 -o "$DYMSH_UPDATE" = 1 \) && 
  fail 1 "--target option is incompatible with --update, --only and --skip options."



# Load .dymsh script of this repository
source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"
checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1


dymsh-stack-deploy()
{
	docker stack deploy -c <(dymsh docker config "$@") ${DYMSH_PROJECT_NAME} \
	  1> >(colorlogpipe --blue >&1) \
	  2> >(colorlogpipe --yellow >&2)
}



if [ -f "./.dymsh/service.yml" -o -f "./.dymsh/service.$DYMSH_TARGET.yml" ]; then
	log "Generating YML configuration..."
	test -n "$DYMSH_PROJECT_NAME" &&
	  log "Project name is '$DYMSH_PROJECT_NAME'" ||
	  { logerr "Project name was not set"; return 1; }
	if [ -n "$DYMSH_TARGET" ]; then   # TODO use dymsh docker config and docker stack up
		log "Target environment is '$DYMSH_TARGET'..."
		dymsh-stack-deploy -d $recursion_depth -t "$DYMSH_TARGET" -- "$@"
	elif [ -z "$DYMSH_TARGET" ]; then
		log "Target environment was not set..."
		dymsh-stack-deploy -d $recursion_depth -- "$@"
	fi
	sleep 1
	exit
fi

# echo skip: ${#SKIP[@]} "${SKIP[@]}"

# echo CUSTOM_ARGS $# "$@"
# echo ALL_ARGS ${#ALL_ARGS[@]} ${ALL_ARGS[@]}

######################



DYMSH_NETWORK_NAME=${DYMSH_NETWORK_NAME:-appnet}

test -n "$DYMSH_IMAGE_CHECK_FILE" && DYMSH_IS_ROOT_REPO=0 || DYMSH_IS_ROOT_REPO=1
test -n "$DYMSH_IMAGE_CHECK_FILE" || export DYMSH_IMAGE_CHECK_FILE=$(mktemp)


if [ $RECURSE -eq 1 ]; then

	echo
	echo
	echo
	log "========================================================="
	log "Running dependencies for \"$DYMSH_SERVICE_NAME\"..."
	log "========================================================="
	echo
	echo
	echo

	for f in $(git submodule foreach  | awk '{print $2}' | tr -d "'");
	do
		cd $f || fail 1 "Could not change to diretory: $f"

		# if [ -f $DYMSH_ROOT/run.sh ];
		# then
		# 	$DYMSH_ROOT/run.sh "${ALL_ARGS[@]}" || fail 1
		# fi
		dymsh docker start "${ALL_ARGS[@]}" || fail 1

		cd -
	done

else
	echo "Skipping dependencies due to --only flag."
fi



is_running()
{
	NAME=$1
	QTD=$2

	# docker service ls | grep -w $NAME #### debug TODO
	docker service ls | grep -w $NAME > /dev/null || return 1
	test -z "$QTD" -o "$QTD" = "0" && return 0;  # QTD not requested
	
	QTD=$(docker service ls \
		  | grep "[^:/\\]$NAME[^:/\\]" \
		  | awk '{print $4}' \
		  | grep -oE '[0-9]+' \
		  | head -1)
	echo $QTD
	test -n "$QTD" || return 1
	test $QTD -gt 0 
}



echo
echo
echo
log "========================================================="
log "Running \"$DYMSH_SERVICE_NAME\"..."
log "========================================================="
echo
echo
echo

DYMSH_IMAGE_NAME=$($DYMSH_ROOT/util/imgname.sh "$@")
[ "$?" -eq 0 ] || fail 1 "Could not generate image name."



# create network if it does not already exists
if docker network ls -f name=$DYMSH_NETWORK_NAME | grep $DYMSH_NETWORK_NAME > /dev/null;
then true
else docker network create --opt encrypted -d overlay $DYMSH_NETWORK_NAME | colorlogpipe
fi


# echo DYMSH_UPDATE=$DYMSH_UPDATE  is_running $DYMSH_IMAGE_NAME is $(is_running $DYMSH_SERVICE_NAME ; echo $?)


# if array_contains $DYMSH_SERVICE_NAME "${SKIP[@]}"; then
# 	echo contains
# else
# 	echo does not contain
# fi

if grep "$DYMSH_IMAGE_NAME" "$DYMSH_IMAGE_CHECK_FILE" > /dev/null;
then
	log "Image $DYMSH_IMAGE_NAME skipped: already started by this session."
elif [ $DYMSH_UPDATE -eq 1 ] && is_running $DYMSH_IMAGE_NAME;
then
	log "Image $DYMSH_IMAGE_NAME skipped: already running, started by another session."
	# echo "$DYMSH_IMAGE_NAME" >> $DYMSH_IMAGE_CHECK_FILE
elif array_contains $DYMSH_SERVICE_NAME "${SKIP[@]}";
then
	log "Image $DYMSH_IMAGE_NAME skipped: $DYMSH_SERVICE_NAME matches skip list (${SKIP[@]})."
else
	docker service rm $DYMSH_SERVICE_NAME | colorlogpipe


	echo -n "Waiting \"$DYMSH_SERVICE_NAME\" service to finish..." | colorlogpipe
	while is_running $DYMSH_SERVICE_NAME 1 > /dev/null;
	do sleep 1; echo -n .; done
	sleep 2
	echo


	log "Starting \"$DYMSH_SERVICE_NAME\" service..."


	if [ -f ./.dymsh/docker/create-service.sh ];
	then
		./.dymsh/docker/create-service.sh $DYMSH_IMAGE_NAME "$@" || fail 10
	else
		docker service create \
		   --name $DYMSH_SERVICE_NAME \
		   --replicas=1 \
		   --network $DYMSH_NETWORK_NAME \
			 $DYMSH_IMAGE_NAME || fail 10
	fi
	echo "$DYMSH_IMAGE_NAME" >> $DYMSH_IMAGE_CHECK_FILE




	echo -n "Waiting \"$DYMSH_SERVICE_NAME\" service to start..." | colorlogpipe
	timed_loop ${SERVICE_MAX_WAIT_TIME:-10} is_running $DYMSH_SERVICE_NAME 1 > /dev/null || 
	   fail 1 "Failed to create service: timed out."
	sleep 2
	echo


	docker service ls | logpipe
	docker service ps $DYMSH_SERVICE_NAME | logpipe
fi



if [ "$DYMSH_IS_ROOT_REPO" -eq 1 ]; then
	if [ -s $DYMSH_IMAGE_CHECK_FILE ]; then
		log --green "Summary of started images:"
		cat $DYMSH_IMAGE_CHECK_FILE | logpipe --brown
		rm $DYMSH_IMAGE_CHECK_FILE
	else
		log --green "No images started by this session."
	fi
fi

