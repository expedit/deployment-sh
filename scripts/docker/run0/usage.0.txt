
Usage: dymsh run [OPTIONS] -- PARAM...

OPTIONS
    --only
        Do not recurse through submodules.
    --skip SERVICENAME,...
        Skip the given services. Use this option when they are already
        running and were started using another image or service name, or
        even without docker). It is recommended for development purposes
        only or when you know what you are doing. Dependencies of the listed
        services will not be skipped.
    -u, --update
        Only restart (by removing and creating) a service if the currently
        running image does not match name:tag string of the new image.
        Service dependencies will not be skipped.
    -t, --target <target-name>
        The target of the service specific input configuration file. Useful
        for different environments (e.g. dev, testing, staging, production).
        Forces declarative running (YAML).
    -h, --help
        Show usage help.

PARAM...
    Parameters to be forwarded to custom scripts, such as .dymsh/init.sh.

