#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
source $DYMSH_ROOT/util/timed-loop.sh
source $DYMSH_ROOT/docker/build0/library.sh

(return 2>/dev/null) && fail 1 "This script cannot be sourced."


############################
# CLI parsing and usage

usage()
{
	cat $DYMSH_ROOT/docker/build0/usage.txt
	exit 255
}


ALL_ARGS=("$@")

DYMSH_BUILD__LOCAL=0
recursion_depth=-1
parallel=0
flags=

is-recursive()
{
	test "$recursion_depth" != 0
}

is-parallel()
{
	test "$parallel" = 1
}

is-verbose()
{
	test "$verbose" = 1
}


while test $# -gt 0 -a "$1" != '--'
do
	# echo "current parameter = $1"
	case $1 in
		-h|--help)
			usage
			;;
		-l|--local)
			DYMSH_BUILD__LOCAL=1
			flags+=l
			shift
			;;
		# --only)
		# 	recursion_depth=0
		# 	shift
		# 	if is-parallel; then
		# 		fail 1 '--only and --parallel are mutually exclusive.';
		# 	fi
		# 	;;
		-d|--depth)
			shift
			test $recursion_depth -eq -1 && recursion_depth="$1"
			shift
			# if is-parallel && ! is-recursive; then
			# 	fail 1 '--depth 0 and --parallel are mutually exclusive.';
			# fi
			;;
		# -p|--parallel)
		# 	flags+=p
		# 	parallel=1
		# 	shift
		# 	if ! is-recursive; then
		# 		fail 1 '--only or --depth 0 and --parallel are mutually exclusive.';
		# 	fi
		# 	;;
		-v|--verbose)
			verbose=1
			flags+=v
			shift
			;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done
if [ $# -gt 0 ]; then
	shift
fi


flags+=p
parallel=1


# echo CUSTOM_ARGS $# "$@"
# echo ALL_ARGS ${#ALL_ARGS[@]} ${ALL_ARGS[@]}

######################

if is-parallel; then
	# test "$recursion_depth" != -1 &&
	#   fail 1 "Parallel build not implemented for --depth."
	build-parallel $recursion_depth $flags  "$@"
else
	build-recursive $flags $recursion_depth  "$@"
fi
######################

