#!/bin/bash
DYMSH_SCRIPTS_ROOT=$(dirname "${BASH_SOURCE}")/../../

# Load internal scripts
source $DYMSH_SCRIPTS_ROOT/util/snippets/color
source $DYMSH_SCRIPTS_ROOT/exp/snippets/fail
source $DYMSH_SCRIPTS_ROOT/util/log.sh
# source $DYMSH_SCRIPTS_ROOT/build/library.sh

(return 2>/dev/null) || fail 1 "This script must be sourced."



build-echo()
{
	test "$DYMSH_BUILD__VERBOSE" = 1 && echo "$@" || true
}


# Usage:  build-only FLAGS PARAM...
# FLAGS:  l, v, vl, lv   => l=local (do not push),  v=verbose
build-only()
{
	local flags=$1
	shift
	DYMSH_BUILD__VERBOSE=$([[ $flags == *"v"* ]] && echo 1)

	# Load .dymsh script of this repository
	source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"

	checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1

	build-echo
	build-echo
	build-echo "========================================================="
	build-echo "Building image for \"$DYMSH_SERVICE_NAME\"..."
	build-echo "========================================================="
	build-echo

	# eval $(dymsh util variables -a --quoted)
	DYMSH_IMAGE_NAME=$($DYMSH_SCRIPTS_ROOT/util/imgname.sh "$@")
	[ "$?" -eq 0 ] || fail 1

	build-echo "Image $DYMSH_IMAGE_NAME"

	test -n "$DYMSH_IMAGE_CHECK_FILE" && DYMSH_IS_ROOT_REPO=0 || DYMSH_IS_ROOT_REPO=1
	test -n "$DYMSH_IMAGE_CHECK_FILE" || export DYMSH_IMAGE_CHECK_FILE=$(mktemp)

	if grep "$DYMSH_IMAGE_NAME" "$DYMSH_IMAGE_CHECK_FILE" > /dev/null; then
		build-echo "Image $DYMSH_IMAGE_NAME was already built."
	else
		# Print basic information about dymsh, git and docker
		printf "%-20s: %s\n" 'Dymsh Version'   "$(dymsh --version)"     > .dymsh-info.txt
		printf "%-20s: %s\n" 'Git Describe'    "$(git describe)"       >> .dymsh-info.txt
		printf "%-20s: %s\n" 'Docker Image'    "$(dymsh util imgname)" >> .dymsh-info.txt

		# Build image
		if [ -f ./.dymsh/docker/build-image.sh ];
		then
			./.dymsh/docker/build-image.sh $DYMSH_IMAGE_NAME "$@" || fail 10
		else
			docker build -t $DYMSH_IMAGE_NAME . || fail 10
		fi
		echo "$DYMSH_IMAGE_NAME" >> $DYMSH_IMAGE_CHECK_FILE

		# Push image
		DYMSH_PUSH_IMAGE=${DYMSH_PUSH_IMAGE:-1}
		if [[ $flags != *"l"* ]] && [ "$DYMSH_PUSH_IMAGE" = "1" ];
		then
			docker push $DYMSH_IMAGE_NAME || fail 11
			build-echo "Image built and pushed: $DYMSH_IMAGE_NAME"
		else
			build-echo "Image built but not pushed: $DYMSH_IMAGE_NAME"
		fi
		build-echo
	fi
}


# Usage: build-only-async FLAGS DIR LEN NAME INDEX RESULTS_FILE PARAM...
build-only-async()
(
	local flags=$1
	local dir=$2
	local len=$3
	local name=$4
	local index=$5
	local results_file=$6

	cd "$dir" &&
	shift 6 &&
	local out=$(printf "%s%${len}s%s: " "$(setcolor --brown)" "${name}" "$(resetcolor)") &&
	local err=$(printf "%s%${len}s: " "$(setcolor --red)" "${name}") &&
	build-only $flags "$@" 2> >(logpipe "$err" >&2) | logpipe "$out"
	echo "$i ${PIPESTATUS[0]}" >> $results_file
)


# Usage: build-parallel depth flags PARAM...
build-parallel()
{
	local recursion_depth=$1
	shift
	local flags=$1
	shift

	local paths=($(dymsh foreach -d $recursion_depth pwd))
	local names=($(for d in ${paths[@]}; do cd "$d" && source ./.dymsh/init.sh && echo "${DYMSH_SERVICE_NAME}"; done))
	local len=$(for s in "${names[@]}"; do echo ${#s}; done | sort -nr | head -n1)
	local results_file=$(mktemp)

	local i=0
	for d in ${paths[*]}
	do
		build-only-async $flags $d ${len} "${names[$i]}" $i "$results_file"   "$@" &
		((i++))
	done

	jobs
	wait
	echo

	log --brown "Building summary:"

	local fail_count=0
	while read I R; do
		test $R -ne 0 && fail_count=$((fail_count+1))
		test $R -ne 0 &&
		log --red "  ${names[$I]} failed with return code ${R}." ||
		log --green "  ${names[$I]} succeeded."
	done < $results_file

	resetcolor

	return $fail_count
}


# Usage: build-recursive flags recursion_depth PARAM...
build-recursive()
{
	local flags=$1
	local recursion_depth=$2
	shift 2
	DYMSH_BUILD__VERBOSE=$([[ $flags == *"v"* ]] && echo 1)

	build-only $flags "$@"

	test $recursion_depth -eq 0 && { log "Build stopped due to reached depth or --only flag." ; exit 0; }

	build-echo
	build-echo
	build-echo "========================================================="
	build-echo "Building dependencies for \"$DYMSH_SERVICE_NAME\"..."
	build-echo "========================================================="
	build-echo
	build-echo

	build-echo "Initializing submodules..."
	git submodule init || exit 1

	build-echo
	export GIT_SSH=$DYMSH_SCRIPTS_ROOT/util/ssh-git.sh

	build-echo
	for f in $(git submodule status | awk '{print $2}');
	do
		build-echo "submodule update -- $f"
		# pwd
		if [ -f $f.pem ]; then
			SUBMODULE=$f git submodule update -- $f || exit 1
		else
			git submodule update -- $f || exit 1
		fi

		build-echo
		build-echo "submodule status -- $f"
		if [ -f $f.pem ]; then
			SUBMODULE=$f git submodule status -- $f || exit 1
		else
			git submodule status -- $f || exit 1
		fi

		cd $f || fail 1 "Could not change to diretory: $f"

		# if [ -f $DYMSH_SCRIPTS_ROOT/build.sh ];
		# then
		# 	$DYMSH_SCRIPTS_ROOT/build.sh -d $((RECURSE-1)) "${ALL_ARGS[@]}" || fail 21
		# fi
		dymsh build -d $((recursion_depth-1)) "${ALL_ARGS[@]}" || fail 21
		cd -
	done

	build-echo
	build-echo
	build-echo


	if [ "$DYMSH_IS_ROOT_REPO" -eq 1 ]; then
		log --green "Summary of built images:"
		cat $DYMSH_IMAGE_CHECK_FILE | logpipe --brown
		rm $DYMSH_IMAGE_CHECK_FILE
	fi

}