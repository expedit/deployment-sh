#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/snippets/array
source $DYMSH_ROOT/util/snippets/child-process
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh
# source $DYMSH_ROOT/util/timed-loop.sh



############################
# CLI parsing and usage

usage()
{
	cat $DYMSH_ROOT/docker/logs/usage.txt
	exit 255
}


# chil-process-register-trap
# chil-process-register-trap
# chil-process-register-trap
# trap 'echo trap' EXIT
# trap -p
# exit 1

ALL_ARGS=("$@")
servicenames_cmd=false

while [ $# -gt 0 -a "$1" != '--' ]
do
	# echo "current parameter = $1"
	case $1 in
		-h|--help)
			usage
			;;
		-f|--follow|-t|--timestamps|--details|--no-resolve|--no-task-ids|--no-trunc|--raw)
			params+=("$1")
			shift
			;;
		-s|--since)
			params+=("--since" "$2")
			shift 2
			;;
		--tail)
			params+=("--tail" "$2")
			shift 2
			;;
		-[0-9]|-[0-9][0-9]|-[0-9][0-9][0-9]|-[0-9][0-9][0-9][0-9]) # matches -0 to -9999
			params+=("--tail" "${1/-/}")
			shift
			;;
		self)
			servicenames_cmd='echo $(get-cur-service-name)'
			shift
			;;
		all)
			servicenames_cmd='echo $(get-all-service-names)'
			shift
			;;
		default)
			servicenames_cmd='echo $(get-cur-service-name)'
			params+=(-f -t --no-task-ids --tail 100)
			shift
			;;
		*)
			if echo "$1" | grep -E '^[a-zA-Z0-9_-]+$' >/dev/null; then
				test "$servicenames_cmd" = false && servicenames_cmd="echo "
				test ${1:0:1} = _ &&
				  servicenames_cmd="$servicenames_cmd "'$(get-project-name)'"$1" ||
				  servicenames_cmd="$servicenames_cmd '$1'"
				shift
			else
				fail 128 "Unexpected parameter: $1"
			fi
			;;
	esac
done
if [ $# -gt 0 ]; then
	shift
fi

test "$servicenames_cmd" = false && usage

get-project-name()
{
	source ./.dymsh/init.sh;
	echo ${DYMSH_PROJECT_NAME}
}

get-cur-service-name()
{
	source ./.dymsh/init.sh;
	echo "${DYMSH_PROJECT_NAME}_${DYMSH_SERVICE_NAME}"
}

get-all-service-names()
{
	echo "Retrieving the list of all services... Please wait." >&2
	dymsh foreach -- bash -c 'source ./.dymsh/init.sh ;echo ${DYMSH_PROJECT_NAME}_${DYMSH_SERVICE_NAME}'
}

# Load .dymsh script of this repository
source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"
checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1

# echo $servicenames_cmd
servicenames="$(eval $servicenames_cmd)"

chil-process-register-trap

for s in $servicenames;
do
	child-process $s dymsh-logs -- docker service logs "${params[@]}" $s
done

sleep 1
# echo $PIDS
# echo $FIFOS

wait
