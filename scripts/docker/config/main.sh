#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/docker/config/library.sh
# source $DYMSH_ROOT/util/snippets/array
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/snippets/child-process
source $DYMSH_ROOT/util/semver.sh
# source $DYMSH_ROOT/util/timed-loop.sh



############################
# CLI parsing and usage

usage()
{
	cat $DYMSH_ROOT/docker/config/usage.txt
	exit 255
}


ALL_ARGS=("$@")
recursion_depth=-1

while [ $# -gt 0 -a "$1" != '--' ]
do
	case $1 in
		-h|--help)
			usage
			;;
		-d|--depth)
			shift
			test $recursion_depth -eq -1 && recursion_depth="$1"
			shift
			;;
		-t|--target)
			DYMSH_TARGET="$2"  # TODO temporary??
			shift 2
			;;
		-l|--list)
			;;
		--list-files)
			;;
		--list-vars)
			;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done
if [ $# -gt 0 ]; then
	shift
fi


source ./.dymsh/init.sh "$@" >&2 || fail 1 "Could not load ./.dymsh/init.sh"
checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1


###

foobar()
(
	cd "$1"
	source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"
	echo "Building configuration for '${DYMSH_SERVICE_NAME}'..." >&2
	yml-replace-vars-all || fail 1 "Could not build configuration."
)


# Params: depth 
config-parallel()
{
	local recursion_depth=$1
	local service_paths=($(dymsh foreach -d $recursion_depth pwd))
	local service_names=($(for d in ${service_paths[@]}; do cd "$d" && source ./.dymsh/init.sh && echo "${DYMSH_SERVICE_NAME}"; done))
	# echo service_names  "${service_names[@]}" >&2
	# echo service_paths  "${service_paths[@]}" >&2

	local i=0
	for d in ${service_paths[*]}
	do
		local name=${service_names[$i]}
		# build-only-async $flags $d ${len} "${service_names[$i]}" $i "$results_file"   "$@" &
		child-process "$name" config -s -sz -e "$(printf '%+10s:' "$name")" -o 'out' -- foobar "$d"
		((i++))
	done

	sleep 1
	wait
}



# TODO rename vars to DYMSH_DOCKER_CONFIG_* after dropping dymsh stack command
export DYMSH_STACK_TMPOUTDIR=$(mktemp -d)
export DYMSH_STACK_TMP_BASE_YML_LIST=${DYMSH_STACK_TMPOUTDIR}/base-yml-list.txt
export DYMSH_STACK_TMP_LBEL_YML_LIST=${DYMSH_STACK_TMPOUTDIR}/lbel-yml-list.txt
touch ${DYMSH_STACK_TMP_LBEL_YML_LIST} ${DYMSH_STACK_TMP_BASE_YML_LIST}

chil-process-register-trap
config-parallel $recursion_depth


# echo "Writing final configuration file to: ${DYMSH_STACK_TMPOUTDIR}"
RND=$(hexdump -n 8 -e '"%08x"' /dev/urandom)
OFS=()

OF=./${RND}.${DYMSH_PROJECT_NAME}.yml
yml-merge $(cat ${DYMSH_STACK_TMP_BASE_YML_LIST}) > $OF
OFS+=($OF)
DKC_INPUT="-f $OF"

if [ -n "${DYMSH_TARGET}" ]; then
	OF=./${RND}.${DYMSH_PROJECT_NAME}.${DYMSH_TARGET}.yml
	yml-merge $(cat ${DYMSH_STACK_TMP_LBEL_YML_LIST}) > $OF
	OFS+=($OF)
	DKC_INPUT="$DKC_INPUT -f $OF"
fi

set -o pipefail
if docker-compose --log-level ERROR ${DKC_INPUT}  config |
	sed -r 's~\$\$\(\[DYMSH-UNSET-VAR-TRACKING\]\{([a-zA-Z0-9_]+)\}\)~${\1}~g'  #1>/tmp/docker-compose-config.out.yml
then
	# INPUTFILES=(${DKC_INPUT})#
	# meld ${INPUTFILES[1]} /tmp/docker-compose-config.out.yml
	rm "${OFS[@]}"
else
	tmpdir=$(mktemp -d)/
	mv "${OFS[@]}" $tmpdir
	logerr "The configuration building has failed with the above error and the intermediate files are:"
	find "$tmpdir" -type f | xargs -l1 echo '  - ' | colorlogpipe --red >&2
	exit 1
fi

sleep 1
