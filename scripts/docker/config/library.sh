

has-target()
{
	test -n "$DYMSH_TARGET"
}

has-label()
{
	test -n "${DYMSH_TARGET}" -a -e "./.dymsh/service.${DYMSH_TARGET}.yml"
}



yml-merge()
{
	export DYMSH_NETWORK_NAME
	$DYMSH_ROOT/util/yml-merge-services.py "$@"
	# cat "$@"
}


# Params:   [target]
# Environ:  DYMSH_STACK_TMPOUTDIR, DYMSH_PROJECT_NAME, DYMSH_SERVICE_NAME,
#           DYMSH_STACK_TMP_BASE_YML_LIST, DYMSH_STACK_TMP_LBEL_YML_LIST,
#           DYMSH_ROOT
yml-replace-vars()
{
	test -n "$DYMSH_STACK_TMPOUTDIR" \
	  -a -d "$DYMSH_STACK_TMPOUTDIR" \
	  -a -n "$DYMSH_PROJECT_NAME" \
	  -a -n "$DYMSH_SERVICE_NAME" \
	  -a -n "$DYMSH_STACK_TMP_BASE_YML_LIST" \
	  -a -n "$DYMSH_STACK_TMP_LBEL_YML_LIST" \
	  -a -f "$DYMSH_STACK_TMP_BASE_YML_LIST" \
	  -a -f "$DYMSH_STACK_TMP_LBEL_YML_LIST" \
	  -a -n "$DYMSH_ROOT" || return 1

	if [ "$1" = '' ]; then
		IF=./.dymsh/service.yml
		OF=${DYMSH_STACK_TMPOUTDIR}/${DYMSH_PROJECT_NAME}-${DYMSH_SERVICE_NAME}.yml
		echo $OF >> $DYMSH_STACK_TMP_BASE_YML_LIST
	else
		IF=./.dymsh/service.$1.yml
		OF=${DYMSH_STACK_TMPOUTDIR}/${DYMSH_PROJECT_NAME}-${DYMSH_SERVICE_NAME}.$1.yml
		echo $OF >> $DYMSH_STACK_TMP_LBEL_YML_LIST
	fi

	$DYMSH_ROOT/util/yml-replace-vars.py $IF > $OF
	# echo "$OF" >&2
}


# Params:   none
# Environ:  DYMSH_TARGET (optional)
yml-replace-vars-all()
{
	dymsh_util_variables_cmd_args=(dymsh util variables --list-all)
	has-target && dymsh_util_variables_cmd_args+=(--target "$DYMSH_TARGET")

	"${dymsh_util_variables_cmd_args[@]}" -- "$@" | yml-replace-vars || return
	if has-label; then
		"${dymsh_util_variables_cmd_args[@]}" -- "$@" | yml-replace-vars ${DYMSH_TARGET} || return
	fi
}

