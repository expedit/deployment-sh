#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh


############################
# CLI parsing and usage

usage()
{
	cat <<-EOF

	Usage: dymsh docker stop [OPTIONS] [-- PARAM...]

	Stop all services started by "dymsh docker start" command.

	OPTIONS
	   -h, --help
	       Show usage help.

	PARAM...
	   Parameters to be forwarded to custom scripts, such as .dymsh/init.sh.
	EOF
	exit 255
}

ALL_ARGS=("$@")
RECURSE=1


while test $# -gt 0 -a "$1" != '--'
do
	case $1 in
		-h|--help)
			usage
			;;
		# --only)
		# 	RECURSE=0
		# 	shift
		# 	;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done
if [ $# -gt 0 ]; then
	shift
fi

# echo CUSTOM_ARGS $# "$@"
# echo ALL_ARGS ${#ALL_ARGS[@]} ${ALL_ARGS[@]}

######################


# Load .dymsh script of this repository
source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"

checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1


if docker stack ls | grep $DYMSH_PROJECT_NAME >/dev/null; then
	docker stack rm ${DYMSH_PROJECT_NAME}
	# dymsh stack down -p "$DYMSH_PROJECT_NAME"   # replaced on 2019-10-14
	exit
fi

false || fail 1 "Could not find service stack to be stopped"



echo
echo
echo
echo "========================================================="
echo "Stopping \"$DYMSH_SERVICE_NAME\"..."
echo "========================================================="
echo

docker service rm $DYMSH_SERVICE_NAME


test $RECURSE -eq 1 || exit 0

echo
echo
echo
echo "========================================================="
echo "Stopping dependencies for \"$DYMSH_SERVICE_NAME\"..."
echo "========================================================="
echo

for f in $(git submodule foreach  | awk '{print $2}' | tr -d "'");
do
	cd $f
	$DYMSH_ROOT/stop.sh "${ALL_ARGS[@]}" || fail 1
	cd -
done


