#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/submodule.sh
source $DYMSH_ROOT/util/semver.sh



############################
# CLI parsing and usage

usage()
{
	cat <<-EOF

	Usage: dymsh check [OPTIONS] [-- PARAM...]

	OPTIONS
	   --chain
	       Fetch repositories of current service and its dependencies.
	   --services
	       Check if working tree and index are clean wrt service dependencies.
	   --index
	       Check if index (=cache) is clean.
	   --worktree
	       Check if working tree is clean.
	   --strict
	       Check everything (--worktree, --index, --services).
	   -h, --help
	       Show usage help.

	PARAM...
	   Parameters which will be forwarded to custom scripts, such as .dymsh.
	EOF
	exit 255
}

ALL_ARGS=("$@")

FETCH=0
CHECK_SERVICES=0
CHECK_INDEX=0
CHECK_WORKTREE=0


while test $# -gt 0 -a "$1" != '--'
do
	# echo "current parameter = $1"
	case $1 in
		-h|--help)
			usage
			;;
		--chain)
			FETCH=1
			shift
			;;
		--services)
			CHECK_SERVICES=1
			shift
			;;
		--index)
			CHECK_INDEX=1
			shift
			;;
		--worktree)
			CHECK_WORKTREE=1
			shift
			;;
		--strict)
			CHECK_SERVICES=1
			CHECK_INDEX=1
			CHECK_WORKTREE=1
			shift
			;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done
if [ $# -gt 0 ]; then
	shift
fi

# echo CUSTOM_ARGS $# "$@"
# echo ALL_ARGS ${#ALL_ARGS[@]} ${ALL_ARGS[@]}

######################


# Fetch the whole project (all repositories, including submodules)
if [ $FETCH -eq 1 -a -z "$DYMSH_INDENT" ]; then
	FETCH_TMP_FILE=$(mktemp)
	log "Fetching project and redirecting stdout to $FETCH_TMP_FILE"
	$DYMSH_ROOT/fetch.sh -- "$@" > $FETCH_TMP_FILE
	if [ $? -eq 0 ]; then
		log "Fetching finished successfully."
	else
		cat $FETCH_TMP_FILE
		fail 1 "Fetch failed!"
	fi
fi

if ! dymsh foreach -- dymsh util variables --check >/dev/null 2>/dev/null;
then
	dymsh foreach -v -- dymsh util variables --check
	exit 1
fi


# Load .dymsh script of this repository
source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"

checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1


INDENT_TOKEN="  "


DYMSH_NETWORK_NAME=${DYMSH_NETWORK_NAME:-appnet}

DYMSH_IMAGE_NAME=$($DYMSH_ROOT/util/imgname.sh "$@")
[ "$?" -eq 0 ] || fail 1 "Could not determine image name."

test -z "$BASE_PATH" && export BASE_PATH=$(dirname $(pwd))/

test -n "$DYMSH_IMAGE_CHECK_FILE" || export DYMSH_IMAGE_CHECK_FILE=$(mktemp)


if grep "$DYMSH_SERVICE_NAME" "$DYMSH_IMAGE_CHECK_FILE" > /dev/null; then DYMSH_MMM=rest ; else DYMSH_MMM=first ; fi

echo "$DYMSH_IMAGE_NAME" \
     $(sed -r 's/([a-z0-9_-]+):([0-9\.a-z_-]+)$/\1 \2/i' <<< "$DYMSH_IMAGE_NAME") \
     "${PWD#$BASE_PATH}" \
     "$DYMSH_SERVICE_NAME" >> $DYMSH_IMAGE_CHECK_FILE


test -n "$DYMSH_SERVICE_CHECK_FILE" || export DYMSH_SERVICE_CHECK_FILE=$(mktemp)


show_changes()
{   # 1 * $ISCHG_S + 2 * $ISCHG_I + 4 * $ISCHG_W
	A=()
	test $(($1 & 4)) -eq 4 && A+=('service(s)')
	test $(($1 & 2)) -eq 2 && A+=('index')
	test $(($1 & 1)) -eq 1 && A+=('working tree')

	local IFS=', '
	echo -n "${A[*]}" | sed -r 's/,/, /g'
	# echo STATUS=$1
}

DYMSH_MMM=$DYMSH_MMM dymsh exp status --summary | colorpipe --cyan "${DYMSH_INDENT}"
STATUS=${PIPESTATUS[0]}
test $STATUS -gt 0 && 
  echo $STATUS "$DYMSH_SERVICE_NAME has modifications on $(show_changes $STATUS)." >> $DYMSH_SERVICE_CHECK_FILE



for f in $(git submodule status | awk '{print $2}');
do
	if [ -d $f -a -e $f/.git ]; then
		cd $f || fail 1 "Could not change to diretory: $f"

		if [ -f $DYMSH_ROOT/check.sh ];
		then
			# echo BEFORE "%$DYMSH_INDENT%"
			DYMSH_INDENT="${DYMSH_INDENT}${INDENT_TOKEN}" $DYMSH_ROOT/check.sh "${ALL_ARGS[@]}" || fail 21
			# echo AFTER " %$DYMSH_INDENT%"
		fi
		cd - > /dev/null
	fi
done



if [ -z "$DYMSH_INDENT" ]; then
	echo
	cat $DYMSH_IMAGE_CHECK_FILE | cut -d' ' -f1 | sort | uniq | colorpipe --blue

	FAILED=0

	test -s "$DYMSH_SERVICE_CHECK_FILE" && {
		echo
		colorout --red "Per-service status modifications:"

		while read STATUS LINE; do
			STATUS_OR=$((${STATUS_OR:-0} | $STATUS))
			echo $LINE
		done < $DYMSH_SERVICE_CHECK_FILE > >(colorpipe --red "  ")
		# echo $STATUS_OR
		
		test $(($STATUS_OR & 4)) -eq 4  -a  $CHECK_SERVICES -eq 1 && FAILED=1
		test $(($STATUS_OR & 2)) -eq 2  -a  $CHECK_INDEX    -eq 1 && FAILED=1
		test $(($STATUS_OR & 1)) -eq 1  -a  $CHECK_WORKTREE -eq 1 && FAILED=1
	}


	F=$(mktemp)
	while read DYMSH_IMAGE_NAME SVC VER REPO_PATH SRVNAME; do
		# log "$SVC:$VER"
		if [ "$SVC" = "$LAST_SVC" -a "$VER" != "$LAST_VER" ]; then
			colorout --red "Failed: $SRVNAME"
			colorout --red "  At $REPO_PATH:"
			colorout --red "     Current Image: $DYMSH_IMAGE_NAME"
			colorout --red "  At $LAST_REPO_PATH:"
			colorout --red "     Last Image: $LAST_IMG_NAME"
			colorout --red ""
		    FAILED=1
		fi
		LAST_IMG_NAME=$DYMSH_IMAGE_NAME
		LAST_VER=$VER
		LAST_SVC=$SVC
		LAST_REPO_PATH=$REPO_PATH
		LAST_SRVNAME=$SRVNAME
	done > $F < <(cat $DYMSH_IMAGE_CHECK_FILE | sort | uniq)

	test -s "$F" && {
		echo
		colorout --red "Services mismatch:";
		cat $F | colorpipe --red "  ";
	}


	rm $DYMSH_IMAGE_CHECK_FILE $DYMSH_SERVICE_CHECK_FILE $F

	echo
	if [ "$FAILED" -eq 0 ]; then
		colorout --green "Project is valid for deployment."
	else
		colorout --red "Project is invalid for deployment."
		exit 1
	fi

fi
