#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../


# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh


# Parse the command line
DYMSH_STACK_COMMAND=build
source $DYMSH_ROOT/stack/cmdparse.sh

# printenv|grep DYMSH_

source ./.dymsh/init.sh "$@" >&2 || fail 1 "Could not load ./.dymsh/init.sh"
checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1


#####################

has-target()
{
	test -n "${DYMSH_TARGET}"
}

has-label()
{
	test -n "${DYMSH_TARGET}" -a -e "./.dymsh/service.${DYMSH_TARGET}.yml"
}

ls-yml-files()
{
	echo ./.dymsh/service.yml
	if has-label; then
		echo ./.dymsh/service.${DYMSH_TARGET}.yml
	fi
}

list-yml-vars()
{
	"$DYMSH_ROOT/util/yml-search-vars.py" < ./.dymsh/service.yml
	if has-label; then
		"$DYMSH_ROOT/util/yml-search-vars.py" < ./.dymsh/service.${DYMSH_TARGET}.yml
	fi
}


if [ "$DYMSH_STACK_TMPOUTDIR" = '' ];
then
	export DYMSH_STACK_TMPOUTDIR=$(mktemp -d)
	export DYMSH_STACK_TMP_BASE_YML_LIST=${DYMSH_STACK_TMPOUTDIR}/base-yml-list.txt
	export DYMSH_STACK_TMP_LBEL_YML_LIST=${DYMSH_STACK_TMPOUTDIR}/lbel-yml-list.txt
	touch ${DYMSH_STACK_TMP_LBEL_YML_LIST}
fi



yml-replace-vars()
{
	if [ "$1" = '' ]; then
		IF=./.dymsh/service.yml
		OF=${DYMSH_STACK_TMPOUTDIR}/${DYMSH_PROJECT_NAME}-${DYMSH_SERVICE_NAME}.yml
		echo $OF >> $DYMSH_STACK_TMP_BASE_YML_LIST
	else
		IF=./.dymsh/service.$1.yml
		OF=${DYMSH_STACK_TMPOUTDIR}/${DYMSH_PROJECT_NAME}-${DYMSH_SERVICE_NAME}.$1.yml
		echo $OF >> $DYMSH_STACK_TMP_LBEL_YML_LIST
	fi

	$DYMSH_ROOT/util/yml-replace-vars.py $IF > $OF
	# echo "$OF" >&2
}


yml-replace-vars-all()
{
	dymsh_util_variables_cmd_args=(dymsh util variables --list-all)
	has-target && dymsh_util_variables_cmd_args+=(--target "$DYMSH_TARGET")

	"${dymsh_util_variables_cmd_args[@]}" -- "$@" | yml-replace-vars || return
	if has-label; then
		"${dymsh_util_variables_cmd_args[@]}" -- "$@" | yml-replace-vars ${DYMSH_TARGET} || return
	fi
}

# set -x
echo "Building configuration for '${DYMSH_SERVICE_NAME}'..." >&2
yml-replace-vars-all || fail 1 "Could not build configuration."
# exit

# Enter submodules
for f in $(git submodule foreach  | awk '{print $2}' | tr -d "'");
do
	cd $f || fail 1 "Could not change to diretory: $f"

	# if [ -f $DYMSH_ROOT/stack/main.sh ];
	# then
	# 	echo DYMSH_IS_NOT_ROOT=1 $DYMSH_ROOT/stack/main.sh "${ALL_ARGS[@]}" || fail 1
	# fi
    DYMSH_IS_NOT_ROOT=1 dymsh stack build "${ALL_ARGS[@]}" || fail 1 "Failed to build configuration for '$f'"

	cd - > /dev/null
done



yml-merge()
{
	export DYMSH_NETWORK_NAME
	$DYMSH_ROOT/util/yml-merge-services.py "$@"
	# cat "$@"
}


if [ "$DYMSH_IS_NOT_ROOT" != 1 ]  # if is root service
then
	#
	# echo "Writing final configuration file to: ${DYMSH_STACK_TMPOUTDIR}"
	RND=$(hexdump -n 8 -e '"%08x"' /dev/urandom)
	OFS=()

	OF=./${RND}.${DYMSH_PROJECT_NAME}.yml
	yml-merge $(cat ${DYMSH_STACK_TMP_BASE_YML_LIST}) > $OF
	OFS+=($OF)
	DKC_INPUT="-f $OF"

	if has-target; then
		OF=./${RND}.${DYMSH_PROJECT_NAME}.${DYMSH_TARGET}.yml
		yml-merge $(cat ${DYMSH_STACK_TMP_LBEL_YML_LIST}) > $OF
		OFS+=($OF)
		DKC_INPUT="$DKC_INPUT -f $OF"
	fi

	if docker-compose --log-level ERROR ${DKC_INPUT}  config |
	   sed -r 's~\$\$\(\[DYMSH-UNSET-VAR-TRACKING\]\{([a-zA-Z0-9_]+)\}\)~${\1}~g'  #1>/tmp/docker-compose-config.out.yml
	then
		# INPUTFILES=(${DKC_INPUT})#
		# meld ${INPUTFILES[1]} /tmp/docker-compose-config.out.yml
		rm "${OFS[@]}"
	else
		tmpdir=$(mktemp -d)/
		mv "${OFS[@]}" $tmpdir
		{
		echo
		echo "The configuration building has failed with the above error and the intermediate files are:"
		find "$tmpdir" -type f
		} >&2
		exit 1
	fi
fi
