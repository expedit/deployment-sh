#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../


# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh


# Parse the command line
source $DYMSH_ROOT/stack/cmdparse.sh



# Load .dymsh script of this repository
load-dymsh()
{
	if [ -z "$DYMSH_PROJECT_NAME" ]; then   # project name was not given
		if [ -f ./.dymsh/init.sh ]; then
			source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh";
		else
			fail 1 "This command must be run from a dymsh project directory when -p is not given"
		fi
	fi
}



case $DYMSH_STACK_COMMAND in
	build)
		fail 1 "this code should not be reached"
		;;
	up)
		DYMSH_COMMAND+=" up"
		load-dymsh
		test -n "$DYMSH_STACK_COMPOSE_YML" || fail 3 "YAML compose file is required. Use --file option."
		docker stack deploy -c ${DYMSH_STACK_COMPOSE_YML} ${DYMSH_PROJECT_NAME}
		exit
		;;
	down)
		DYMSH_COMMAND+=" down"
		load-dymsh
		docker stack rm ${DYMSH_PROJECT_NAME}
		exit
		;;
	status)
		DYMSH_COMMAND+=" status"
		load-dymsh
		echo -e 'SERVICES:\n'
		docker stack services ${DYMSH_PROJECT_NAME}
		echo -e '\nCONTAINERS:\n'
		docker stack ps ${DYMSH_PROJECT_NAME}
		echo
		exit 
		;;
	*)
		usage
		;;
esac
