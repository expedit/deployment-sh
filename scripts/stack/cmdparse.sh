############################
# CLI parsing and usage help

usage()
{
	cat $DYMSH_ROOT/stack/usage.txt
	exit 255
}


ALL_ARGS=("$@")

while test $# -gt 0 -a "$1" != '--'
do
	# echo "current parameter = $1"
	case $1 in
		build|up|down|status)
			DYMSH_STACK_COMMAND="$1"
			shift
			;;
		-t|--target)
			DYMSH_TARGET="$2"  # TODO temporary??
			test "$DYMSH_STACK_COMMAND" != 'build' && fail 2 "Target is valid for build command only"
			shift 2
			;;
		-p|--project)
			DYMSH_PROJECT_NAME="$2"
			shift 2
			test -n "$DYMSH_PROJECT_NAME" || fail 1 "Invalid name for project: $DYMSH_PROJECT_NAME"
			test "$DYMSH_STACK_COMMAND" = 'build' -o "$DYMSH_STACK_COMMAND" = '' && fail 2 "Project name is valid for up, down and status commands only"
			;;
		-f|--file)
			DYMSH_STACK_COMPOSE_YML="$2"
			test "$DYMSH_STACK_COMMAND" != 'up' && fail 2 "--file is valid for up command only"
			shift 2
			;;
		-h|--help)
			usage
			;;
		*)
			fail 128 "Unexpected parameter: $1"
			;;
	esac
done
if [ $# -gt 0 ]; then
	shift    # skip "--" from command line
fi
