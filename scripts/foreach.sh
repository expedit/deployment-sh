#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh

############################
# CLI parsing and usage

usage()
{
	cat <<-EOF

	Usage: dymsh foreach [OPTIONS] [ORDER_OPTIONS]
	       dymsh foreach [OPTIONS] [PRE_ORDER_COMMAND] [-- POST_ORDER_COMMAND]

	OPTIONS
	  -d NUMBER
	     The max recursion depth. NUMBER is 0 for the root repo only, 1 for
	     direct children, etc.
	  -m all|first|rest
	     The matching mode for submodules when DYMSH_SERVICE_NAME is not unique.
	     Matched submodules are ellegible for command execution.
	     - all: all submodules are match (the default)
	     - first: only the first submodule is match
	     - rest: all but the first submdule are match.
	  -f REGEX
	     Filter submodules by the given regular expression.
	     Matched submodules are ellegible for command execution.
	  -i, --interactive
	     Commands to be executed are interactive (receive user's input from
	     keyboard). This prevent terminal synchronization issues.
	  -s, --shell
	     Each command parameter is handled as a string to be executed inside
	     a shell. The default behavior is to see each command as an array, where
	     the first element is the program to be run.
	  -h, --help
	     Show usage help.
	  -v, -vv
	     Be verbose or very verbose

	ORDER_OPTIONS
	  --pre-order COMMAND
	     Execute the COMMAND at pre-order tree traversal
	  --post-order COMMAND
	     Execute the COMMAND at post-order tree traversal

	PRE_ORDER_COMMAND
	  The command to be executed at pre-order tree traversal

	POST_ORDER_COMMAND
	  The command to be executed at post-order tree traversal

	Examples:
	  dymsh foreach echo my pre order command -- echo my post order command
	  dymsh foreach --pre-order echo preordercmd --post-order echo postordercmd
	  dymsh foreach --post-order echo post --pre-order echo pre
	  dymsh foreach -s echo 1 \&\& echo 2 -- 'echo 3 && echo 4'
	EOF
	exit 255
}

ALL_ARGS=("$@")
PRE_ORDER_CMD=()
POS_ORDER_CMD=()
TRAVERSAL_MODE=
SM_MATCH_MODE=all
SM_MATCH_REGEX=
VERBOSE=0

OPTION_CONFLICT_MSG="Remember --pre-order/--post-order and -- cannot be used together"

max_recursion_depth=100

while test $# -gt 0
do
	# echo "current parameter = $1"
	case $1 in
	-h|--help)
		usage
		;;
	-d)
		shift
		max_recursion_depth="$1"
		test "$max_recursion_depth" -gt 100 -o "$max_recursion_depth" -lt 0 && max_recursion_depth=100
		shift
		;;
	-m)
		shift
		SM_MATCH_MODE=$1
		shift
		;;
	-f)
		shift
		SM_MATCH_REGEX=$1
		shift
		;;
	-i|--interactive)
		shift
		INTERACTIVE=1
		;;
	-v)
		VERBOSE=1
		shift
		;;
	-vv)
		VERBOSE=2
		shift
		;;
	-s|--shell)
		USE_SUBSHELL=1
		shift
		;;
	*)
		while test $# -gt 0
		do
			case $1 in
			--pre-order)
				test "${#PRE_ORDER_CMD[@]}" -gt 0 \
				  && fail 1 "Invalid option $1. $OPTION_CONFLICT_MSG"
				TRAVERSAL_MODE=pre
				shift
				;;

			--post-order)
				TRAVERSAL_MODE=post
				shift
				;;
			--)
				test -z "$TRAVERSAL_MODE" || fail 1 "Invalid option $1. $OPTION_CONFLICT_MSG"
				TRAVERSAL_MODE=post
				shift
				;;
			*)
				test "$TRAVERSAL_MODE" = pre && PRE_ORDER_CMD+=("$1")
				test "$TRAVERSAL_MODE" = post && POS_ORDER_CMD+=("$1")
				test -z "$TRAVERSAL_MODE" && PRE_ORDER_CMD+=("$1")
				shift
				;;
			esac
		done
		;;
	esac
done


verbose1()
{
	test $VERBOSE = 1 -o $VERBOSE = 2
}

verbose2()
{
	test $VERBOSE = 2
}

roots_relative_path()
{
	pwd | sed -r "s~$PROJEC_ROOT_PATH~~g" -
}

is_root_repo()
{
	test $DYMSH_IS_ROOT_REPO -eq 1
}

out()
{
	# echo -n "$A""$@" | hexdump -C
	# return 0

	# if [ "$1" = "%" ]; then
	# 	A="${DYMSH_INDENT%??}+ ";
	# 	shift
	# else
		A="${DYMSH_INDENT}"
	# fi

	# ARGS=("$@")

	colorout "$A""$@"
	# echo
}

outpipe()
{
	while IFS= read -r LINE; do
		echo "${DYMSH_INDENT}$LINE";
	done
}

# exit

######################
INDENT_TOKEN="  "

test -n "$DYMSH_TRAVERSAL_FILE" && DYMSH_IS_ROOT_REPO=0 || DYMSH_IS_ROOT_REPO=1
test -n "$DYMSH_TRAVERSAL_FILE" || export PROJEC_ROOT_PATH=$(dirname $(pwd))/


if is_root_repo; then
	verbose1 && out "PRE-order command:  ${PRE_ORDER_CMD[@]}" | colorpipe --brown 1>&2

	verbose1 && out "POST-order command: ${POS_ORDER_CMD[@]}" | colorpipe --brown 1>&2

	verbose1 && echo 1>&2

	export DYMSH_RECURSION_DEPTH=0
fi


test -n "$DYMSH_TRAVERSAL_FILE" || export DYMSH_TRAVERSAL_FILE=$(mktemp)

# fix for compatibility between 0.7.x and 0.8.x
DYMSH_SERVICE_NAME=$(test -f ./.dymsh/init.sh && source ./.dymsh/init.sh || source ./.dymsh; echo $DYMSH_SERVICE_NAME)
[ "$?" -eq 0 ] || fail 1


# DYMSH_INDENT="$DYMSH_INDENT$INDENT_TOKEN"

verbose1 && out "Entering ${DYMSH_SERVICE_NAME:-$(pwd)}..." | colorpipe --yellow 1>&2
verbose2 && out "At '$(roots_relative_path)'" | colorpipe --yellow 1>&2
# out $DYMSH_PROJECT_NAME | colorpipe --brown
# exit


filter()
{
	test -z "$SM_MATCH_REGEX" && return 0
	echo "$DYMSH_SERVICE_NAME" | grep -E "$SM_MATCH_REGEX" > /dev/null
}


visited()
{
	grep "$DYMSH_SERVICE_NAME" "$DYMSH_TRAVERSAL_FILE" > /dev/null
}

visit_all()
{
	test $SM_MATCH_MODE = all
}

visit_first()
{
	test $SM_MATCH_MODE = first
}

visit_rest()
{
	test $SM_MATCH_MODE = rest
}

exec-command()
{
	if [ "$USE_SUBSHELL" = 1 ]; then
		bash -c "$*"
	else
		"$@"
	fi
}


# visited   mode     pre    post
# 0         all       Y      Y
# 0         first     Y      Y
# 0         rest      N      N
# 1         all       Y      Y
# 1         first     N      N
# 1         rest      Y      Y

if visited ; then DYMSH_MMM=rest ; else DYMSH_MMM=first ; fi

if filter && (visit_all || (visit_first && ! visited) || (visit_rest && visited)); then
	verbose1 && out "PRE-order visiting: ${DYMSH_SERVICE_NAME:-$(pwd)}" | colorpipe --brown 1>&2
	# verbose2 && out "At '$(roots_relative_path)'" | colorpipe --darkgray

	if [ "${#PRE_ORDER_CMD[@]}" -gt 0 ]; then
		if [ "$INTERACTIVE" = 1 ]; then
			DYMSH_MMM=$DYMSH_MMM exec-command "${POS_ORDER_CMD[@]}"
		else
			DYMSH_MMM=$DYMSH_MMM exec-command "${PRE_ORDER_CMD[@]}" | outpipe
		fi
		RET=${PIPESTATUS[0]}
		test $RET -eq 0 || fail $RET "Command '${PRE_ORDER_CMD[*]}' has failed"
	fi
fi
# echo nao deveria entrar aqui

# Load .dymsh script of this repository
#source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"

# checkversion "$DYMSH_VERSION" $(dymsh --version) || fail 1

for f in $(git submodule status | awk '{print $2}');
do
	# set -x
	if [ -d $f -a -e $f/.git -a "$DYMSH_RECURSION_DEPTH" -lt "$max_recursion_depth" ]; then

		cd $f || fail 1 "Could not change to diretory: $f"

		if [ -f $DYMSH_ROOT/foreach.sh ];
		then
			DYMSH_INDENT="${DYMSH_INDENT}${INDENT_TOKEN}" \
			DYMSH_RECURSION_DEPTH="$((DYMSH_RECURSION_DEPTH+1))" \
			  $DYMSH_ROOT/foreach.sh "${ALL_ARGS[@]}" || fail 21
		fi
		# echo bla

		cd - > /dev/null
	fi
done



if filter && (visit_all || (visit_first && ! visited) || (visit_rest && visited)); then
	verbose1 && out "POST-order visiting ${DYMSH_SERVICE_NAME:-$(pwd)}" | colorpipe --brown 1>&2
	# verbose2 && out "At '$(roots_relative_path)'" | colorpipe --darkgray 1>&2

	# echo array size is "${#POS_ORDER_CMD[@]}"
	if [ "${#POS_ORDER_CMD[@]}" -gt 0 ]; then
		if [ "$INTERACTIVE" = 1 ]; then
			DYMSH_MMM=$DYMSH_MMM exec-command "${POS_ORDER_CMD[@]}"
		else
			DYMSH_MMM=$DYMSH_MMM exec-command "${POS_ORDER_CMD[@]}" | outpipe
		fi
		RET=${PIPESTATUS[0]}
		test $RET -eq 0 || fail $RET "Command '${POS_ORDER_CMD[*]}' has failed"
	fi
fi

echo "$DYMSH_SERVICE_NAME" >> $DYMSH_TRAVERSAL_FILE


verbose1 && out "Leaving ${DYMSH_SERVICE_NAME:-$(pwd)}..." | colorpipe --yellow 1>&2
verbose2 && out "At '$(roots_relative_path)'" | colorpipe --yellow 1>&2

verbose2 && echo 1>&2

true
