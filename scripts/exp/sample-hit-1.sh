#!/bin/bash
set -x

BASE_DIR=/tmp/dymsh/dyt/sample/1

mkdir -p $BASE_DIR &&
cd $BASE_DIR &&
rm -rf rmt/ src/ &&

mkdir src rmt &&
cd src &&
for d in a b; do
	# init an empty repo
	mkdir $d &&
	cd $d &&
	dymsh exp hit init myproj $d-svc &&

	# init an empty bare repo to be the remote
	mkdir $BASE_DIR/rmt/$d &&
	cd $BASE_DIR/rmt/$d &&
	git init --bare &&
	cd - &&

	# add remote and push to it
	git remote add origin $BASE_DIR/rmt/$d &&
	git push origin dev master &&
	git push origin --tags &&

	cd .. &&
	echo ----------------------------
done &&
rm -rf b c d &&

cd a &&

# will succeed
dymsh exp hit require $BASE_DIR/rmt/b mod-b &&
dymsh exp hit require $BASE_DIR/rmt/b mod-b2 &&


# feature creation, commit and merge on the root repo.
dymsh exp hit feature ft1 &&
touch bla &&
git add bla &&
git commit -m "add bla" &&
dymsh exp hit merge &&


# feature creation on root repo and commit on the second.
dymsh exp hit feature ft2 &&
cd mod-b &&
touch bla &&
git add bla &&
git commit -m "add bla" &&
cd .. &&
dymsh exp hit merge &&


# release creation and merge
dymsh exp hit release minor &&
dymsh foreach -v -- git branch &&
cd mod-b &&
touch foo &&
git add foo &&
git commit -m "add foo" &&
cd .. &&
touch bar &&
git add bar &&
git commit -m "add bar" &&
dymsh exp hit merge &&

dymsh check &&
dymsh exp hit checkout master &&
dymsh check &&


dymsh exp hit hotfix patch &&
touch xpto &&
git add xpto &&
git commit -m "add xpto" &&
dymsh exp hit merge &&


dymsh exp hit release minor &&
cd mod-b &&
touch zar &&
git add zar &&
git commit -m "add zar" &&
cd .. &&
dymsh exp hit merge


dymsh exp hit feature ft3 &&
touch pei &&
git add pei &&
git commit -m "add pei" &&
dymsh exp hit merge &&

dymsh check &&
dymsh exp hit push &&
dymsh check &&

set +x &&
echo "TEST HAS FINISHED SUCCESSFULLY"
