#!/bin/bash

############################
# CLI parsing and usage

usage()
{
    cat <<-EOF

	Usage: dymsh exp status [OPTIONS]

	OPTIONS
	   -s, --summary, --short
	       Show short information on one line.

	   -m, --message
	       Show last commit message (for --short)

	   -a, --author
	       Show last commit author (for --short)

	   -d, --diff
	       Show git diff.

	   -l, --log
	       Show git log
	         
	   -h, --help
	       Show usage help.
	EOF
    exit 255
}


DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh


while test $# -gt 0
do
    # echo "current parameter = $1"
    case $1 in
    -s|--summary|--short)
        shift
        OPT_SHORT=1
        ;;
    -m|--message)
		shift
		OPT_MSG=1
		;;
    -a|--author)
		shift
		OPT_AUTHOR=1
		;;
    -d|--diff)
        shift
        OPT_DIFF=1
        ;;
    -l|--log)
        shift
        OPT_LOG=1
        ;;
    -h|--help)
        usage
        ;;
    *)
    
        ;;
    esac
done



opt_diff()
{
    test "$OPT_DIFF" = 1
}

opt_msg()
{
    test "$OPT_MSG" = 1
}

opt_author()
{
    test "$OPT_AUTHOR" = 1
}

opt_log()
{
    test "$OPT_LOG" = 1
}

opt_short()
{
    test "$OPT_SHORT" = 1
}

short_reldate()
{
	# this echo " " injects the separator at the end of the line
	cat - <(echo " ") |
	while read -d " " TK; do
		case $TK in
			seconds)
				echo -n s
				;;
			minutes)
				echo -n m
				;;
			hours)
				echo -n h
				;;
			days)
				echo -n d
				;;
			weeks)
				echo -n w
				;;
			months)
				echo -n mo
				;;
			years)
				echo -n y
				;;
			ago)
				echo -n " ago"
				;;
			*)
				echo -n $TK
				;;
		esac
	done
	echo
}

show_log_ar()
{
    git log -1 --pretty=format:"%ar" | short_reldate
}


show_log_msg()
{
    git log -1 --pretty=format:"%B"
}


show_last_commit()
{
	if opt_author; then FMT+="%an <%ae> "; fi
	if opt_msg;    then FMT+="%s"; fi
	if [ -n "$FMT" ]; then
		echo -n '| '
		git log -1 --pretty=format:"$FMT"
	fi
}


# --short option
convert()
{
	sed -r 's/([0-9]+) files? changed,?( ([0-9]+) insertions?...,?)?( ([0-9]+) deletions?...)?/:\1,:\3,:\5,/g' |
	sed -r 's/:,/:0:/g' | 
	sed -r 's/[,:]/ /g'
}


RED=$(setcolor --red)
WHI=$(setcolor --lightgray)
GRE=$(setcolor --green)
BLU=$(setcolor --blue)
BRW=$(setcolor --brown)
DGRAY=$(setcolor --darkgray)




show_status()
{
	local FBRANCH
	local SERVICES
	local CHANGES
	local FCH
	local FSV
	local CS
	local CSstr
	local W
	local I
	ISCHG_S=0
	ISCHG_I=0
	ISCHG_W=0

	# very slow command
	F1=$(mktemp)
	git diff --stat | tail -1 | convert > $F1 &


	FBRANCH=$(mktemp)
    SERVICES=($(git submodule status | xargs -l | cut -d' ' -f2))
	CHANGES=($(git status --porcelain -b |
	           grep -vE '^\?\?' |
	           tee >(head -1 > $FBRANCH) |
	           tail -n +2 |
	           sed -r 's/...//'))
	# echo ${CHANGES[@]}
	# echo ${SERVICES[@]}

    FCH=$(mktemp)
    FSV=$(mktemp)
    sed -r 's/ +/\n/g' - <<< "${CHANGES[@]}" | grep -v "^$" | sort > $FCH
    sed -r 's/ +/\n/g' - <<< "${SERVICES[@]}" | grep -v "^$" | sort > $FSV
	# echo ---
	# cat $FCH
	# echo ---
	# cat $FSV
	# echo ---

    # changed services: intesection of CHANGES and SERVICES arrays
    CS=$(comm -12 $FCH $FSV |  wc -l)
    test "$CS" = "0" && COLOR=$WHI || COLOR=$RED
    CSstr="$COLOR $CS $WHI"
    test $CS -gt 0 && ISCHG_S=1

    I=($(git diff --stat --cached | tail -1 | convert) )
    test "$I" = "0 0 0" -o -z "$I" && COLOR=$WHI || { COLOR=$RED; ISCHG_I=1; }
    I=($COLOR ${I[@]:-0 0 0} $WHI)

    wait
    W=($(cat $F1))
    test "$W" = "0 0 0" -o -z "$W" && COLOR=$WHI || { COLOR=$RED; ISCHG_W=1; }
    W=($COLOR ${W[@]:-0 0 0} $WHI)


    # echo $CS ${#SERVICES[@]} \
    # $((${#CHANGES[@]} - $CS)) \
    #     ${W[@]} \
    #     ${I[@]} \

    printf "Svc=%s%1d%s/%1d %sI(%d,%d+,%d-)%s %sW(%d,%d+,%d-)%s $BLU%s$WHI" \
        $CSstr ${#SERVICES[@]} \
        ${I[@]} \
        ${W[@]} \
        "$(cat $FBRANCH)"

    rm $FBRANCH $FCH $FSV $F1

    # echo 1\*$ISCHG_S + 2\*$ISCHG_I + 4\*$ISCHG_W
    return $((4*$ISCHG_S + 2*$ISCHG_I + 1*ISCHG_W))
}


DYMSH_SERVICE_NAME=$(bash <<< "source ./.dymsh/init.sh; echo -n \"\$DYMSH_SERVICE_NAME\"")
TAG_NAME=$($DYMSH_ROOT/util/tagname.sh "$@")


if opt_short;
then
	AGO="$(show_log_ar)"
	STATUS="$(show_status)"
	STATUS_CODE=$?

	# DYMSH_MMM -> matching mode for submodules
	test "$DYMSH_MMM" = "rest" && COLOR=$DGRAY || COLOR=$BRW
    
    echo "$COLOR$DYMSH_SERVICE_NAME:$TAG_NAME$GRE[$AGO]$WHI: $STATUS $(show_last_commit)$WHI"

else
    echo "--------------- status ---------------"
    echo ':: '"$DYMSH_SERVICE_NAME"

    git status -b --porcelain

    if opt_diff; then
        echo "---------------  diff  ---------------"
        git diff --color
    fi


    if opt_log; then
        echo "---------------  log   ---------------"
        git log -3 --pretty=format:"%h [%ar] %d [%ai] by %an <%ae> %+B" --graph --color
    fi

    echo ===================================

    STATUS_CODE=0
fi


setcolor --lightgray

exit $STATUS_CODE
