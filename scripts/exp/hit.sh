#!/bin/bash
export DYMSH_ROOT=$(dirname "$0")/../

# Load internal scripts
source $DYMSH_ROOT/exp/snippets/fail
source $DYMSH_ROOT/util/log.sh
source $DYMSH_ROOT/util/semver.sh

show-deprecation-warning() { setcolor --red; echo "This command is deprecated"; resetcolor; }

# CLI usage output
usage()
{
	cat <<-EOF
	Usage:
	   dymsh exp hit [GENOPT...] CMD [CMDOPT...]

	GENOPT...    General options
	   --dry-run
	   --quiet
	   --only
	   -h, --help

	CMD          Command (operation to be performed)
	   alias, init, clone, rq, require, br, branch, co, checkout,
	   feature, release, hotfix, merge, push, pull, help.

	CMDOPT...    Command options (specific for each command)


	Commands details:

	   alias
	            print the command for dymsh aliases (enables short command syntax);

	   init <project-name> <service-name>
	            initializes an empty git repository, adds a default .dymsh file,
	            builds the first version number and checkout to dev branch;

	   clone <repository-url> <path>
	            clones a repository recursively (with submodules);

	   require <repository-url> <name> [<directory>=.]
	            adds a submodule from the given URL to the directory given by <directory>/<name>.
	            the checked out branch will have the same name as the one on the super repository (make sure they match).

	   branch
	            information about current branch;

	   checkout <branch-name>
	            checks out a branch recursively (all submodules);

	   feature <branch-name>
	            create feature branches recursively;

	   release <new-version>|major|minor|patch
	            create release branches recursively;

	   hotfix minor|patch
	            create hotfix branches recursively;

	   merge
	            recursively merge the current branch (a feature, release or hotfix branch)
	            into dev only (for feature branches) or dev and master (for release
	            and hotfix branches);

	   push [<repository-name> [<branch-name>...]]
	            push the repository tree (with submodules) to a remote; dev and master branches are always pushed;

	   pull [<repository-name>]
	            pull the repository tree (with submodules) from a remote;

	   help
	            show this help.
	EOF
	exit
}


# globals
VERREGEX='[0-9]+\.[0-9]+\.[0-9]+'



# functions

_string_in()
{
	VAL="$1"
	shift
	while [ $# -gt 0 ];
	do
		# echo "Testing if '$VAL' matches '$1'..."
		if [[ "$VAL" == $1 ]]; then return 0; fi
		shift
	done
	return 1
}


_detect_cur_branch()
{
	git branch | grep '*' | awk '{print $2}'
}


_detect_branch_type()
{
	test -n "$1" || return 1
	case "$1" in
		dev)
			echo dev;
			;;
		master)
			echo master;
			;;
		release-*)
			echo release;
			;;
		hotfix-*)
			echo hotfix;
			;;
		*)
			echo feature;
			;;
	esac
	return 0
}


_test_branch_in()
{
	CURBR=$(_detect_cur_branch)
	_string_in "$CURBR" "$@"
}


_check_modules_changed_only()
{
	MODS=$(git submodule status | awk '{print $2}')
	test -n "$MODS" || return 1
	REGEX=$(echo "$MODS" | sed -r 's/ /|/g' | xargs -I{} echo '^ M ('"{}"') *$')
	while IFS='' read LINE; do
		echo "$LINE" |
		grep -E "$REGEX" || return 1;
	done < <(git status --porcelain)
}

_has_submodule_only_changes()
{
	if [ $(git status --porcelain | wc -l) -gt 0 ]; then
		if ! _check_modules_changed_only; then
			return 1   # there are changes which are not only submodules
		else
			return 0   # only submodules have changes
		fi
	fi
	return 2  # there are no changes
}

_first_mmm()
{
	test -z "$DYMSH_MMM" -o "$DYMSH_MMM" == "first"
}

_is_root_repo()
{
	test "$DYMSH_MMM" = ""
}


# Detect version for the service in the current directory
# <versions-filename>
_detect_cwd_version()
{
	test -f "$1" || return 1
	VERPARAM="$1"
	VER=$(source ./.dymsh/init.sh && cat "$VERPARAM" | grep "$DYMSH_SERVICE_NAME" | grep -oE "$VERREGEX")
	if [ -z "$VER" ]; then
		echo "The given version file '$VERPARAM' does not specify " \
		     "the version for '$DYMSH_SERVICE_NAME' service." | colorpipe --red
		echo "Nothing has been modified." | colorpipe --red
		return 1
	fi
	echo $VER
	return 0
}

_list_merge_manual_commands()
{
	git status --porcelain | grep -E '^(AA|UU)' | awk '{print "$ nano "$2"; git add/rm "$2}'
	git status --porcelain | grep -E '^(AU|UD|UA|DU)' | awk '{print "$ less "$2"; git add/rm "$2}'
	git status --porcelain | grep -E '^(DD)' | awk '{print "$ git reset -- "$2}'
	# git status --porcelain | grep -E '^(DD|AU|UD|UA|DU|AA|UU)' | awk '{print "$ ? "$2}'
}

_handle_merge_conflict()
{
# TODO handle merge conflicts:
#      foreach unmerged file do `nano file`;
#      test VERSION file against VERREGEX; if failed do `nano VERSION`
#      git add -u && git commit

	{ set +x; } 2>/dev/null   # disable xtrace
	clear
	sleep 0.25
	echo "You have merge conflicts:" | colorpipe --red
	git status | colorpipe --cyan
	echo "But not be scared about it, follow the instructions below:" | colorpipe --blue
	cat <<-EOF
	 1) edit the conflicting files (choose one method):
	     guided) $ git mergetool   # (if you know how...)
	     manual) $(_list_merge_manual_commands | sed -r '2,$s/^/             /')
	 2) commit the changes:
	     $ git commit
	 3) then, exit this shell (see the $(echo -e '\e[0;31m[merging]\e[m') on the prompt):
	     $ exit
	EOF

	TERM=xterm \
	  bash \
	    --rcfile <(cat ~/.bashrc ; echo -ne 'PS1="\[\e[0;31m\][merging]\[\e[m\]$PS1"') \
	    $CONFLICT_HANDLING_SCRIPT

	test $QUIET -eq 0 && set -x
}

#########################

# MIGRATED
# init <project-name> <service-name>
# init_repo()
# {
# 	test $QUIET -eq 0 && set -x

# 	cat <<-EOF > .dymsh &&
# 	DYMSH_VERSION=~$(dymsh --version)
# 	DYMSH_PROJECT_NAME="$1"
# 	DYMSH_SERVICE_NAME="$2"
# 	DYMSH_NETWORK_NAME="$1-net"
# 	EOF
# 	shift 2 &&
# 	git init "$@" &&
# 	git add -f .dymsh &&
# 	dymsh version -m "Initial commit" 0.0.1 &&
# 	git checkout -b dev

# 	{ RET=$?; set +x; } 2>/dev/null   # disable xtrace
# 	return $RET
# }


# MIGRATED
# require <repo-url> <name> [<directory>=.]
# require()
# {
# 	# echo ========
# 	if _test_branch_in 'master' 'hotfix-*' 'release-*'; then
# 		echo "You are in a hotfix-*, release-* or in the master branch." | colorpipe --red
# 		echo "Change to dev or a feature branch and try again." | colorpipe --red
# 		echo "Nothing has been modified." | colorpipe --red
# 		return 1;
# 	else
# 		echo bransh is ok
# 	fi

# 	url="$1"
# 	name="$2"
# 	path="${3:-.}/$2"

# 	test -n "$url" -a -n "$name" || return 1

# 	# skip if --dry-run option enabled
# 	# echo $DRY_RUN $QUIET
# 	if [ $DRY_RUN = 1 ]; then
# 		if [ $QUIET -eq 0 ]; then
# 			echo "- git submodule add -b $(_detect_cur_branch) --name '$name' '$url' '$path' &&" | colorpipe --cyan
# 			echo "- git config diff.submodule log &&" | colorpipe --cyan
# 			echo "- git config status.submodulesummary 1 &&" | colorpipe --cyan
# 			echo "- git config -f .gitmodules 'submodule.$name.branch' . &&" | colorpipe --cyan
# 			echo "- git add .gitmodules &&" | colorpipe --cyan
# 			echo "- git commit -m \"Adding $name submodule from '$url' to '$path' directory\"" | colorpipe --cyan
# 		fi
# 		return 0
# 	fi

# 	test $QUIET -eq 0 && set -x

# 	git submodule add -b $(_detect_cur_branch) --name "$name" "$url" "$path" &&
# 	git config diff.submodule log &&
# 	git config status.submodulesummary 1 &&
# 	git config -f .gitmodules "submodule.$name.branch" . &&
# 	git add .gitmodules &&
# 	git commit -m "Adding $name submodule from '$url' to '$path' directory"

# 	{ RET=$?; set +x; } 2>/dev/null
# 	return $RET
# }

############

feature_begin()
{
	echo at feature_begin
	# pre-conditions
	if ! _test_branch_in dev;
	then
		echo "You must be in the dev branch to create a feature branch." | colorpipe --red
		echo "Nothing has been modified." | colorpipe --red
		return 1
	fi

	if _string_in "$1" 'master' 'dev' 'release-*' 'hotfix-*';
	then
		echo "Invalid feature branch name: $1" | colorpipe --red
		echo "Feature branches cannot be named as master, dev, release-* or hotfix-*." | colorpipe --red
		echo "Nothing has been modified." | colorpipe --red
		return 1
	fi

	# skip if --dry-run option enabled
	if [ $DRY_RUN = 1 ]; then
		if [ $QUIET -eq 0 ]; then
			echo "- git checkout -b $1 dev" | colorpipe --cyan
		fi
		return 0
	fi

	test $QUIET -eq 0 && set -x   # enable xtrace (command execution information)

	# actual commands (TODO should we create a commit "Starting feature branch 'myfeature'"?)
	git checkout -b $1 dev

	{ RET=$?; set +x; } 2>/dev/null     # disable xtrace
	return $RET
}


feature_merge()
{
	echo at feature_merge
	echo feature_merge params are: "$@"
	while [ $# -gt 0 ]; do
		case $1 in
			--conflict)
				export CONFLICT_HANDLING_SCRIPT=$2
				shift 2
				;;
			*)
				echo "Invalid option given to feature_merge: $1" | colorpipe --red
				return 1
				;;
		esac
	done
	echo CONFLICT_HANDLING_SCRIPT="$CONFLICT_HANDLING_SCRIPT"

	test $# -eq 0 || return 1

	# pre-conditions
	if _test_branch_in 'master' 'dev' 'release-*' 'hotfix-*'; then
		echo "You must be in a feature branch. Current branch: $(_detect_cur_branch)." | colorpipe --red
		echo "Nothing has been modified." | colorpipe --red
		return 1
	fi

	MODS_CHANGED=0
	if [ $(git status --porcelain | wc -l) -gt 0 ]; then
		if ! _check_modules_changed_only; then
			echo "There are merge conflicts or changes on working tree or in the index." | colorpipe --red
			echo "Please, fix, stash or commit them, then try again." | colorpipe --red
			echo "Nothing has been modified." | colorpipe --red
			return 1
		else
			echo "Submodules have changed." | colorpipe --cyan
			MODS_CHANGED=1
		fi
	fi

	if [ $DRY_RUN = 1 ]; then
		if [ $QUIET -eq 0 ]; then
			echo feature merge dry run | colorpipe --cyan
		fi
		return 0
	fi

	test $QUIET -eq 0 && set -x   # enable xtrace (command execution information)

	source ./.dymsh/init.sh && TMP_RMT_FILE=$DYMSH_TMP_DIR/$DYMSH_SERVICE_NAME &&

	if _first_mmm
	then
		pwd -P > $TMP_RMT_FILE &&
		if [ $MODS_CHANGED = 1 ]; then
			git add -u &&
			git commit -m "Updating submodules" || return
		fi
		
		CURBR=$(_detect_cur_branch) &&
		git checkout dev &&
		git merge --no-ff "$CURBR" -m "Merge branch '$CURBR' into dev"
		test $? -eq 0 || _handle_merge_conflict || return 1

		git branch -d "$CURBR"
	else
		{ git remote rm dyfs 2>/dev/null || true; } &&
		git remote add dyfs $(cat $TMP_RMT_FILE) &&
		git fetch dyfs &&
		git fetch dyfs --tags &&

		CURBR=$(_detect_cur_branch) &&
		git checkout dev &&
		git reset --hard dyfs/dev

		git branch -d "$CURBR"
	fi

	{ RET=$?; set +x; } 2>/dev/null     # disable xtrace
	return $RET
}


#######################


# {major|minor|patch|x.y.z|<filename>} <source-branch> <branch-type> 
release_or_hotfix_begin()
{
	test $# -eq 3 || return 1
	VERPARAM="$1"
	SRCBR=$2
	BRTYPE=$3
	shift 3

	# pre-conditions
	if ! _test_branch_in $SRCBR;
	then
		echo "You must be in the $SRCBR branch to create a $BRTYPE branch." | colorpipe --red
		echo "Nothing has been modified." | colorpipe --red
		return 1
	fi


	test -f "$VERPARAM" && VERPARAM="$(_detect_cwd_version)"

	if echo "$VERPARAM" | grep -E "$VERREGEX"; then
		VER="$VERPARAM"
	else
		if _string_in "$VERPARAM" major minor patch; then
			POS="$VERPARAM"
		else
			echo "Invalid version for $BRTYPE branch: $VERPARAM" | colorpipe --red
			echo "A $BRTYPE branch version must be named as major, minor, patch or x.y.z." | colorpipe --red
			echo "Nothing has been modified." | colorpipe --red
			return 1
		fi
	fi

	MODS_CHANGED=0
	if [ $(git status --porcelain | wc -l) -gt 0 ]; then
		if ! _check_modules_changed_only; then
			echo "There are merge conflicts or changes on working tree or in the index." | colorpipe --red
			echo "Please, fix, stash or commit them, then try again." | colorpipe --red
			echo "Nothing has been modified." | colorpipe --red
			return 1
		else
			echo "Submodules have changed." | colorpipe --cyan
			MODS_CHANGED=1
		fi
	fi


	# Compute version number
	test -f VERSION || fail 129 "VERSION file not found. Please create one and try again."
	OLD_VER=$(cat VERSION)
	if [ -n "$POS" ];
	then
		NEW_VER=$(increment_version $POS $OLD_VER)
	else
		NEW_VER=$VER
	fi
	echo "Next version will be $NEW_VER"
	# return 0

	NEWBR=$BRTYPE-$NEW_VER


	COMMIT_MSG="Bumped version number from $OLD_VER to $NEW_VER into new branch '$NEWBR'"


	# skip if --dry-run option enabled
	if [ $DRY_RUN = 1 ]; then
		if [ $QUIET -eq 0 ]; then
			echo "- source ./.dymsh/init.sh &&" | colorpipe --cyan
			echo "- TMP_RMT_FILE=$DYMSH_TMP_DIR/$DYMSH_SERVICE_NAME &&" | colorpipe --cyan
			if _first_mmm; then
				echo "- git checkout -b $NEWBR $SRCBR &&" | colorpipe --cyan
				echo "- echo \"$NEW_VER\" > VERSION &&" | colorpipe --cyan
				echo "- git add VERSION &&" | colorpipe --cyan
				echo "- { test $MODS_CHANGED = 1 && git add -u || true; } &&" | colorpipe --cyan
				echo "- git commit -m \"$COMMIT_MSG\"" | colorpipe --cyan
			else
				echo "- { git remote rm dyfs 2>/dev/null || true; } &&" | colorpipe --cyan
				echo "- git remote add dyfs <local-repo-url> &&" | colorpipe --cyan
				echo "- git fetch dyfs &&" | colorpipe --cyan
				echo "- git fetch dyfs --tags &&" | colorpipe --cyan
				echo "- git checkout $NEWBR &&" | colorpipe --cyan
				echo "- git reset --hard dyfs/$NEWBR" | colorpipe --cyan
			fi
		fi
		return 0
	fi

	# commands
	test $QUIET -eq 0 && set -x   # enable xtrace (command execution information)

	source ./.dymsh/init.sh && TMP_RMT_FILE=$DYMSH_TMP_DIR/$DYMSH_SERVICE_NAME &&


	if _first_mmm
	then
		git checkout -b $NEWBR $SRCBR &&
		echo "$NEW_VER" > VERSION &&
		git add VERSION &&
		{ test $MODS_CHANGED = 1 && git add -u || true; } &&
		git commit -m "$COMMIT_MSG" &&
		pwd -P > $TMP_RMT_FILE
	else
		{ git remote rm dyfs 2>/dev/null || true; } &&
		git remote add dyfs $(cat $TMP_RMT_FILE) &&
		git fetch dyfs &&
		git fetch dyfs --tags &&
		git checkout $NEWBR &&
		git reset --hard dyfs/$NEWBR
	fi

	{ RET=$?; set +x; } 2>/dev/null     # disable xtrace
	return $RET
}


# <src-branch-type> <dest-branch>
release_or_hotfix_merge()
{
	echo release_or_hotfix_merge params are: "$@"
	while [ $# -gt 2 ]; do
		case $1 in
			--conflict)
				export CONFLICT_HANDLING_SCRIPT=$2
				shift 2
				;;
			*)
				echo "Invalid option given to release_or_hotfix_merge: $1" | colorpipe --red
				return 1
				;;
		esac
	done
	echo CONFLICT_HANDLING_SCRIPT="$CONFLICT_HANDLING_SCRIPT"

	test $# -eq 2 || return 1
	SRCBR=$1-$(cat VERSION)
	DSTBR=$2

	if [ $DRY_RUN = 1 ]; then
		if [ $QUIET -eq 0 ]; then
			echo "release merge dry run" | colorpipe --cyan
		fi
		return 0
	fi

	# Get all submodules registered on the source branch
	git checkout $SRCBR || return 10
	MODS=$(git submodule status | awk '{print $2}')
	REGEX=$(echo "$MODS" | sed -r 's~ ~|~g' | xargs -I{} echo '^(\?\?| M) ('"{}"')/? *$')
	echo "MODS='$MODS'"
	echo "REGEX='$REGEX'"
    # REGEX="^(??| M) (sm1|sm2|...) *$"

	# commands
	shift 2

	# echo "---------------" 1>&2
	# echo "GIT DIFF-TREE:" 1>&2
	# pwd 1>&2
	# git branch 1>&2
	# git diff-tree --no-commit-id --name-only -r HEAD 1>&2
	# dymsh check 1>&2
	# echo "================================================" 1>&2
	# git status 1>&2
	# echo "================================================" 1>&2
	# gitk --all & sleep 2

	test $QUIET -eq 0 && set -x   # enable xtrace (command execution information)

	source ./.dymsh/init.sh && TMP_RMT_FILE=$DYMSH_TMP_DIR/$DYMSH_SERVICE_NAME &&

	if _first_mmm
	then
		pwd -P > $TMP_RMT_FILE &&
		git checkout $DSTBR || return 11

		# git diff-tree --no-commit-id --name-only -r HEAD | hexdump -C 1>&2

# 		if [ "$(git diff-tree --no-commit-id --name-only -r HEAD)" = "VERSION" ]; then # unmodified leaf

# 			PARENT_COMMIT=$(git rev-list --parents -n 1 $SRCBR | cut -d' ' -f2) &&
# 			git checkout $DSTBR &&
# 			git reset --hard $PARENT_COMMIT
# dymsh check 1>&2

# 		elif git log -1 --pretty=%B | grep 'Bumped version number'; then  # unmodified non-leaf
# 			echo "UNMODIFIED NON-LEAF SUBMODULE"
# 			PARENT_COMMIT=$(git rev-list --parents -n 1 $SRCBR | cut -d' ' -f2) &&
# 			git checkout $PARENT_COMMIT
# 			if [ "$(git status --porcelain)" = "" ]; then  # really unmodified
# 				git checkout $DSTBR 
# 				git reset --hard $PARENT_COMMIT
# 			else # unexpected
# 				meld .
# 			fi
# 		else
#			git checkout $DSTBR || return 11

		git status --porcelain
		# Untracked modules
		# BUG/2018-08-16: does not capture the case when a submodule is in a subdirectory of an untracked directory:
		# E.g.  SRCBR has a submodule in modules/pgsql and DSTBR does not have the modules/ directory
		#       MODS  will be 'modules/pgsql' and
		#       REGEX will be '^(\?\?| M) (modules/pgsql)/? *$'
		#       but `git status --porcelain` below will produce '?? modules/' which does not match the regex.
		MODS_UU=$(git status --porcelain | grep -E "$REGEX" | grep -E '^\?\?' | awk '{print $2}')
		# Modified on work tree modules
		MODS__M=$(git status --porcelain | grep -E "$REGEX" | grep -E '^ M' | awk '{print $2}')

		echo "MODS_UU='$MODS_UU'"
		echo "MODS__M='$MODS__M'"

		if [ -n "$MODS__M" ]; then
			git add $MODS__M
			git commit -m "Updating modified submodules on $DSTBR"
		fi

		# TODO/2018-08-16: mayne SRCBR should 'master' when DSTBR is 'dev'
		git merge --no-ff "$SRCBR" -m "Merge branch '$SRCBR' into $DSTBR"
		test $? -eq 0 || _handle_merge_conflict || return 1

		# MODS__M=$(git status --porcelain | grep -E "$REGEX" | grep -E '^ M' | awk '{print $2}')
		# echo "MODS1='$MODS_UU'"

		if [ -n "$MODS_UU" ]; then
			git add $MODS_UU
			git commit -m "Updating untracked-before-merge submodules on $DSTBR"
		fi

		# Bufix for BUG/2018-08-16 above
		# Modified on work tree modules
		MODS__M=$(git status --porcelain | grep -E "$REGEX" | grep -E '^ M' | awk '{print $2}')
		echo "MODS__M='$MODS__M'"
		if [ -n "$MODS__M" ]; then
			git add $MODS__M
			git commit -m "Updating modified-after-merge submodules on $DSTBR"
		fi


		test $DSTBR = master && git tag -a $(cat VERSION) -m "Merge branch '$SRCBR' into master" || true
		# fi
	else
		{ git remote rm dyfs 2>/dev/null || true; } &&
		git remote add dyfs $(cat $TMP_RMT_FILE) &&
		git fetch dyfs &&
		git fetch dyfs --tags &&

		git checkout $DSTBR &&
		git reset --hard dyfs/$DSTBR
	fi

	# gitk --all

	{ RET=$?; set +x; } 2>/dev/null     # disable xtrace
	return $RET
}


# <branch-type>
release_or_hotfix_merge_preconditions()
{
	BRTYPE=$1

	if ! _test_branch_in "$BRTYPE-*"; then
		echo "You must be in a $BRTYPE-* branch. Current branch: $(_detect_cur_branch)." | colorpipe --red
		echo "Nothing has been modified." | colorpipe --red
		return 1
	fi

	MODS_CHANGED=0
	if [ $(git status --porcelain | wc -l) -gt 0 ]; then
		if ! _check_modules_changed_only; then
			echo "There are merge conflicts or changes on working tree or in the index." | colorpipe --red
			echo "Please, fix, stash or commit them, then try again." | colorpipe --red
			echo "Nothing has been modified." | colorpipe --red
			return 1
		else
			echo "Submodules have changed." | colorpipe --cyan
			MODS_CHANGED=1
		fi
	fi
}


sync_tree()
{
	# if _test_branch_in "master"; then
	# 	echo "You must be in the dev branch. Current branch: $(_detect_cur_branch)." | colorpipe --red
	# 	echo "Nothing has been modified." | colorpipe --red
	# 	return 1
	# fi

	_has_submodule_only_changes
	case $? in
		0)  # only submodules have changes
			# echo "Submodules have changed." | colorpipe --cyan
			# git status --porcelain | colorpipe --cyan
			SYNC=1
			;;
		1)  # there are changes on submodules and other objects
			echo "There are merge conflicts or changes on working tree or in the index." | colorpipe --red
			echo "Please, fix, stash or commit them, then try again." | colorpipe --red
			echo "Nothing has been modified." | colorpipe --red
			return 1
			;;
		2)  # there are no changes
			# echo "No changes"
			SYNC=0
			;;
	esac


	if [ $DRY_RUN = 1 ]; then
		if [ $QUIET -eq 0 ]; then
			echo sync tree dru run | colorpipe --cyan
		fi
		return 0
	fi

	test $QUIET -eq 0 && set -x   # enable xtrace (command execution information)

	source ./.dymsh/init.sh && TMP_RMT_FILE=$DYMSH_TMP_DIR/$DYMSH_SERVICE_NAME &&

	if _first_mmm; then

		pwd -P > $TMP_RMT_FILE &&

		test $SYNC = 0 && return 0

		git add -u && git commit -m "Updating submodules"

	else
		{ git remote rm dyfs 2>/dev/null || true; } &&
		git remote add dyfs $(cat $TMP_RMT_FILE) &&
		git fetch dyfs &&
		git fetch dyfs --tags &&

		CURBR=$(_detect_cur_branch) &&
		git reset --hard dyfs/$CURBR
	fi

}


#################

# {major|minor|patch|x.y.z}
release_begin()
{
	release_or_hotfix_begin ${1:-?} dev release
}


# {major|minor|patch|x.y.z}
hotfix_begin()
{
	release_or_hotfix_begin ${1:-?} master hotfix
}


##########################

# ARGS...
# Expect CMD, FNC and OPTS variables to be defined
exec_recursive()
{
	# echo "At exec_recursive ""$@"" with env:"
	# echo CMD=$CMD FNC=$FNC OPTS=$OPTS
	# printenv|sort
	# echo ---------------
	if [ -n "$CMD" -a -n "$FNC" ]; then
		echo "DYMSH_MMM -> $DYMSH_MMM"
		if _is_root_repo;
		then
			# echo is root repo;
			if [ -z "$DRY_RUN" -o "$DRY_RUN" -eq 0 ]; then
				# First runs the command with --dry-run option, then actualy runs the command
				dymsh foreach -v ${INTERACTIVE:+-i} -- dymsh exp hit --quiet --dry-run ${OPTS[@]} $CMD "$@" &&
				dymsh foreach -v ${INTERACTIVE:+-i} -- dymsh exp hit ${OPTS[@]} $CMD "$@"
			else
				# OPTS already has --dry-run
				dymsh foreach -v ${INTERACTIVE:+-i} -- dymsh exp hit ${OPTS[@]} $CMD "$@"
			fi
			RET=$?
			echo ROOT
			test $RET -eq 0 || echo "Command '$CMD $@' failed." | colorpipe --red
			return $RET
		else
			echo "DYMSH HIT: $FNC $@"
			# exit
			$FNC "$@"
			RET=$?
			test $RET -eq 0 || echo "Command '$FNC $@' failed." | colorpipe --red
			return $RET
		fi
	else
		echo "Command not specified." | colorpipe --red
	fi
}


###########################
[ "$#" -ge 1 ] || usage

# Globals
VERREGEX='[0-9]+\.[0-9]+\.[0-9]+'
export RECURSIVE=1
export DRY_RUN=0
export QUIET=0
test -n "$DYMSH_TMP_DIR" || export DYMSH_TMP_DIR=$(mktemp -d -t dymsh-remote.XXXXXXXX)


# Parse CLI general options
DONE=0
OPTS=()
while [ $# -gt 0 -a $DONE -eq 0 ];
do
	case $1 in
		--function)
			shift
			"$@"
			exit
			;;
		--only)
			export RECURSIVE=0
			OPTS+=(--only)
			shift
			;;
		--dry-run)
			export DRY_RUN=1;
			OPTS+=(--dry-run)
			shift
			;;
		--quiet)
			export QUIET=1;
			OPTS+=(--quiet)
			shift
			;;
		help|-h|--help)
			usage
			exit
			;;
		*)
			DONE=1
			;;
	esac
done


echo "# CWD: $(pwd)" 1>&2

# Parse CLI command
DONE=0
CMD=""
while [ $# -gt 0 -a $DONE -eq 0 ];
do
	case $1 in
		alias)
			echo 'alias dyt="dymsh exp hit"'
			echo '# eval $(dymsh exp hit alias)'
			exit 0
			;;
		init)
			show-deprecation-warning
			test $# -gt 2 || usage
			shift
			init_repo "$@"
			exit
			;;
		clone)
			show-deprecation-warning
			[[ "$RECURSIVE" = "1" ]] && RECURSIVE=--recursive || RECURSIVE=
			shift
			GIT_REF=master
			GIT_REF_ARG="-b master"
			while [ $# -gt 0 ]; do
				case "$1" in
					-b|--ref|--branch)
						if ! grep -E '^[a-z0-9A-Z_-]+$' <<< "$2"; then
							echo "Invalid git branch or ref name: $2"
							exit 1
						fi
						GIT_REF="$2"
						GIT_REF_ARG="-b $2"
						shift 2
						;;
					*)
						git clone ${GIT_REF_ARG} ${RECURSIVE} "$1" "$2" &&
						cd "$2" &&
						dymsh exp hit co $GIT_REF
						shift 2
						;;
				esac
			done
			exit
			;;
		rq|require)
			show-deprecation-warning
			test $# -gt 2 || usage
			shift
			require "$@"
			exit
			;;
		br|branch)
			git branch -a || exit
			echo "Current branch type: $(_detect_branch_type $(_detect_cur_branch))"
			exit
			;;
		co|checkout)
			show-deprecation-warning
			test $# -eq 2 || usage
			test $RECURSIVE = 1 || { echo "Cannot checkout branch without recursion. Remove --only option."; exit 1; }
			dymsh foreach -v -- bash -c "git checkout $2"
			exit 0
			;;
		feature|release|hotfix)
			# test -d .git || fail 99 "You must be in the top repository to run dyt $1"
			CMD=$1
			FNC=${CMD}_begin
			shift

			# ps -p $$ -o args
			# echo DYMSH_MMM=$DYMSH_MMM
			# echo ===========
			# set -x
			test $# -gt 0 || { echo "Missing parameter for $CMD"; exit 1; }
			test $RECURSIVE = 1 || { echo "Cannot create feature, release or hotfix branch without recursion. Remove --only option."; exit 1; }
			# echo "before calling exec_recursive ""$@"" with env:" ; printenv|sort
			# echo --------------
			exec_recursive "$@"
			# echo "exec_recursive ""$@"" returned $?"
			exit
			;;
		mg|merge)
			# test -d .git || fail 99 "You must be in the top repository to run dyt merge"
			shift
			test $RECURSIVE = 1 || { echo "Cannot merge branch without recursion. Remove --only option."; exit 1; }
			
			CURBR=$(_detect_cur_branch)
			test "$CURBR" = "dev" -o "$CURBR" = "master" && {
				echo "Only feature, release* and hotfix* branches may be merged." | colorpipe --red
				echo "The current branch is '$CURBR'." | colorpipe --red
				echo "Nothing has been modified." | colorpipe --red
				exit 1;
			}

			CURBRTYPE=$(_detect_branch_type $CURBR)

			if [ $CURBRTYPE = feature ]; then
				CMD="merge"
				FNC=${CURBRTYPE}_merge
				RECURSIVE=${RECURSIVE:-1}
				INTERACTIVE=1
				exec_recursive "$@"
			else
				HITCMD="dymsh foreach -v -i -- dymsh exp hit"

				if _is_root_repo; then
					CURBR=$(_detect_cur_branch) &&
					$HITCMD ${OPTS[@]} --function release_or_hotfix_merge_preconditions $CURBRTYPE &&
					$HITCMD ${OPTS[@]} --function release_or_hotfix_merge "$@" $CURBRTYPE master || exit

					# TODO/2018-08-16: once CURBRTYPE is already merged into master,
					#   maybe the following merges could be:
					#     - release: master into dev
					#     - hotfix:  master into release-* and dev
					#   but SIDE EFFECTS must be evaluated to prevent conflict between
					#   the master tag, the version in the branch name and the VERSION file.
					case $CURBRTYPE in
						release)
							$HITCMD ${OPTS[@]} --function release_or_hotfix_merge "$@" $CURBRTYPE dev || exit
							;;
						hotfix)
							RELEASE_BRANCHES="$(git branch | sed 's/[* ]//g' | grep release-)"
							for b in $RELEASE_BRANCHES dev; do
								$HITCMD ${OPTS[@]} --function release_or_hotfix_merge "$@" $CURBRTYPE $b || exit
							done
							;;
					esac

					dymsh foreach -v -- bash -c 'git branch -d '"$CURBRTYPE"'-$(cat VERSION)'
				else
					echo "ERROR !!!!! " && exit 255
				fi
			fi

			exit
			;;

		sync)
			# test -d .git || fail 99 "You must be in the top repository to run dyt sync"
			CMD=$1
			FNC=sync_tree
			test $RECURSIVE = 1 || { echo "Cannot sync repository tree without recursion. Remove --only option."; exit 1; }
			shift
			exec_recursive "$@"
			exit

			shift
			dymsh foreach -v -- dymsh exp hit "${OPTS[@]}" --function sync_tree "$@" &&
			echo $?
			exit 0
			;;
		push)
			# test -d .git || fail 99 "You must be in the top repository to run dyt push"
			# TODO should check the repo tree with 'dymsh check --strict'
			test $RECURSIVE = 1 || { echo "Cannot push without recursion. Remove --only option."; exit 1; }
			test -n "$2" && REMOTE=$2 || REMOTE=origin
            test -n "$3" && BRANCHES="$3" || BRANCHES=
			dymsh foreach -v -- bash -c "pwd; git push $REMOTE dev master $BRANCHES && git push $REMOTE --tags"
			exit 0
			;;
		pull)
			# test -d .git || fail 99 "You must be in the top repository to run dyt pull"
			test $RECURSIVE = 1 || { echo "Cannot pull without recursion. Remove --only option."; exit 1; }
			test -n "$2" && REMOTE=$2 || REMOTE=origin
            #test -n "$3" && BRANCHES="$3" || BRANCHES=
			dymsh foreach -v -- bash -c "git pull $REMOTE && git submodule update --init"
			exit 0
			;;
		*)
			echo "Unknown command specified: $1" | colorpipe --red
			exit
			;;
	esac
done

echo "Invalid command line." | colorpipe --red
