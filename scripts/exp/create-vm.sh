#!/bin/bash

fail()
{
	RET_CODE=$1
	echo $2
	shift
	shift
	echo "To destroy the machine, type:" "$@"
	exit $RET_CODE
}


upfind()
{
	LPWD=$PWD
	cd $1
	shift
	while [ $PWD != / ]
	do
		find "$PWD"/ -maxdepth 1 "$@" -exec echo {} \;
		cd ..
	done
	cd $LPWD
	return 1
}


dymsh_package()
{
	DYMSH_BIN_PATH=$(upfind $DYMSH_ROOT -name dymsh -executable | head -1)
	DYMSH_ROOT_DIR=$(dirname $DYMSH_BIN_PATH)
	DYMSH_VERSION=$(cat $DYMSH_ROOT_DIR/VERSION)

	OLDPWD=$PWD
	cd $DYMSH_ROOT_DIR
	tar -czvf dymsh-$DYMSH_VERSION.tar.gz \
	    --transform "s,^,dymsh-$DYMSH_VERSION/," \
	    *

	test $? -eq 0 && cd $OLDPWD && echo $DYMSH_ROOT_DIR/dymsh-$DYMSH_VERSION.tar.gz && return 0
	cd $OLDPWD
	echo && return 1
}

dymsh_upload()
{
	VM_NAME=$1
	VM_IP=$(docker-machine ip $VM_NAME)
	DYMSH_PACKAGE=$(dymsh_package | tail -1)
	KH_TMP=$(tempfile)
	ssh-keyscan $VM_IP > $KH_TMP
	scp -o UserKnownHostsFile=$KH_TMP \
	    $DYMSH_PACKAGE  root@$VM_IP:/root/ \
		|| fail 255 "Could not copy dymsh tarball" docker-machine rm $VM_NAME
	rm $KH_TMP
}



usage()
{
	BIN=exp/$(basename $0 | cut -d . -f 1)
	cat <<-EOF
	Usage: dymsh $BIN ENV_NAME ROLE DO_SIZE QUANT/RANGE
	   ENV_NAME      stage, prod, ...
	   ROLE          front, back, api, etc. [a-z0-0]+
	   DO_SIZE       512mb 1gb 2gb etc.
	   QUANT/RANGE   2, 01, 03, 02-03, 1-04, etc.

	Expected environment variables (shell):
		DYMSH_PROJECT_NAME    Regex: [a-z0-9]+. Default: sample
		DO_REGION       Samples: nyc1, nyc2, sfo, etc... Default: nyc1
		DO_TOKEN        Digital Ocean's API token. Required.
		SSH_FINGERPRINT Fingerprint of a previously registered SSH key.
		                Required.
	EOF
	exit 127
}



[ $# -eq 4 ] || usage

VM_ENV=$1
VM_ROLE=$2
VM_SIZE=$3
VM_QUANTITY=$4

DYMSH_PROJECT_NAME=${DYMSH_PROJECT_NAME:-sample}
DO_REGION=${DO_REGION:-nyc1}

[ -n "$DO_TOKEN" ] || fail 127 "DO_TOKEN variable must be set."
[ -n "$SSH_FINGERPRINT" ] || fail 127 "SSH_FINGERPRINT variable must be set."

if echo $VM_QUANTITY | grep -E '[0-9]+-[0-9]+';
then
	VM_START_RANGE=$(echo $VM_QUANTITY | grep -Eo '^[0-9]+')
	VM_FINAL_RANGE=$(echo $VM_QUANTITY | grep -Eo '[0-9]+$')
elif echo $VM_QUANTITY | grep -E '[0-9]+';
then
	VM_START_RANGE=1
	VM_FINAL_RANGE=$VM_QUANTITY
else
	fail 127 "Invalid QUANT/RANGE parameter: $VM_QUANTITY"
fi


DYMSH_ROOT=$(dirname "$0")
echo $DYMSH_ROOT
# exit

for N in $(seq -w $VM_START_RANGE $VM_FINAL_RANGE)
do
	VM_NAME=$DYMSH_PROJECT_NAME-$VM_ENV-$VM_ROLE-$N
	docker-machine create \
		--engine-label custom.node.role=$VM_ROLE \
		--driver=digitalocean \
		--digitalocean-access-token=$DO_TOKEN \
		--digitalocean-size=$VM_SIZE \
		--digitalocean-region=$DO_REGION \
		--digitalocean-image=debian-8-x64 \
		--digitalocean-private-networking=true \
		--digitalocean-ssh-key-fingerprint=$SSH_FINGERPRINT \
		$VM_NAME \
		|| fail 1 "Could not create docker machine" docker-machine rm $VM_NAME


	dymsh_package && dymsh_upload $VM_NAME

	# The command below sends all snippets to docker-machine's SSH
	# by piping snippets and a here-document using cat
	cat $DYMSH_ROOT/snippets/* - <<-EOF |
		upgrade
		disable_services
		swap
		firewall
		dymsh
	EOF
	docker-machine ssh $VM_NAME \
		|| fail 3 "Could not initialize remote machine" docker-machine rm $VM_NAME


	cat >> $VM_NAME.properties <<-EOF
	#
	VM_ENV=$VM_ENV
	VM_NAME=$VM_NAME
	VM_ROLE=$VM_ROLE
	VM_SIZE=$VM_SIZE
	VM_REGION=$DO_REGION
	VM_IP=$VM_IP
	VM_DYMSH_VERSION=$DYMSH_VERSION
	VM_SSH_FINGERPRINT=$SSH_FINGERPRINT
	EOF

done
