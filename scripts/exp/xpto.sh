#!/bin/bash
# dymsh foreach -- dymsh exp xpto

#typeset -fx dygit-push


if [ -z "$DYMSH_MMM" -o "$DYMSH_MMM" == "first" ]
then
	cat <<-EOF
	----------------------
	git add -u
	git add ...
	git commit -m "..."
	dymsh version patch
	git push origin
	git push origin --tags
	exit
	----------------------
	dygit-push "MSG":
	     git add -u   &&
	     git commit -m "\$MSG" &&
	     dymsh version patch  &&
	     git push origin      &&
	     git push origin --tags
	EOF
else
	cat <<-EOF
	----------------------
	git fetch origin
	git fetch origin --tags
	git checkout master
	git reset --hard origin/master
	exit
	----------------------
	dygit-pull BRANCH:
	     run all commands above using BRANCH instead of master
	EOF
fi

bash --init-file <(cat /etc/bash.bashrc ~/.bashrc - <<EOF

dygit-push()
{
	test \$# -gt 0 || { echo "Message required"; return 1; }
	git add -u &&
	git commit -m "\$1" &&
	dymsh version patch &&
	git push origin &&
	git push origin --tags || { echo "FAILED" ; return 1; }
}

dygit-pull()
{
	test \$# -gt 0 || { echo "Message required"; return 1; }
	git fetch origin         &&
	git fetch origin --tags  &&
	git checkout \$1      &&
	git reset --hard origin/\$1 || { echo "FAILED" ; return 1; }
}

git status
echo --------------------
EOF
)

