#!/bin/bash

DYMSH_ROOT=$(dirname "$0")

# Load internal scripts
source $DYMSH_ROOT/snippets/fail
source $DYMSH_ROOT/../util/log.sh


# parse the command line in the form: [PARAM... --] PARAM...
# the separator -- is required only if the first part is given
# both parts may be empty
# if the first part is empty, the separator -- is not required


declare -a ARGS
FOLLOW=0

parse_args()
{
	ARGS_FINISHED=0
	while [ $# -gt 0 ];
	do
		case $1 in
			--)
				# echo "ENDOFARGS"
				return 0
				;;
			-f)
				FOLLOW=1
				ARGS+=("$1")
				;;
			*)
				# echo "PARAM $1"
				ARGS+=("$1")
				;;
		esac
		shift
	done
	return 1
}

parse_args "$@"

if [ $? -eq 0 ]; then
	shift ${#ARGS[@]}
	shift  # the -- argument
else
	echo "No args given to log"
fi

setcolor --lightgray

source ./.dymsh/init.sh "$@" || fail 1 "Could not load ./.dymsh/init.sh"
DYMSH_NETWORK_NAME=${DYMSH_NETWORK_NAME:-appnet}

# echo "${ARGS[@]}"



print_log()
{
	ID=$1
	echo $ID
	FIFO_FILE=/tmp/dymsh-logs-$ID
	test -e $FIFO_FILE && { rm $FIFO_FILE || fail 1 "Could not remove fifo"; }
	mkfifo $FIFO_FILE
	FIFOS="$FIFOS $FIFO_FILE"

	# sed "s/^/[$ID] /" < $FIFO_FILE &
	colorlogpipe --brown "[$ID] " < $FIFO_FILE &
	echo "docker logs ${ARGS[@]} $ID"
	docker logs ${ARGS[@]} $ID \
	    2> >(colorpipe --red  > $FIFO_FILE) \
	    1> >(colorpipe --blue > $FIFO_FILE) &
	PID=$!
	PIDS="$PIDS $PID"
}


CONTAINER_IDS=$(docker ps -f name="$DYMSH_SERVICE_NAME" -q)
for ID in $CONTAINER_IDS; do
	print_log $ID
done

if [ -n "$PIDS" ]; then
	echo "PIDs: $PIDS"
	echo "================================================="
else
	echo "No running containers."
	echo "Printing last exited one..."
	print_log $(docker ps -f name="$DYMSH_SERVICE_NAME" -f status=exited -aq | head -1)
	# exit 0
fi

finish()
{
	echo Finishing child processes
	kill $PIDS
}

int_handler()
{
	echo " SIGINT caught"
	finish
}

term_handler()
{
	echo " SIGTERM caught"
	finish
}

cleanup()
{
	wait
	test -n "$FIFOS" && rm $FIFOS
	echo "DONE"
	exit 1
}

trap int_handler INT
trap term_handler TERM
trap cleanup EXIT

wait
