#!/bin/bash

# Init
set -x &&

BASE_DIR=/tmp/dymsh/dyt/sample/3 &&

mkdir -p $BASE_DIR &&
cd $BASE_DIR &&
rm -rf rmt/ src/ &&

mkdir src rmt &&
cd src &&
for d in a b c; do
	# init an empty repo
	mkdir $d &&
	cd $d &&
	dymsh exp hit init sample3 $d-svc &&

	# init an empty bare repo to be the remote
	mkdir $BASE_DIR/rmt/$d &&
	cd $BASE_DIR/rmt/$d &&
	git init --bare &&
	cd - &&

	# add remote and push to it
	git remote add origin $BASE_DIR/rmt/$d &&
	git push origin dev master &&
	git push origin --tags &&

	cd .. &&
	echo ----------------------------
done &&
rm -rf b c &&

cd a &&
dymsh exp hit require $BASE_DIR/rmt/b mod-b &&
dymsh exp hit require $BASE_DIR/rmt/c mod-c &&
cd mod-b &&
dymsh exp hit require $BASE_DIR/rmt/c mod-c &&
cd .. &&
git add mod-b &&
git commit -m "updating mod-b" &&

# Add files into dev and commit
dymsh exp hit feature develop &&
dymsh foreach -v -m first -- bash -c "
pwd &&
set -x &&
echo 'f-uu on dev' > f-uu &&
echo 'f-dd on dev' > f-dd &&
echo 'f-ud on dev' > f-ud &&
echo 'f-du on dev' > f-du &&
git add f-uu f-dd f-ud f-du &&
git commit -m 'commit on dev' &&
echo 'Done' " &&
dymsh exp hit merge

# Branch b1 (will be us)
dymsh exp hit feature b1 &&
dymsh foreach -v -m first -- bash -c "
# 
git status &&
set -x &&
echo 'f-aa on b1' > f-aa &&
echo 'f-uu on b1' > f-uu &&
echo 'f-ud on b1' > f-ud &&
git add f-aa f-uu f-ud &&
git mv f-dd f-au &&
git rm f-du &&
git commit -m 'commit on b1' &&
echo 'Done' " &&
dymsh exp hit checkout dev &&


# Branch b2 (will be them)
dymsh exp hit feature b2 &&
dymsh foreach -v -m first -- bash -c "
git status &&
set -x &&
echo 'f-aa on b2' > f-aa &&
echo 'f-uu on b2' > f-uu &&
echo 'f-du on b2' > f-du &&
git add f-aa f-uu f-du &&
git mv f-dd f-ua &&
git rm f-ud &&
git commit -m 'commit on b2' &&
echo 'Done' " &&
dymsh exp hit checkout dev &&

# Merging b1 (them) into dev (us)
dymsh exp hit co b1 &&
dymsh exp hit merge &&

# Merging b2 (them) into dev/b1 (us)
dymsh exp hit co b2 &&
# dymsh exp hit merge

TMPFILE=$(mktemp)
cat > $TMPFILE <<EOF
echo -e "\e[0;32m# Automatic resolving conflicts for this sample on"
pwd
set -x
echo f-uu > f-uu
echo f-aa > f-aa
git rm f-du
git add -u
git status
git commit --no-edit
{ set +x; } 2>/dev/null
echo -e "\e[m"
exit
EOF

dymsh exp hit merge --conflict $TMPFILE &&
rm $TMPFILE

dymsh exp hit release minor &&
dymsh exp hit merge &&
dymsh check &&
dymsh exp hit push &&


false || { set +x; git status; echo '=========================='; git status --porcelain; }

set +x &&
echo "TEST HAS FINISHED SUCCESSFULLY"
