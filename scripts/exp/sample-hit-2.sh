#!/bin/bash
set -x

BASE_DIR=/tmp/dymsh/dyt/sample/2

mkdir -p $BASE_DIR &&
cd $BASE_DIR &&
rm -rf rmt/ src/ &&

mkdir src rmt &&
cd src &&
for d in a b c d; do
	# init an empty repo
	mkdir $d &&
	cd $d &&
	dymsh exp hit init myproj $d-svc &&

	# init an empty bare repo to be the remote
	mkdir $BASE_DIR/rmt/$d &&
	cd $BASE_DIR/rmt/$d &&
	git init --bare &&
	cd - &&

	# add remote and push to it
	git remote add origin $BASE_DIR/rmt/$d &&
	git push origin dev master &&
	git push origin --tags &&

	cd .. &&
	echo ----------------------------
done &&
rm -rf b c d &&

cd a &&

# will succeed
dymsh exp hit require $BASE_DIR/rmt/b b &&
dymsh exp hit require $BASE_DIR/rmt/c c &&

cd c/ &&
dymsh exp hit rq $BASE_DIR/rmt/b b &&
dymsh exp hit rq $BASE_DIR/rmt/d d &&

cd .. &&
git add c &&
git commit -m "update submodule c" &&


reset &&
git status &&

dymsh check &&
dymsh exp hit release minor > /dev/null &&
touch foo && git add foo && git commit -m "add foo" &&
dymsh check &&
dymsh exp hit merge > /dev/null &&
dymsh check &&

dymsh exp hit push &&
dymsh check &&

set +x &&
echo "TEST HAS FINISHED SUCCESSFULLY"
