
# Create cache repo
echo =================================
mkdir cache
cd cache/
git init
date > cache-config
cat <<EOF > .dymsh
export DYMSH_SERVICE_NAME=cache
export DYMSH_PUSH_IMAGE=0
EOF
git add .dymsh cache-config
dymsh version 0.1.0
git remote add origin $(pwd)/../remotes/cache
git push origin master
git push origin --tags
git tag
git submodule status
cd ..


# Add cache repo as api's submodule
cd api
git checkout master
git submodule add $(pwd)/../remotes/cache
git commit -m "adding api cache (submodule)"
dymsh version patch
git push origin master
git push origin  --tags
git tag
git submodule status
cd ..


# Upgrade submodule web/api
cd web/api
git fetch
git checkout master
git reset --hard origin/master
cd ..
git add -u
dymsh version patch
git push origin 
git push origin --tags
cd ..


# Upgrade submodule main/web
cd main/web
git fetch
git checkout master
git merge origin/master
cd ..
git add -u
dymsh version patch
git push origin 
git push origin --tags
cd ..

