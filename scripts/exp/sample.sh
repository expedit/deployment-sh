
# Create container and remotes directories
mkdir dymsh-test
cd dymsh-test/
mkdir remotes
cd remotes
for d in db api batch adm web main cache; do
  mkdir $d ; cd $d ; git init --bare ; cd .. ;
done
cd ..

# Create db repo
echo =================================
mkdir db
cd db/
git init
date > db-file1
cat <<EOF > .dymsh
export DYMSH_SERVICE_NAME=db
export DYMSH_PUSH_IMAGE=0
EOF
git add .dymsh db-file1
dymsh version 0.1.0
git remote add origin $(pwd)/../remotes/db
git push origin master
git push origin --tags
git tag
git submodule status
cd ..

# Create api repo
echo =================================
mkdir api
cd api
git init
date > api-file-v1
cat <<EOF > .dymsh
export DYMSH_SERVICE_NAME=api
export DYMSH_PUSH_IMAGE=0
EOF
git add .dymsh api-file-v1
git submodule add $(pwd)/../remotes/db/
dymsh version 0.1.0
git remote add origin $(pwd)/../remotes/api/
git push origin master
git push origin --tags
git tag
git submodule status
cd ..


# Create batch repo
echo =================================
mkdir batch
cd batch/
git init
date > bat-file
cat <<EOF > .dymsh
export DYMSH_SERVICE_NAME=batch
export DYMSH_PUSH_IMAGE=0
EOF
git add .dymsh bat-file
git submodule add $(pwd)/../remotes/api/
dymsh version 0.1.0
git remote add origin $(pwd)/../remotes/batch/
git push origin master
git push origin  --tags
git tag
git submodule status
cd ..

# Create adm repo
echo =================================
mkdir adm
cd adm
git init
date > adm-file-v1
cat <<EOF > .dymsh
export DYMSH_SERVICE_NAME=adm
export DYMSH_PUSH_IMAGE=0
EOF
git add .dymsh adm-file-v1
git submodule add $(pwd)/../remotes/batch/
git submodule add $(pwd)/../remotes/api
dymsh version 0.1.0
git remote add origin $(pwd)/../remotes/adm/
git push origin master
git push origin --tags
git tag
git submodule status

# Update api repo to v2 and fetch from adm repo
echo =================================
cd ../api
date > api-file-v2
git add api-file-v2
dymsh version minor
git push origin 
git push origin --tags
git tag
git submodule status
cd ../adm/

cd api/
git fetch
git checkout 0.2.0
cd ..
git add -u
dymsh version minor
git push origin 
git push origin --tags
git tag
git submodule status
cd ..

# Update api repo to v3
echo =================================
cd api/
date > api-file-v3
git add api-file-v3
dymsh version minor
git push origin 
git push origin --tags
git tag
git submodule status
cd ..

# Create web repo
echo =================================
mkdir web
cd web
git init
touch index.html
cat <<EOF > .dymsh
export DYMSH_SERVICE_NAME=web
export DYMSH_PUSH_IMAGE=0
EOF
git add .dymsh index.html
git submodule add $(pwd)/../remotes/api/
dymsh version 0.1.0
git remote add origin $(pwd)/../remotes/web/
git push origin master
git push origin --tags
git tag
git submodule status
cd ..

# Create main repo
echo =================================
mkdir main
cd main
git init
touch README.md
cat <<EOF > .dymsh
export DYMSH_SERVICE_NAME=main
export DYMSH_PUSH_IMAGE=0
EOF
git add .dymsh README.md
git submodule add $(pwd)/../remotes/web
dymsh version 0.1.0
git submodule add $(pwd)/../remotes/adm
dymsh version minor
git remote add origin $(pwd)/../remotes/main
git push origin master
git push origin --tags
git tag
git submodule status
cd ..
