
###################################################
# Add new branch and service name for api v2
cd api
git checkout 0.2.0
git checkout -b master-v2
sed -ri 's/=api/=api-v2/' .dymsh
git add -u
git commit -m "update service name for v2"
dymsh version patch
git push origin master-v2
git push origin --tags
cd ..

# Update api inside adm
cd adm/api
echo %%%%%%%%%%%%%%%%%%%%%%
pwd
git status
git fetch
git checkout master-v2
cd ..
git add -u
dymsh version patch
git push origin 
git push origin --tags
cd ..

# Update adm inside main
cd main/adm
echo %%%%%%%%%%%%%%%%%%%%%%
pwd
git status
git fetch
git checkout 0.2.1
cd ..
git add -u
dymsh version patch
git push origin 
git push origin --tags
cd ..

# Add new branch and service name for api v1
cd api
git checkout 0.1.0
git checkout -b master-v1
sed -ri 's/=api/=api-v1/' .dymsh
git add -u
git commit -m "update service name for v1"
dymsh version patch
git push origin master-v1
git push origin --tags
cd ..

cd batch/api
echo %%%%%%%%%%%%%%%%%%%%%%
pwd
git status
git fetch
git checkout master-v1
cd ..
git add -u
dymsh version patch
git push origin 
git push origin  --tags
cd ..

cd adm/batch
echo %%%%%%%%%%%%%%%%%%%%%%
git status
git fetch
git checkout 0.1.1
cd ..
git add -u
dymsh version patch
git push origin 
git push origin --tags
cd ..

# Update adm inside main
cd main/adm
echo %%%%%%%%%%%%%%%%%%%%%%
pwd
git status
git fetch
git checkout 0.2.2
cd ..
git add -u
dymsh version patch
git push origin 
git push origin --tags
cd ..
