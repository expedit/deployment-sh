

#

```
docker build . -t dymsh-test:1 &&
docker run -ti --rm \
           --name dymsh-test \
           --hostname dymsh-test \
           -v $(pwd):/data \
           -v /var/run/docker.sock:/var/run/docker.sock \
           dymsh-test:1 bash -c 'dymsh -v; bash' \
|| echo "Failed"
```


