cat <<EOF
Package: dymsh
Version: $DYMSH_VERSION
Maintainer: Cristiano Expedito
Architecture: all
Description: Dymsh - deployment shell scripts for git submodules and docker
Pre-Depends: git (>=2.7), bash (>=4.3), python2.7, python-yaml (>=3.0), python3 (>=3.5), python3-yaml (>=3.0), bc (>=1.0), jq (>=1.5), ssh-client, rsync
Depends: docker-ce (>=17.10) | docker.io (>=17.10)
EOF
