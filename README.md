# Deployment Scripts (DYMSH)

This repository contains a set of Bash and Python scripts for building and running projects which uses Docker in swarm mode for containerization and Git submodules for source code versioning and dependency management. It allows recursivelly building and running the entire application.


## Pre-requisites

* git >= 2.7.4
* docker >= 18.01.0-ce
* dymsh >=0.8.0
* jq, bc, python, python-yaml, ...

[comment]: # (Do not forget to update the version in this document. Current=0.8.0)

## What's new?

- Project structure now requires .dymsh to be a directory;
- dymsh run may use imperative or declarative deploy;
- environment variables names changed to follow a pattern;
- the docker/ and test/ directories are not used anymore;
- docker/ scripts moved to .dymsh/docker/;
- test/ scripts moved to .dymsh/test/;
- dymsh run supports the --target command line option;
- building and running in parallel.


## Summary

- [Installing](doc/INSTALL.md)
- [Project structure](doc/PROJECT.md)
- [Usage: build, deploy, test, stop](doc/USAGE.md)


## Command line usage

Execute `dymsh` or `dyt` for detailed usage help.

Other commands do not have CLI parameters currently.

