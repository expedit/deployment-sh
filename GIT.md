# Git summary

## Git objects:

- GitObject is a state of a set of files, accordingly identified by a SHA1.
- Commit is a GitObject with date, author identification, etc.

- Branch is mutable reference to a Commit
- Tag is a constant reference to a Commit

- HEAD is a reference to the current GitObject. It may point to a Commit, a Branch or a Tag. If it points to a Commit or Tag, it is said we are in detached HEAD state.


## Git "directories":

- Working directory, working tree or simply Workspace: the filesystem where you store and edit your current files.
- The Index, Staging area or Cache: a file where information about your next Commit is stored.
- The Local repository: the directory where your GitObjects are stored.
- The Remote repository: the remote directory where other's GitObjects are stored.

 Workflow: see images folder


## git reset [--soft|--mixed|--hard] Commit/Tag/Branch

DESC: updates the current branch (which HEAD points to) to the given Commit/Tag/Branch. Consequentely, it also updates the HEAD itself.

* --hard: updates the Workspace and the Index to match the commit.
* --mixed (default): updates the Index to match the Commit. Workspace stay as before.
* --soft: does not update neither Workspace nor Index.

WARN: if in detached HEAD state, commits may be lost


## git checkout commit/tag/branch

DESC: updates the HEAD to the given Commit/Tag/Branch and updates the Workspace to reflect it.
WARN: if in detached HEAD state, commits may be lost
NOTE: does not change modified files in Workspace 



## Changing matrix:

|COMMAND        |  Workspace | Index | Local | Remote | HEAD | HEAD's target |
|---------------|------------|-------|-------|--------|------|---------------|
|reset --soft   |    no      |   no  |   no  |   no   |  yes |       yes     |
|reset --mixed  |    no      |  yes  |   no  |   no   |  yes |       yes     |
|reset --hard   |   yes      |  yes  |   no  |   no   |  yes |       yes     |
|checkout       |   yes      |   no  |   no  |   no   |  yes |        no     |


## Brainstorming...

| Git command                     |     Dymsh/hit command     |
|---------------------------------|---------------------------|
|git add file...                  | hit wi file...            |
|git reset HEAD file...           | hit iw file...            |
|git commit -m ""                 | hit il -m ""              |
|git push origin master           | hit lr origin master      |
|git pull origin                  | hit rl                    |
|git checkout -b newbranch ref    | hit br cr newbranch=ref   |
|git checkout master              | hit br sw master          |
|git branch                       | hit br [ls]               |
|git branch -m old new            | hit br mv old new         |
|git branch -d branchname         | hit br rm branchname      |


