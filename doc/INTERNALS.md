# Internals

When a user execute `dymsh CMD PARAM...`, dymsh forward all parameters to the script defined by `CMD`:

* **scripts/**
    - **exp/** - experimental scripts (not documented here).
    - **util/** - utility scripts
    - **version.sh** - update the version of the repository
    - **info.sh** - show information
    - **fetch.sh** - fetch all submodules recursively
    - **check.sh** - check the project and its submoudles recursively
    - **build.sh** - builds and pushes the docker image
    - **run.sh** - runs the application
    - **test.sh** - executes automated tests for the application
    - **stop.sh** - stops the application


| Script         | Action     | 
|:-:             |:-:         |
| `version.sh`   | Update this repository's version by updating the VERSION file, creating a new commit and tagging the commit. |
| `info.sh`      | Show information about the images this repository would generate in an hierarchical view. |
| `fetch.sh`      | Fetch all submodules recursively handling possible git clone/fetching failures due to denied permissions. |
| `check.sh`      | Fetch all submodules using `fetch.sh` and checks them recursively to make sure they are deployable, i.e. all services have a unique version on the whole project. Different versions should be solved by creating additional branches which changes DYMSH_SERVICE_NAME variable inside .dymsh file. |
| `build.sh`     | Builds docker images for this repository and all submodules and pushs them to a registry. Performs a pre-order depth-first search. |
| `run.sh`       | Execute all docker images respecting submodule dependency three. Performs a post-order depth-first search. |
| `test.sh`      | Execute the tests for the current repository. Is not recursive, that means it does not traverse the repository tree. |
| `stop.sh`      | Stops all services. Performs a pre-order depth-first search. |
| `util/tagname.sh` | Create the tag name for the docker image based on the current git branch, commit and tag. At least one commit must exists or it will fail. |
| `util/imgname.sh` | Create the docker image name according to DYMSH_SERVICE_NAME and DYMSH_REGISTRY_DOMAIN variables and `tagname.sh` |
| `util/log.sh` | Script for coloured output to stdout/stderr via function or pipe. |
| `util/submodule.sh` | Script for handling initialization/cloning/updating of already registered submodules. |
| `util/ssh-git.sh` | Script for using SSH keys when cloning submodules. |
| `util/timed-loop.sh` | Script for timed wait loops. Indirectly affected by SERVICE_MAX_WAIT_TIME and TEST_MAX_WAIT_TIME. |

~~No script requires additional parameters, but if given they will be passed to custom scripts (see previous section).~~ TODO review this paragraph.


The `.dymsh` is executed by using the `source` command at the beginning of each script. All arguments are forwared to `.dymsh` script. This file may be sourced more than once for the same dymsh command if the command invokes inner scripts.
