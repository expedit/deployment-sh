# Single project structure

A repository which uses dymsh must respect the following directory structure:

* **repository**
  - **.dymsh/** - *required*
    - **init.sh** - *required* - initialization shell script
    - **service.yml** - *optional* - main declarative configuration file (YAML)
    - **service.*.yml** - *optional* - additional declarative configuration files (YAML)
    - **docker/** - *optional*
      + **build-image.sh** - app-defined service builder
      + **create-service.sh** - app-defined service creator
    - **test/** - *optional*
        + **connect.sh** - initial test, will be retried a few times before failing
        + **start.sh** - start automated tests
  - **Dockerfile** - *required*


## The `.dymsh/` directory

This directory is required and holds all dymsh configuration.


### The `.dymsh/init.sh` file

This script contains the project settings. It is run as a shell script, that means you may run any command (e.g. pwd, ls) however any variable defined inside the script will be exported automatically. Dymsh gives a special meaning for some variables (also called configuration variables):

| Variable              | Used by | Description                                                 | Default           |
|:-                     |:-:      |:-:                                                          |:-:                |
| DYMSH_VERSION         | dymsh   | Rule of dymsh versions required/supported by the project.   | 0.8.x             |
| DYMSH_PROJECT_NAME    | Any     | The name of the project shared among all services           | **[Required]**    |
| DYMSH_SERVICE_NAME    | Any     | The docker's service name                                   | **[Required]**    |
| DYMSH_NETWORK_NAME    | Any     | The docker's overlay network name                           | **[Required]**    |
| DYMSH_PUSH_IMAGE      | build   | Whether the built image should be pushed to the registry. See note below.  | 1  |
| DYMSH_REGISTRY_DOMAIN | build   | The registry domain (hostname + port) to where push images. | registry:5000 |
| TODO SERVICE_MAX_WAIT_TIME | run | Max time to wait for service start. An internal condition will test if the service has started. See note below. | 10 |
| TODO TEST_MAX_WAIT_TIME | test | Max time to wait for connection test. The condition is defined by `./test/connect.sh`.  | 10    |


**Note 1:** DYMSH_VERSION uses semantic versioning rules. Possible values are 1.x, ^1.0.0; 1.2.x, ~1.2.0; ^0.2.6, ~0.2.6.

**Note 2:** `dymsh build` uses these variables to generate the image name and determine whether the built image shall be pushed to a registry or become local. For DYMSH_PUSH_IMAGE any value different from 1 will be interpreted as false. DYMSH_REGISTRY_DOMAIN will be ignored if DYMSH_PUSH_IMAGE is not equal to 1.

**Note 3:** TODO the waiting time starts from 0 and is incremented each time the condition fails. When the waiting time value reaches the value of the variable an error is returned.


The minimum acceptable content is the definition of the three required variables (see table above).

Additionally, the following variables may be created by dymsh and be available inside the script (they may also be empty):


| Variable              | Defined by | Description                                                        |
|:-:                    |:-:      |:-:                                                                 |
| DYMSH_TARGET          | run     | The target name specified by --target command line option.         |
| DYMSH_COMMAND         | Any     | The command which is `source`ing the script                        |
| DYMSH_IMAGE_NAME      | run     | The image name generated from other variables and git tag/commit.  |

Since you may run any command its up to you to avoid using commands which have side effects unless you are sure about their impact.


Example:

    DYMSH_PROJECT_NAME=foo
    DYMSH_SERVICE_NAME=bar
    DYMSH_NETWORK_NAME=foo-network

    case $DYMSH_TARGET in
        dev)
            NODE_ENV=devel
            ;;
        state)
            NODE_ENV=stage
            ;;
        prod)
            NODE_ENV=prod
            ;;
        *)
            echo "Invalid target name" >&2
            return 1
    esac



## The `Dockerfile` file

A Dockerfile is also needed for building the docker's images which will contain the service. No specific configuration or commands is needed here.



# Custom project structure

TODO 
### The docker directory

This directory is deprecated and will only be seen by dymsh if there is no service.yml into .dymsh directory.

This directory is optional and may have zero or more scripts. If a script is missing a default command is executed. The following table summarizes it:

| Script           | Arguments        | Action          | Default command |
|:-:               |:-:               |:-:              |:-:              |
| `build-image.sh`     | `DYMSH_IMAGE_NAME ...`   | Build the service image | `docker build -t DYMSH_IMAGE_NAME .`   |
| `create-service.sh`  | `DYMSH_IMAGE_NAME ...`   | Create the service | `docker service create --name DYMSH_SERVICE_NAME --replicas=1 --network=DYMSH_NETWORK_NAME DYMSH_IMAGE_NAME` |

All scripts above also receive the arguments passed to its caller script. For instance:

    dymsh build -- dev 1 X 0

would call

    ./.dymsh/docker/build-image.sh DYMSH_IMAGE_NAME dev 1 X 0


### The test directory

This directory is optional and may have zero or more scripts. If a script is missing, no action is performed and it is considered to be successfully executed. Therefore, if no script exists the whole test will succeed.

| Script        | Action                     | Default command    |
|:-:            |:-:                         |:-:                 |
| `connect.sh`  | Should test if the service is responding in some way.  | true  |
| `start.sh`    | Should start the automated test set.    | true      |

All scripts above also receive the arguments passed to its caller script. For instance:

    dymsh test -- dev 1 X 0

would call

    ./test/connect.sh DYMSH_IMAGE_NAME dev 1 X 0
    ./test/start.sh DYMSH_IMAGE_NAME dev 1 X 0


# Advanced project structure

This section shows how a project with git submodules should look like. For instance, if you a have a *my-root-repo* which depends on *my-2nd-level-repo* and *my-other-2nd-level-repo*, and *my-2nd-level-repo* depends on *my-3rd-level-repo*, your directory three should look like this:

* my-root-repo
    - **.dymsh/**
    - **Dockerfile**
    - docker/
    - test/
    - submodules
        + *my-2nd-level-repo.pem*
        + *my-2nd-level-repo.pre.sh*
        + *my-2nd-level-repo.pos.sh*
        + *my-2nd-level-repo.err.sh*
        + my-2nd-level-repo.pem.pub
        + my-2nd-level-repo/
            * **.dymsh/**
            * **Dockerfile**
            * submodules
                - *my-3rd-level-repo.pem*
                - *my-3rd-level-repo.pre.sh*
                - *my-3rd-level-repo.pos.sh*
                - *my-3rd-level-repo.err.sh*
                - my-3rd-level-repo.pem.pub
                - my-3rd-level-repo/
                    + ...
        + *my-other-2nd-level-repo.pem*
        + *my-other-2nd-level-repo.pre.sh*
        + *my-other-2nd-level-repo.pos.sh*
        + *my-other-2nd-level-repo.err.sh*
        + my-other-2nd-level-repo.pem.pub
        + my-other-2nd-level-repo/
            * ...

The files in bold are required. Files in italic are optional but useful for cloning/fetching the submoudles. The functions of these files are:

- `*.pem` files are SSH keys for cloning/fetching the respective submodule without password. If a PEM file for a given submodule does not exist, dymsh tries to clone/fetching without any SSH key. This way, the repository must be public or the system must handle the key by issuing a global or a per-host key (See [SSH config](http://man.openbsd.org/ssh_config));
- `*.pem.pub` files are not used by dymsh. They are here just to remember public keys are required by git remote servers.
- `*.pre.sh` files are hook scripts which dymsh executes before trying cloning/fetching. The script should return 0 on success and non-zero on failure. If the script fails dymsh abort the whole operation.
- `*.pos.sh` files are hook scripts which dymsh executes after successfully cloning/fetching. The script should return 0 on success and non-zero on failure. If the script fails dymsh abort the whole operation.
- `*.err.sh` files are hook scripts which dymsh executes after cloning/fetching failure. The script should return 0 on success and non-zero on failure. If the script fails dymsh abort the whole operation.
