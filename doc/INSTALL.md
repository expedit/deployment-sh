# Installing dymsh

## By cloning

You should clone this repository, enter into its directory and install to an directory which is on your PATH (visible by your shell). Choose just **ONE** of the following installation methods:

As root:

    cd /opt
    git clone -b 0.8.x https://expedit@bitbucket.org/expedit/deployment-sh.git dymsh
    cd dymsh
    ./install --dyt /usr/local/bin

As a common user:

    mkdir -p ~/bin
    PATH=$PATH:$HOME/bin
    cd $HOME
    git clone -b 0.8.x https://expedit@bitbucket.org/expedit/deployment-sh.git dymsh
    cd dymsh
    ./install --dyt ~/bin

If you want this installation permament, add the line `PATH=...` to the file `~/.profile`.

### Updating

As the same user used when installing:

```
cd /path/to/dymsh-source     # e.g. /opt/dymsh, ~/dymsh
git pull origin
git checkout 0.8.x
```

Note: if your old installation version was <=0.6.2 and you want to use `dyt`, just uninstall and install dymsh binary again (after `git pull` above):

```
./install -u
./install --dyt path/to/binary   # e.g. ~/bin or /usr/local/bin
```

## From Debian Package

Just download (or build) and install the debian package:

- Installing:

```
    sudo dpkg -i  /path/to/dymsh.deb
```

- Building the package and installing:

```
    cd /path/to/dymsh-source
    ./package.sh --debian
    sudo dpkg -i  releases/dymsh_0.8.2_all.deb
```

- Uninstalling (both --purge and --remove work):

```
sudo dpkg --purge dymsh
```


## From tarball

TODO


## TODO 

To test installation in a previous version of debian.

    docker run -ti --rm --volume ~/devel:/devel debian:8.5 /bin/bash
