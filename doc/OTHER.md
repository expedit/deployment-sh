# Experimental

## Digital Ocean's machine creation

This script creates a Debian Droplet applying security rules (packages upgrade, firewall configuration), enabling Swap and installing dymsh.

Type `dymsh exp/create-vm` without parameters for help.

Sample:

```
export DYMSH_PROJECT_NAME=myproj
export DO_REGION=nyc3
export DO_TOKEN=<digital-ocean-api-token>
export SSH_FINGERPRINT=<ssh-fingerprint>
dymsh exp/create-vm prod front 512mb 01
```


## Docker logs

Shows the docker logs for the running containers of this repository.

```
dymsh exp/logs -f --
```

TODO: I think this command is not needed anymore due to docker has logs for containers and services now. To be checked.

## Dyt

Dyt is yet experimental too.


## Planning: introduction of dyp

Dyp is the tool for provisioning by writing a YAML configuration which is handled internally by Terraform and Ansible. TODO


# Third-party tools

- [https://github.com/qzb/sh-semver](https://github.com/qzb/sh-semver)

