
# Deploying

Once dymsh is installed, you just need to enter the root directory of your repository and execute some commands. The common usage is building, running, testing and stopping. Therefore, you should execute these commands:

    docker swarm init
    dymsh clone URL project -b branch
    cd project
    dymsh check
    dymsh docker build -l
    dymsh docker start -t dev
    dymsh test
    dymsh docker stop

Note that none of the commands requires additional parameters. You may, however, give extra arguments to them and they will be forwarded without any changes to `.dymsh`, internal scripts, user scripts and submodules.


## ***Deprecated from here! Needs reviewing***


### Working with submodules

Git submodules always point to commits (which may also be ponted by branches or tags) and are in detached state (no branch) by default.

Definitions used here:

- *container* is the repository you get by issuing the `git clone ...` command. You may think as your root repository.
- *submodule* is one or more repository(ies) you add to your container because the container depends on them but you cannot use as simple packages, like node modules or java libraries (e.g. you or your team are actively developing it). In the context of dymsh, it's expected the submodule be a docker service with a .dymsh file inside it.
- *upgrade* of a submodule is the process of actively changing the version/tag/commit of a submodule and, consequently, pushing these modifications.
- *update* of a submodule is the process of getting already pushed upgrades.

Since version 0.6.0, dymsh offers a dymsh ~~git~~ exp hit (or merely *dyt*) which makes it easy working with a repository tree (i.e. submodules). Dyt offers commands for cloning, checking out, pulling and pushing the whole tree as much as for creating feature, release and hotfix branches, and merge them into dev and master branches using a workflow inspired in the [Vincent Driessen's branching model](http://nvie.com/posts/a-successful-git-branching-model) and extended to handle a repository tree.

In the following sections some commands are shown. Remember all dyt commands must be run from **within the root repository**.

#### Showing usage help

```
dymsh exp hit help  # or dyt help or dyt -h
```

#### Creating the 'dyt' alias for 'dymsh exp hit'

If you **DID NOT** installed using *--dyt* option:

```
eval $(dymsh exp hit alias)
```

#### Initializing a repository with dymsh support

```
mkdir </path/to/dir>
cd </path/to/dir
dyt init <project-name> <service-name>
```

This command will create an empty git repository, add a .dymsh file (already configured for the given project and service names), build the 0.0.1 version and checkout the dev branch.

#### Cloning an existing repository and its submodules recursively

```
dyt clone <repository-url> <path/to/dir>
cd </path/to/dir>
```

#### Checking out a branch recursively

```
dyt checkout <branch-name>    # or 'dyt co ...'
```

#### Adding a submodule to an existing repository

```
dyt require <repository-url> <name> [<directory>]
```

Add the submodule from the given `<repository-url>` to the `<directory>/<name>` directory. The `<directory>` parameter defaults to `./` (the current repository root).
Current checked out branch must be dev or a feature branch. This operation is **not recursive**.

#### Adding a new feature to the project

```
dyt feature <feature-branch-name>      # 1) Start a feature

nano file1
git add file1
git commit -m "adding file1"

cd <submodule>
nano file2
git add file2
git commit -m "adding file2"
cd -

dyt merge        # 2) Once the feature is done, merge into dev
```

1) Start the feature branch: a feature branch name may be anything except master, dev, release-&ast;, or hotfix-&ast;. You must be in the **dev** branch.

2) The merge command will detect you are in a **feature** branch and take the needed operations to update any repositories recursively and merge them into dev. Then, the feature branch is deleted.

#### Preparing a new production release

```
dyt release <ver>   # 1) Start a release: <ver> = major, minor, patch or x.y.z

nano file1
git add file1
git commit -m "fixing file1 bug"

cd <submodule>
nano file2
git add file2
git commit -m "fixing file2 bug"
cd -

# Once the release is ready to go to production
dyt merge    # 2) Merge the release branch into dev and master
```

1) Start the release branch. A release branch will be automatically created and checked out based on the current version and the *ver* option. You must be in the **dev** branch.

2) The merge command will detect you are in a **release** branch and take the needed operations to update any repositories recursively and merge them into dev **and** master. A version tag is created and the feature branch is deleted.

#### Fixing a production critical bug

```
dyt hotfix <ver>   # 1) Start a hotfix: <ver> = minor, patch

nano file1
git add file1
git commit -m "fixing file1 critical bug"

cd <submodule>
nano file2
git add file2
git commit -m "fixing file2 critical bug"
cd -

# Once the hotfix is ready to go to production
dyt merge    # 2) Merge the hotfix branch into master, etc.
```

1) Start the hotfix branch. A hotfix branch will be automatically created and checked out based on the current version and the *ver* option. You must be in the **master** branch.

2) The merge command will detect you are in a **hotfix** branch and take the needed operations to update any repositories recursively and merge them into dev **and** master **and** any open release branches. A version tag is created and the hotfix branch is deleted.

#### Pushing the changes to a repository recursively

```
dyt push <repository>
```

The *<repository>* is optional and defaults to origin.

#### Pulling changes from a repository recursively

```
dyt pull <repository>
```

The *<repository>* is optional and defaults to origin.

#### Checking the repository tree

```
dymsh check --strict
```


### Notes

- Avoid more than one unmerged release or hotfix branches at a time: since they will be merged into master this may lead to undesired tree conflicts;
- Avoid direct commits to the dev branch, try to always branching off from dev (feature or release branches), commit the changes, then merge back using `dyt merge`;
- The `dyt merge` command will merge into master (from release or hotfix branches), release branches (from hotfix branches) and, finally, dev (from all branches);
- Tree conflicts are not handled automatically by dyt and needs manual correction and merging. However, dyt detect conflicts during `dyt merge` and start a terminal showing instructions on how to solve.


#### Handling merge conflicts

Merging is the process of joining two o more branches together. In this process conflicts may happen (always in developer machine) and need to be fixed before pushing to any remote machine.

Git names the source and destination branches as *them* and *us*, respectivelly:

```
# merging them (from) into us
git checkout us
git merge them
```


##### Cases:

In the following the status for `git status --porcelain` and `git status`, respectivelly (X = index, us; Y = work tree, them):

| XY  | human-readable status message  |
|:-:  |:-:                             |
| AA  | unmerged, both added           |
| UU  | unmerged, both modified        |
| AU  | unmerged, added by us          |
| UA  | unmerged, added by them        |
| DD  | unmerged, both deleted         |
| UD  | unmerged, deleted by them      |
| DU  | unmerged, deleted by us        |


The most commons cases are:

1) Both modified (UU): the same lines of the file were modified on A and B;

2) Deleted by someone (UD, DU): the file was modified on A and deleted on B;

3) Both added (AA): the file was added on A and on B;


##### Reproducing conflicts:

See also:

AU,UA,DD => https://stackoverflow.com/questions/3021649/git-how-to-create-different-unmerged-states

### Executing the sample tests:

Just open the terminal and type the following commands, one at a time:

```
dymsh exp sample-hit-1
dymsh exp sample-hit-2
dymsh exp sample-hit-3-conflict
dymsh exp sample-hit-4-sync
```

Each command should output the message "TEST HAS FINISHED SUCCESSFULLY" on the last line.