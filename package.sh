#!/bin/bash

dymsh_package()
{
	DYMSH_BIN_PATH=$(find $DYMSH_ROOT -name dymsh -executable | head -1)
	DYMSH_ROOT_DIR=$(dirname $DYMSH_BIN_PATH)
	DYMSH_VERSION=$(cat $DYMSH_ROOT_DIR/VERSION)

	EXTENSION="tar.gz"
	OUTFILE="$OUTDIR/dymsh-$DYMSH_VERSION.$EXTENSION"

	echo "Packaging dymsh into $OUTFILE..."

	mkdir -p $OUTDIR

	OLDPWD=$PWD
	cd $DYMSH_ROOT_DIR
	tar -czvf "$OUTFILE" \
	    --transform "s,^,dymsh-$DYMSH_VERSION/," \
	    --exclude="*.$EXTENSION" \
	    --exclude="releases/*" \
	    --exclude='imgs/*' \
	    --exclude='.ignore/*' \
	    --exclude='.vscode/*' \
	    *

	if [ $? -eq 0 ]; then
		cd $OLDPWD
		cat <<-EOF
		Package written to $OUTFILE
		To install in another system:
		  1) copy the package to some directory
		  2) enter into the directory
		  3) then execute:
		       tar -xzvf dymsh-$DYMSH_VERSION.$EXTENSION
		       cd dymsh-$DYMSH_VERSION.$EXTENSION
		       ./install --dyt path/to/binary   # e.g. ~/bin or /usr/local/bin
		EOF
		return 0
	else
		cd $OLDPWD
		echo
		return 1
	fi
}

build-dymsh-deb-package()
{
	DYMSH_BIN_PATH=$(find $DYMSH_ROOT -name dymsh -executable | head -1)
	DYMSH_ROOT_DIR=$(dirname $DYMSH_BIN_PATH)
	DYMSH_VERSION=$(cat $DYMSH_ROOT_DIR/VERSION)

	local outdir=$(mktemp -d)
	mkdir -p $outdir/opt/dymsh $outdir/usr/local/bin

	rsync -av "$DYMSH_ROOT_DIR/scripts" $outdir/opt/dymsh/
	cp "$DYMSH_ROOT_DIR/"{dymsh,VERSION} $outdir/opt/dymsh/
	
	DEB_PKG_DIR="$DYMSH_ROOT_DIR/debian-package"
	mkdir $outdir/DEBIAN
	cp "$DEB_PKG_DIR/DEBIAN/"* $outdir/DEBIAN/
	DYMSH_VERSION=$DYMSH_VERSION \
	  bash "$DEB_PKG_DIR/DEBIAN/.control.template.sh" > $outdir/DEBIAN/control

	find $outdir

	dpkg-deb --build $outdir "$OUTDIR"

	rm -rf $outdir
}


DYMSH_ROOT=$(dirname $(readlink -f "$0"))
# DYMSH_ROOT=$(dirname "$0")
echo "DYMSH_ROOT=$DYMSH_ROOT"

OUTDIR="$DYMSH_ROOT/releases"

debian=0
is-debian-package() { test $debian = 1; }

while [ $# -gt 0 ]; do
	case "$1" in
		-o|--outdir)
			OUTDIR="$(readlink -f "$2")"
			test -d "$OUTDIR" \
			  || OUTDIR="$DYMSH_ROOT/releases"
			shift 2
			;;
		--debian)
			debian=1
			shift
			;;
		*)
			echo "Unknown argument: $1"
			exit 1
			;;
	esac
done

# exit 1

if is-debian-package; then build-dymsh-deb-package
else dymsh_package
fi
